//
//  ProfileTableViewCell.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface ProfileTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *parameterNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *parameterValueLabel;


@end
