//
//  ProfileTableViewCell.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ProfileTableViewCell.h"

@implementation ProfileTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.parameterNameLabel.text = @"";
    self.parameterValueLabel.text = @"";
}

@end
