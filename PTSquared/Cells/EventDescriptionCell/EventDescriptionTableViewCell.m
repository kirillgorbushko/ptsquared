//
//  EventDescriptionTableViewCell.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "EventDescriptionTableViewCell.h"
#import "CalendarDataSource.h"

@interface EventDescriptionTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *sessionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionStartTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

@implementation EventDescriptionTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.sessionNameLabel.text = @"";
    self.sessionStartTimeLabel.text = @"";
    self.sessionTypeLabel.text = @"";
    self.session = nil;
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];

}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIView *selectionView = [[UIView alloc] initWithFrame:self.frame];
    selectionView.backgroundColor = [UIColor menuBlueColor];
    self.selectedBackgroundView = selectionView;
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

#pragma mark - Custom Accessors

- (void)setSession:(Session *)session
{
    _session = session;
    [self fillAllFields];
}

#pragma mark - Public

- (void)setWhiteColorForCell
{
    self.sessionNameLabel.textColor = [UIColor whiteColor];
    self.sessionStartTimeLabel.textColor = [UIColor whiteColor];
    self.sessionTypeLabel.textColor = [UIColor whiteColor];
}

- (void)setGrayStyleForCell
{
    self.sessionNameLabel.textColor = [UIColor grayColor];
    self.sessionStartTimeLabel.textColor = [UIColor grayColor];
    self.sessionTypeLabel.textColor = [UIColor grayColor];
}

- (void)fillAllFields
{
    if ([[self.session.sessionType capitalizedString] isEqualToString:@"Individual"]) {
        self.sessionTypeLabel.text = @"Private";
    }  else if (self.session.sessionIsPublic) {
        self.sessionTypeLabel.text = @"Public";
    } else {
        self.sessionTypeLabel.text = [self.session.sessionType capitalizedString];
    }
    
    self.sessionNameLabel.text = [self.session.sessionName capitalizedString];
    self.sessionStartTimeLabel.text = [CalendarDataSource timeFromDate:self.session.sessionDate];

    if (self.session.sessionIsComplete) {
        [self setGrayStyleForCell];
    } else {
        [self setWhiteColorForCell];
    }
}

@end
