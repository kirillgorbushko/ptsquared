//
//  EventDescriptionTableViewCell.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Session.h"

@interface EventDescriptionTableViewCell : UITableViewCell

@property (strong, nonatomic) Session *session;

- (void)setGrayStyleForCell;

@end
