//
//  ProfileFooterTableViewCell.m
//  PT2
//
//  Created by Kirill Gorbushko on 03.08.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "ProfileFooterTableViewCell.h"

@interface ProfileFooterTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

@implementation ProfileFooterTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

@end