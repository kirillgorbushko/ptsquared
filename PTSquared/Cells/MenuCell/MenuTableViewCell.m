//
//  PTMenuTableViewCell.m
//  PTSquared
//
//  Created by Kirill on 11/11/14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIView *selectionView = [[UIView alloc] initWithFrame:self.frame];
    selectionView.backgroundColor = [UIColor menuBlueColor];
    self.selectedBackgroundView = selectionView;
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

@end
