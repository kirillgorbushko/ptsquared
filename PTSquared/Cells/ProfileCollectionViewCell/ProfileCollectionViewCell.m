//
//  ProfileCollectionViewCell.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ProfileCollectionViewCell.h"

@implementation ProfileCollectionViewCell

#pragma mark - LIfeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.clientPhotoImageView.image = nil;
}

#pragma mark - Public

- (void)startAnimatingActivityIndicator
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.ptLogo.hidden = NO;
    });
}

- (void)stopAnimatingActivityIndicator
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.ptLogo.hidden = YES;
    });
}

@end
