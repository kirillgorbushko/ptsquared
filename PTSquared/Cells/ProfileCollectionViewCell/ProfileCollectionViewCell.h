//
//  ProfileCollectionViewCell.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface ProfileCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *clientPhotoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ptLogo;

- (void)stopAnimatingActivityIndicator;
- (void)startAnimatingActivityIndicator;

@end
