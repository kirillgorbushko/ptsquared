//
//  FrontTutorialCollectionViewCell.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 25.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface FrontTutorialCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
