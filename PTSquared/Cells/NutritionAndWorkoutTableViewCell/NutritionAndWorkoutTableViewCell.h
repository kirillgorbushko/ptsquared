//
//  NutritionTableViewCell.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface NutritionAndWorkoutTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *planNameLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end
