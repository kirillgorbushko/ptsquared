//
//  NutritionTableViewCell.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NutritionAndWorkoutTableViewCell.h"

@implementation NutritionAndWorkoutTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.planNameLabel.text = @"";
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIView *selectionView = [[UIView alloc] initWithFrame:self.frame];
    selectionView.backgroundColor = [UIColor menuBlueColor];
    self.selectedBackgroundView = selectionView;
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

@end
