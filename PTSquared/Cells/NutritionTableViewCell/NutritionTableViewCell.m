//
//  NutritionTableViewCell.m
//  PT2
//
//  Created by Yuriy Sirko on 7/7/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NutritionTableViewCell.h"

@interface NutritionTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingBottomSeparatorSpace;

@end

@implementation NutritionTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
    self.descriptionLabel.text = @"";
    self.headerLabel.text = @"";
    
    self.leadingBottomSeparatorSpace.constant = 15.f;
}

#pragma mark - Public

- (void)setStyleForLastCell
{
    self.leadingBottomSeparatorSpace.constant = 0.f;
    [self layoutIfNeeded];
}

@end
