//
//  NutritionTableViewCell.h
//  PT2
//
//  Created by Yuriy Sirko on 7/7/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface NutritionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

- (void)setStyleForLastCell;

@end
