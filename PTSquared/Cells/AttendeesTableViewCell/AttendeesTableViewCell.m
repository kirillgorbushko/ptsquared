//
//  AttendeesTableViewCell.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AttendeesTableViewCell.h"

@implementation AttendeesTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.photoImageView.image = [UIImage imageNamed:PersonPlaceholderFileName];
    [self setCornerRadius];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.photoImageView.image = [UIImage imageNamed:PersonPlaceholderFileName];
    self.nameLabel.text = @"";
}

#pragma mark - Private Methods

- (void)setCornerRadius
{
    self.photoImageView.layer.cornerRadius = CGRectGetHeight(self.photoImageView.bounds) / 2;
    self.photoImageView.layer.masksToBounds = YES;
}

@end
