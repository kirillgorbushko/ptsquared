//
//  SupersetHeaderTableViewCell.h
//  PTSquared
//
//  Created by sirko on 5/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface SupersetHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
