//
//  PTTutorialCell.h
//  PTSquared
//
//  Created by Kirill on 11/10/14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

@interface TutorialCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)updateTextPosition:(CGFloat)scrollValue;

@end
