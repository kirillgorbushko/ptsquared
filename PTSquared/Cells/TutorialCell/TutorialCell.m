//
//  PTTutorialCell.m
//  PTSquared
//
//  Created by Kirill on 11/10/14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

#import "TutorialCell.h"

@implementation TutorialCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.titleLabel.text = @"";
}

#pragma mark - Public

- (void)updateTextPosition:(CGFloat)scrollValue
{
    CGPoint ancorPoint = self.titleLabel.layer.anchorPoint;
    ancorPoint.x = scrollValue;
    self.titleLabel.layer.anchorPoint = ancorPoint;
}

@end
