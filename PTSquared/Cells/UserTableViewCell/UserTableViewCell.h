//
//  UserTableViewCell.h
//  PTSquared
//
//  Created by Admin on 09/06/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class ClientProfile;

#import <UIKit/UIKit.h>

static NSString *const UserTableViewCellIdentifier = @"UserTableViewCell";

@interface UserTableViewCell : UITableViewCell

@property (strong, nonatomic) ClientProfile *user;

@end
