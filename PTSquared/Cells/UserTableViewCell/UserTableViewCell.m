//
//  UserTableViewCell.m
//  PTSquared
//
//  Created by Admin on 09/06/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "UserTableViewCell.h"
#import "ClientProfile.h"

@interface UserTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *userAvatar;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end

@implementation UserTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.userAvatar.image = [UIImage imageNamed:PersonPlaceholderFileName];
    [self setCornerRadius];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.userName.text = @"";
    self.userAvatar.image = [UIImage imageNamed:PersonPlaceholderFileName];
}

#pragma mark - Public Methods

- (void)setUser:(ClientProfile *)user
{
    _user = user;
    [self fillCellUserData:user];
}

#pragma mark - Private Methods

- (void)setCornerRadius
{
    self.userAvatar.layer.cornerRadius = CGRectGetHeight(self.userAvatar.bounds) / 2;
    self.userAvatar.layer.masksToBounds = YES;
}

- (void)fillCellUserData:(ClientProfile*)user
{
    self.userName.text = [NSString stringWithFormat:@"%@ %@", user.clientFirstName, user.clientLastName];
    NSString *avatarPath = user.clientAvatar.avatarImageUri;
    if (avatarPath) {
        __weak typeof(self) weakSelf = self;
        [[NetworkManager sharedManager] ptSquaredAPIGetImageWithPath:avatarPath withCompletition:^(BOOL success, UIImage *response) {
            if (success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    weakSelf.userAvatar.image = response;
                });
            }
        }];
    }
}

@end
