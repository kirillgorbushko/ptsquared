//
//  WorkoutTableViewCell.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

typedef NS_ENUM(NSUInteger, WorkoutTableViewCellType) {
    WorkoutTableViewCellTypeNormal = 0,
    WorkoutTableViewCellTypeSuperset = 1,
};

@interface WorkoutTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *repeatCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *notesLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@property (assign, nonatomic) WorkoutTableViewCellType type;

- (void)showSeparatorView;

@end
