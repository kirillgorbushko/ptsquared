//
//  WorkoutTableViewCell.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "WorkoutTableViewCell.h"

@interface WorkoutTableViewCell ()

@end

@implementation WorkoutTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.notesLabel.text = @"";
    self.repeatCountLabel.text = @"";
    self.descriptionLabel.text = @"";
    
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

#pragma mark - Public

- (void)showSeparatorView;
{
    self.separatorView.hidden = NO;
}

#pragma mark - Custom Accessors

- (void)setType:(WorkoutTableViewCellType)type
{
    _type = type;
    if (self.type == WorkoutTableViewCellTypeSuperset) {
        self.separatorView.hidden = YES;
//        self.notesLabel.hidden = YES;
    }
}

@end
