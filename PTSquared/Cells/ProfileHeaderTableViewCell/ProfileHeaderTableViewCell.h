//
//  ProfileHeaderTableViewCell.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface ProfileHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerNameLabel;

@end
