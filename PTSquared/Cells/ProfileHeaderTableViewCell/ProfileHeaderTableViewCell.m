//
//  ProfileHeaderTableViewCell.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ProfileHeaderTableViewCell.h"

@implementation ProfileHeaderTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.headerNameLabel.text = @"";
}

@end
