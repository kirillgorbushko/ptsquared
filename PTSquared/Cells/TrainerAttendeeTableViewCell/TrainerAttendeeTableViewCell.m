//
//  TrainerAttendeeTableViewCell.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerAttendeeTableViewCell.h"
#import "ClientProfile.h"
#import "Avatar.h"

static NSString *const TickIconName = @"Tick_Small_Icon";
static NSString *const AlertIconName = @"Alert_Small_Icon";

static NSString *const SessionInvited = @"invited";
static NSString *const SessionPaid = @"paid";
static NSString *const SessionUnpaid = @"unpaid";

@interface TrainerAttendeeTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *attendeePhotoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *attendeeStatusImageView;
@property (weak, nonatomic) IBOutlet UILabel *attendeeNameLabel;

@end

@implementation TrainerAttendeeTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self trimCorners];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.attendeePhotoImageView.image = nil;
    self.attendeeStatusImageView.image = nil;
    self.attendeeNameLabel.text = @"";
}

#pragma mark - Custom Accessors

- (void)setAttendee:(ClientProfile *)attendee
{
    _attendee = attendee;
    [self fillAllFields];
}

#pragma mark - Private

- (void)setStatusForAttendee
{
    NSString *imageName;
    NSString *clientStatus = self.attendee.clientCurrentStatusForSessionID;
    NSArray *statuses = @[SessionPaid, SessionInvited, SessionUnpaid];
    
    NSInteger statusIndex = [statuses indexOfObject:clientStatus];
    switch (statusIndex) {
        case AttendeeStatusPaid:{
            imageName = TickIconName;
            break;
        }
        case AttendeeStatusUnpaid: {
            imageName = AlertIconName;
            break;
        }
        case AttendeeStatusInvited: {
            //TODO
            break;
        }
    }
    
    if (imageName.length) {
        self.attendeeStatusImageView.image = [UIImage imageNamed:imageName];
    }
}

- (void)fillAllFields
{
    NSString *imageURL = self.attendee.clientAvatar.avatarImageUri;
    
    if (imageURL.length) {
        __weak __typeof(self) weakSelf = self;
        [[NetworkManager sharedManager] ptSquaredAPIGetImageWithPath:imageURL withCompletition:^(BOOL success, UIImage *response) {
           dispatch_async(dispatch_get_main_queue(), ^{
               weakSelf.attendeePhotoImageView.image = response;
           });
        }];
    }
    
    NSString *firstName = self.attendee.clientFirstName;
    NSString *lastName = self.attendee.clientLastName;
    self.attendeeNameLabel.text = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    
    
    [self setStatusForAttendee];
}

- (void)trimCorners
{
    self.attendeePhotoImageView.layer.cornerRadius = CGRectGetHeight(self.attendeePhotoImageView.bounds) / 2;
    self.attendeePhotoImageView.layer.masksToBounds = YES;
}

@end
