//
//  TrainerAttendeeTableViewCell.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class ClientProfile;

typedef NS_ENUM(NSUInteger, AttendeeStatus) {
    AttendeeStatusPaid,
    AttendeeStatusInvited,
    AttendeeStatusUnpaid
};

@interface TrainerAttendeeTableViewCell : UITableViewCell

@property (strong, nonatomic) ClientProfile *attendee;

@end
