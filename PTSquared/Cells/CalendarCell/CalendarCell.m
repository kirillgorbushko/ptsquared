//
//  CalendarCell.m
//  testCalendar
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "CalendarCell.h"
#import "CalendarDay.h"
#import "Session.h"
#import "Animation.h"
#import "CalendarDataSource.h"

static NSString *const BothSessionImageName = @"Client_calendar_Both_Sessions"; //red-blue
static NSString *const PublicSessionImageName = @"Client_Calendar_Public_Session"; //red
static NSString *const ScheduledSessionImageName = @"Client_Calendar_Scheduled_Session"; //blue
static NSString *const IndicatorSessionImageName = @"Sessions_Indicator"; //blue

@interface CalendarCell()

@property (weak, nonatomic) IBOutlet UIImageView *dayMarkImageView;

@end

@implementation CalendarCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.dayMarkImageView.image = nil;
    self.daySelectionView.hidden = YES;
    self.dayNumberLabel.text = @"";
    self.dayNumberLabel.textColor = [UIColor whiteColor];
}

#pragma mark - Public

- (void)setSessionMode:(SessionMode)mode
{
    NSString *imageName;
    switch (mode) {
        case SessionModePublic: {
            imageName = PublicSessionImageName;
            break;
        }
        case SessionModeScheduled: {
            imageName = ScheduledSessionImageName;
            break;
        }
        case SessionModeAll: {
            imageName = BothSessionImageName;
            break;
        }
        case SessionModeIndicate:{
            imageName = IndicatorSessionImageName;
            break;
        }
        default:
            break;
    }
    if (imageName.length) {
        self.dayMarkImageView.image = [UIImage imageNamed:imageName];
    }
}

- (void)setCellSelected:(BOOL)selected
{
    self.daySelectionView.hidden = !selected;
    self.daySelectionView.backgroundColor = [UIColor whiteColor];
}

- (void)setCellSelectedWithDay:(CalendarDay *)selectedDay;
{
    BOOL isGroup = NO;
    BOOL isPublic = NO;
    BOOL isIndividual = NO;
    
    for (Session *session in selectedDay.calendarSessions) {
        if (session.sessionIsPublic && [session.sessionType isEqualToString:@"group"]) {
            isPublic = YES;
            continue;
        }

        if ([session.sessionType isEqualToString:@"group"]) {
            isGroup = YES;
        }
        if ([session.sessionType isEqualToString:@"individual"]) {
            isIndividual = YES;
        }
    }
    if (isPublic && isIndividual && isGroup) {
        [self setSessionMode:SessionModeAll];
    } else if ( (isIndividual && isGroup)) {
        [self setSessionMode:SessionModeScheduled];
    } else if ((isPublic && isIndividual) || (isPublic && isGroup) ){
        [self setSessionMode:SessionModeAll];
    } else if (isPublic) {
        [self setSessionMode:SessionModePublic];
    } else if (isIndividual || isGroup) {
        [self setSessionMode:SessionModeScheduled];
    }
}

- (void)setCellSelectedWithSessions:(NSArray *)sessions
{
    BOOL isGroup = NO;
    BOOL isPublic = NO;
    BOOL isIndividual = NO;
    
    for (Session *session in sessions) {
        if (session.sessionIsPublic && [session.sessionType isEqualToString:@"group"]) {
            isPublic = YES;
            continue;
        }
        
        if ([session.sessionType isEqualToString:@"group"]) {
            isGroup = YES;
        }
        if ([session.sessionType isEqualToString:@"individual"]) {
            isIndividual = YES;
        }
    }
    if (isPublic && isIndividual && isGroup) {
        [self setSessionMode:SessionModeAll];
    } else if ( (isIndividual && isGroup)) {
        [self setSessionMode:SessionModeScheduled];
    } else if ((isPublic && isIndividual) || (isPublic && isGroup) ){
        [self setSessionMode:SessionModeAll];
    } else if (isPublic) {
        [self setSessionMode:SessionModePublic];
    } else if (isIndividual || isGroup) {
        [self setSessionMode:SessionModeScheduled];
    }
}


@end
