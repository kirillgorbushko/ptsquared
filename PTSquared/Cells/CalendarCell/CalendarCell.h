//
//  CalendarCell.h
//  testCalendar
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class CalendarDay;

typedef NS_ENUM(NSUInteger, SessionMode) {
    SessionModeScheduled,
    SessionModePublic,
    SessionModeAll,
    SessionModeIndicate,
    SessionModeNone
};

@interface CalendarCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *dayNumberLabel;
@property (weak, nonatomic) IBOutlet UIView *daySelectionView;

- (void)setSessionMode:(SessionMode)mode;
- (void)setCellSelected:(BOOL)selected;
- (void)setCellSelectedWithDay:(CalendarDay *)selectedDay;

- (void)setCellSelectedWithSessions:(NSArray *)sessions;

@end
