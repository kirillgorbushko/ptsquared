//
//  WorkoutHeadTableViewCell.h
//  PT2
//
//  Created by Kirill Gorbushko on 29.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

@interface WorkoutHeadTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@end
