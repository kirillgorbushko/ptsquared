//
//  WorkoutHeadTableViewCell.m
//  PT2
//
//  Created by Kirill Gorbushko on 29.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "WorkoutHeadTableViewCell.h"

@interface WorkoutHeadTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

@implementation WorkoutHeadTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.headerTitleLabel.text = @"";
    self.separatorView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

@end