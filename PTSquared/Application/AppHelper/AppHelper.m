//
//  AppHelper.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 06.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AppHelper.h"
#import "MBProgressHUD.h"
#import "CredentialFetchService.h"
#import "ProfileManager.h"
#import "AppDelegate.h"
#import "CalendarEventSynchronizer.h"

static NSInteger const InternalApplicationErrorCode = 99999;

@implementation AppHelper

+ (BOOL)isNSStringIsValidEmail:(NSString *)stringToCheck applyFilter:(BOOL)filter
{
    BOOL stricterFilter = filter;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:stringToCheck];
}

+ (BOOL)isNSStringIsValidPhoneNumber:(NSString *)stringToCheck
{
    NSString *allowedSymbols = @"^[+]{0,}[1-9]{1}[0-9]{4,14}$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:stringToCheck];
}

+ (NSString *)appName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleNameKey];
}

+ (void)alertViewWithMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:[AppHelper appName] message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    });
}

+ (void)alertViewWithMessage:(NSString *)message delegate:(id /*<UIAlertViewDelegate>*/)alertViewDelegate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:[AppHelper appName] message:message delegate:alertViewDelegate cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    });
}

+ (void)alertViewWithMessage:(NSString *)message delegate:(id /*<UIAlertViewDelegate>*/)alertViewDelegate otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:[AppHelper appName] message:message delegate:alertViewDelegate cancelButtonTitle:@"OK" otherButtonTitles:otherButtonTitles, nil] show];
    });
}


+ (NSError *)errorWithDescription:(NSString *)description inDomain:(NSString *)domain
{
    NSError *error = [NSError errorWithDomain:domain code:InternalApplicationErrorCode userInfo:@{ NSLocalizedDescriptionKey : description }];
    return error;
}

+ (void)showLoader
{
    [MBProgressHUD showHUDAddedTo:[AppHelper topView] animated:YES].dimBackground = YES;
}

+ (void)hideLoader
{
    [MBProgressHUD hideAllHUDsForView:[AppHelper topView] animated:YES];
}

+ (void)showLoaderOnView:(UIView *)view
{
    [MBProgressHUD showHUDAddedTo:view animated:YES].color = [[ColorSchemeManager sharedManager].interfaceColor colorWithAlphaComponent:0.3f];
}

+ (UIView *)topView
{
    return [UIApplication sharedApplication].keyWindow;
}

+ (void)resetAllSettings
{
    [[CredentialFetchService service] setNeedLoginState];
    [ProfileManager sharedManager].profileType = UNDEFINED;
    [[CalendarEventSynchronizer sharedInstance] dispatchCurrentCalendar];
}

+ (void)prepareInformationButtons:(NSArray *)buttons
{
    for (UIButton *button in buttons) {
        button.layer.cornerRadius = 5.f;
        button.layer.borderWidth = 1.5f;
        button.layer.borderColor = [UIColor grayColor].CGColor;
    }
}

+ (NSString *)NSDocumentsDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

+ (UIViewController *)rootViewController
{
    return  ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController;
}

@end
