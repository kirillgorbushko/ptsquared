//
//  AppHelper.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 06.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface AppHelper : NSObject

+ (NSError *)errorWithDescription:(NSString *)description inDomain:(NSString *)domain;
+ (BOOL)isNSStringIsValidEmail:(NSString *)stringToCheck applyFilter:(BOOL)filter;
+ (BOOL)isNSStringIsValidPhoneNumber:(NSString *)stringToCheck;

+ (void)alertViewWithMessage:(NSString *)message delegate:(id /*<UIAlertViewDelegate>*/)alertViewDelegate otherButtonTitles:(NSString *)otherButtonTitles, ...;
+ (void)alertViewWithMessage:(NSString *)message delegate:(id /*<UIAlertViewDelegate>*/)alertViewDelegate;
+ (void)alertViewWithMessage:(NSString *)message;

+ (NSString *)NSDocumentsDirectoryPath;
+ (NSString *)appName;

+ (void)showLoader;
+ (void)hideLoader;
+ (void)showLoaderOnView:(UIView *)view;
+ (UIView *)topView;

+ (UIViewController *)rootViewController;

+ (void)resetAllSettings;
+ (void)prepareInformationButtons:(NSArray *)buttons;

@end
