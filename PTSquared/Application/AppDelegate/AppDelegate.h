//
//  AppDelegate.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 23.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AutoLogin.h"

static NSString *const NotificationKeyNeedUpdateUserProfileData = @"NotificationKeyNeedUpdateUserProfileData";

@interface AppDelegate : UIResponder <UIApplicationDelegate, AutoLoginDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)showLoginViewController;

@end

