//
//  AppDelegate.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 23.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "ModeSelectionViewController.h"
#import "Session.h"
#import "GoogleAnaliticsManager.h"
#import "CalendarEventSynchronizer.h"
#import "ContainerViewController.h"
#import "CredentialFetchService.h"
#import "HomeViewController.h"
#import "SwitchViewController.h"

static NSString *const WorkoutPushIdentifier = @"WORKOUT_CATEGORY";
static NSString *const NutritionPushIdentifier = @"NUTRITION_CATEGORY";
static NSString *const NotificationViewButtonTitle = @"View it!";

@interface AppDelegate()

@property (copy, nonatomic) NSString *pushIdentifier;
@property (strong, nonatomic) AutoLogin *autoLogger;

@end

@implementation AppDelegate

#pragma mark - LifeCycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[NetworkManager sharedManager] startMonitoringNetwork];
    [CalendarEventSynchronizer sharedInstance];
    [GMSServices provideAPIKey:GoogleMapsAPIKey];
    [self registerForRemoteNotification];
    [self performAutoLogin];

    [[GoogleAnaliticsManager sharedManager] saveLaunchTime];
    return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSDKAppEvents activateApp];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[GoogleAnaliticsManager sharedManager] saveCloseTime];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[GoogleAnaliticsManager sharedManager] saveCloseTime];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

#pragma mark - PushNotifications

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    [NetworkManager sharedManager].deviceTokenForRemoteNotification = token;
    [[CredentialFetchService service] saveClientPushToken:token];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%s  %@", __PRETTY_FUNCTION__, error.localizedDescription);
    [[[UIAlertView alloc] initWithTitle:[AppHelper appName] message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if (application.applicationState == UIApplicationStateActive) {
        [self updateAction];

        if ([userInfo allKeys].count && [NetworkManager sharedManager].isUserLoggined) {
            NSString *alert = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:[AppHelper appName] message:alert delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            });

            if ([[[alert componentsSeparatedByString:@" "] lastObject] isEqualToString:@"workout"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationKeyDidReceivedNewWorkoutCategory object:nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationKeyDidReceivedNewNutritionCategory object:nil];
            }
        }
    }
#if DEBUG
    NSLog(@"UserInfo: %@", userInfo);
#endif
}

- (void)updateAction
{
    [[ColorSchemeManager sharedManager] updateColorsService];
    
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topViewController = ((ContainerViewController *)[topController presentedViewController]).navigationController.topViewController;
    if ([topViewController isKindOfClass:[HomeViewController class]]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationKeyNeedUpdateUserProfileData object:nil];
    }
}

#ifdef __IPHONE_8_0

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)notification completionHandler:(void (^)())completionHandler
{
    if ([identifier isEqualToString:@"ACCEPT_IDENTIFIER"]) {
        self.pushIdentifier = [[notification valueForKey:@"aps"] valueForKey:@"category"];
        if (self.autoLogger.loginned) {
            [self showPushRequiredController];
        } else {
            [self performAutoLogin];
        }
    }
    if (completionHandler) {
        completionHandler();
    }
}

#endif

#pragma mark - Notifications

- (void)registerForRemoteNotification
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        NSSet *categories = [self prepareNotificationActions];
        UIUserNotificationType types = (UIUserNotificationTypeAlert| UIUserNotificationTypeSound| UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types
                                                     categories:categories];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        //        hide this warning - support for ios7
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge)];
#pragma clang diagnostic pop
    }
}

- (void)scheduleNotificationWithSession:(Session *)session
{
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    NSDate *fireDate = [session.sessionDate dateByAddingTimeInterval:+ 60];
    
    localNotification.fireDate = fireDate;
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.alertBody = [NSString stringWithFormat:@"Event - %@ with trainer %@ will start in 15 minutes", session.sessionName, session.sessionTrainerName];
    localNotification.alertTitle = [AppHelper appName];
    localNotification.alertAction = @"OK";
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)cancellAllLocalNotifications
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (NSSet *)prepareNotificationActions
{
    UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
    acceptAction.identifier = @"ACCEPT_IDENTIFIER";
    acceptAction.title = NotificationViewButtonTitle;
    acceptAction.activationMode = UIUserNotificationActivationModeForeground;
    acceptAction.destructive = NO;
    acceptAction.authenticationRequired = NO;
    
    UIMutableUserNotificationCategory *workoutCategory = [[UIMutableUserNotificationCategory alloc] init];
    workoutCategory.identifier = WorkoutPushIdentifier;
    [workoutCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    [workoutCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextMinimal];
    
    UIMutableUserNotificationCategory *nutritionCategory = [[UIMutableUserNotificationCategory alloc] init];
    nutritionCategory.identifier = NutritionPushIdentifier;
    [nutritionCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    [nutritionCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextMinimal];
    
    NSSet *categories = [NSSet setWithArray: @[workoutCategory, nutritionCategory]];
    return categories;
}

#pragma mark - Private

- (void)setRootViewController:(UIViewController *)viewController
{
    self.window.rootViewController = viewController;
    [self.window makeKeyAndVisible];
}

- (void)showLoginViewController
{
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UINavigationController *startLoginViewController = [loginStoryboard instantiateViewControllerWithIdentifier:@"navigationController"];
    [self setRootViewController:startLoginViewController];
}

- (void)performAutoLogin
{
    if (!self.autoLogger) {
        self.autoLogger = [[AutoLogin alloc] init];
    }
    self.autoLogger.delegate = self;
    [self.autoLogger performAutoLoginIfPossible];
}

- (void)didFinishAutoLogin:(ProfileType)type
{
    if(!type && !self.pushIdentifier) {
        [self showLoginViewController];
    } else {
        [self showPushRequiredController];
    }
}

- (void)showPushRequiredController
{
    if ([self.pushIdentifier isEqualToString:WorkoutPushIdentifier]) {
        UIViewController *container = [AppHelper rootViewController].presentedViewController;
        if ([container isKindOfClass:[ContainerViewController class]]) {
            [((ContainerViewController *)container) selecItem:2];
        }
    } else if ([self.pushIdentifier isEqualToString:NutritionPushIdentifier]) {
        UIViewController *container = [AppHelper rootViewController].presentedViewController;
        if ([container isKindOfClass:[ContainerViewController class]]) {
            [((ContainerViewController *)container) selecItem:3];
        }
    }
}

@end