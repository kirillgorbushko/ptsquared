//
//  AppStyle.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 07.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//
#pragma mark - Defines

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

//comment for disable hidding statusBar on Informations screens
//#define HIDE_STATUS_BAR_ON_INFO

#pragma mark - Notifications

static NSString *const NotificationKeyDidReceivedNewWorkoutCategory = @"NotificationKeyDidReceivedNewWorkoutCategory";
static NSString *const NotificationKeyDidReceivedNewNutritionCategory = @"NotificationKeyDidReceivedNewNutritionCategory";

#pragma mark - Keys

static NSString *const GoogleMapsAPIKey = @"AIzaSyCnGvClbfz75Inc46FB5fmS-weJZkgLe0I";

#pragma mark - Messages

static NSString *const AlertMessageInvalidInputParameters = @"You must enter both email and password.";
static NSString *const AlertMessageEmptyFacebookValidationCode = @"You must enter a Validation Code. If you don't have one, ask your PT.";
static NSString *const AlertMessageInvalidFacebookValidationCode = @"The Validation Code is invalid. Please try again or ask your PT to confirm the code.";
static NSString *const AlertMessageInvalidEmail = @"Email is not valid.";
static NSString *const AlertMessageEmptyPassword = @"You must enter a password.";
static NSString *const AlertMessageInvalidPassword = @"The same password must be entered in both fields.";
static NSString *const AlertMessagePasswordTooSmall = @"Password must be at least 6 characters";
static NSString *const AlertMessageInvalidValidationCode = @"Please enter validation code.";
static NSString *const AlertMessageFacebookSharingSuccessful = @"Session successfully shared on Facebook.";
static NSString *const AlertMessageFacebookSharingCanceled = @"Session sharing wars canceled.";
static NSString *const AlertMessageFacebookSharingFinishedWithError = @"An error occured during sharing on Facebook ";
static NSString *const AlertMessageLogoutError = @"Some error occured during logout. Please try again";
static NSString *const AlertMessageLoginError = @"Cant login. Please try again later";
static NSString *const AlertMessageLoginErrorFacebook = @"This Facebook account is not registered with PT2. Please register first and try again.";
static NSString *const AlertMessageSuccessfullRegistration = @"Registration successfull. You now can login.";
static NSString *const AlertMessageErrorRegistration = @"Cant register new user. Please try again later.";
static NSString *const AlertMessageUserNotConnectedWithFacebook = @"Can`t login in to Facebook.";
static NSString *const AlertMessageNoInternetConnection = @"Internet connection are lost. Please try again later or connect your phone to network and try again.";
static NSString *const AlertMessageWorkoutHasBeenRemoved = @"The linked workout has been removed";
static NSString *const AlertMEssageNoCameraAccessGranted = @"This app requires access to your device's Camera's.\n\nPlease enable Camera access for this app in Settings / Privacy / Camera";

static NSString *const NotificationMessageRequestWasSentSuccessfully = @"Your request was successfully completed.";
static NSString *const NotificationMessageRequestWasSentWithError = @"Cant proceed request. Please try again later.";
static NSString *const NotificationMessageCompleteSessionRequestError = @"Cant complete session. Please try again later.";
static NSString *const NotificationMessageJoinToSession = @"Are you sure you want to join this session?";

#pragma mark - Fonts

static NSString *const FontProximaNovaSemibold = @"ProximaNova-Semibold";
static NSString *const FontProximaNovaRegular = @"ProximaNovaA-Regular";
static NSString *const FontProximaNovaLight = @"ProximaNovaA-Light";

#pragma mark - Const

static NSString *const KeyCalcSessionScan = @"SessionScanCalculation";
static NSString *const PersonPlaceholderFileName = @"avatar";

