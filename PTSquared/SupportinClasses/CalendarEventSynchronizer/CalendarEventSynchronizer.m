//
//  CalendarEventSynchronizer.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 11.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "CalendarEventSynchronizer.h"
#import <EventKit/EventKit.h>
#import "ProfileManager.h"
#import "Session.h"

static NSString *const TrainerCalendarName = @"PT2 Trainer Calendar";
static NSString *const ClientCalendarName = @"PT2 Client Calendar";

static NSString *const TrainerCalendarCreatedLocal = @"PT2 Trainer Calendar Created local";
static NSString *const TrainerCalendarCreatediCloud = @"PT2 Trainer Calendar Created iCloud";
static NSString *const ClientCalendarCreatedLocal = @"PT2 Client Calendar Created local";
static NSString *const ClientCalendarCreatediCloud = @"PT2 Client Calendar Created iCloud";

static NSString *const ClientLocalCalendarCreated = @"ClientLocalCalendarCreated";
static NSString *const TrainerLocalCalendarCreated = @"TrainerLocalCalendarCreated";

@interface CalendarEventSynchronizer()

@property (strong, nonatomic) EKEventStore *eventStore;
@property (strong, nonatomic) __block EKCalendar *currentCalendar;
@property (assign, nonatomic) __block BOOL isCreateInProgress;

@end

@implementation CalendarEventSynchronizer

#pragma mark - Public

+ (instancetype)sharedInstance
{
    static CalendarEventSynchronizer *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[CalendarEventSynchronizer alloc] init];
    });
    return sharedManager;
}

- (void)dispatchCurrentCalendar
{
    self.currentCalendar = nil;
    self.isCreateInProgress = NO;
}

- (void)requestAccess:(void (^)(BOOL granted, NSError *error))success
{
    [[CalendarEventSynchronizer sharedInstance].eventStore requestAccessToEntityType:EKEntityTypeEvent completion:success];
}

- (void)synchronizeCalendarEventsWithSessions:(NSArray *)sessions inRangeFromDate:(NSDate *)fromDate toDate:(NSDate *)endDate calendarType:(CalendarSynchronizerType)type
{
    if (self.isCreateInProgress) {
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    void (^PerformSync)() = ^{
        if (!weakSelf.currentCalendar) {
            weakSelf.currentCalendar = [weakSelf getLocalEventCalendarsForType:type];
        }
        if (weakSelf.currentCalendar) {
            [weakSelf performSyncForSessions:sessions withEventsFromCalendar:weakSelf.currentCalendar inRangeWithStartDate:fromDate endDate:endDate];
        } else {
            //cant get local calendar and iCloudCalendar
        }
    };
    
    [self requestAccess:^(BOOL granted, NSError *error) {
        if (granted) {
            weakSelf.isCreateInProgress = YES;
            PerformSync();
        }
    }];
}

#pragma mark - Private

#pragma mark - SyncMethods

- (void)performSyncForSessions:(NSArray *)sessions withEventsFromCalendar:(EKCalendar *)selectedCalendar inRangeWithStartDate:(NSDate *)startDate endDate:(NSDate *)dateEnd
{
    NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sessionId" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedSessions = [sessions sortedArrayUsingDescriptors:sortDescriptors];

    NSPredicate *searchRequestPredicate = [[CalendarEventSynchronizer sharedInstance].eventStore predicateForEventsWithStartDate:startDate endDate:dateEnd calendars:@[selectedCalendar]];
    NSArray *requestedEvents = [[CalendarEventSynchronizer sharedInstance].eventStore eventsMatchingPredicate:searchRequestPredicate];
    //clean if events created in calendar but no events in schedule
    if (requestedEvents.count && !sortedSessions.count) {
        for (int i = 0; i < requestedEvents.count; i++) {
            [self removeEvent:requestedEvents[i]];
        }
    } else {
        if (requestedEvents.count) {
            NSMutableArray *sessionCopy = [sortedSessions mutableCopy];
            //remove saved events in calendar from list of new events to add
            for (int s = 0; s < requestedEvents.count; s++) {
                for (int i = 0; i < sortedSessions.count; i++) {
                    if ([self isEvent:requestedEvents[s] isEqualToSession:sortedSessions[i] performUpdateIfNeeded:YES]) {
                        [sessionCopy removeObject:sortedSessions[i]];
                    }
                }
            }
            //save not added events to calendar
            if (sessionCopy.count) {
                for (int i = 0; i < sessionCopy.count; i++) {
                    [self saveEventWithSession:sessionCopy[i] inCalendar:selectedCalendar];
                }
            }
            //remove events in calendar that not actual more
            NSMutableArray *requestedEventsCopy = [requestedEvents mutableCopy];
            for (int s = 0; s < requestedEvents.count; s++) {
                for (int i = 0; i < sortedSessions.count; i++) {
                    if ([self isEvent:requestedEvents[s] isEqualToSession:sortedSessions[i] performUpdateIfNeeded:NO]) {
                        [requestedEventsCopy removeObject:requestedEvents[s]];
                    }
                }
            }
            for (int i = 0; i < requestedEventsCopy.count; i++) {
                [self removeEvent:requestedEventsCopy[i]];
            }
        } else {
            //if zero session fetched add all new session to calendar
            for (int i = 0; i < sortedSessions.count; i++) {
                [self saveEventWithSession:sortedSessions[i] inCalendar:selectedCalendar];
            }
        }
    }
    
    self.isCreateInProgress = NO;
}

#pragma mark - CalendarEvents Methods

- (void)removeEvent:(EKEvent *)eventTorRemove
{
    NSError *error = nil;

    if ([[CalendarEventSynchronizer sharedInstance].eventStore eventWithIdentifier:eventTorRemove.eventIdentifier]) {
        [[CalendarEventSynchronizer sharedInstance].eventStore removeEvent:eventTorRemove span:EKSpanThisEvent error:&error];
    }
}

- (void)saveEventWithSession:(Session *)sessionToStore inCalendar:(EKCalendar *)calendar
{
    if (!sessionToStore.sessionIsClientJoined && [ProfileManager sharedManager].profileType == CLIENT){
        return;
    }
    
    EKEvent *event = [EKEvent eventWithEventStore:[CalendarEventSynchronizer sharedInstance].eventStore];
    event.calendar = calendar;
    event.title = sessionToStore.sessionName;
    event.location = sessionToStore.sessionAddress;
    event.startDate = sessionToStore.sessionDate;
    
    NSDate *endDate = [sessionToStore.sessionDate dateByAddingTimeInterval:(sessionToStore.sessionDuration * 60)];
    event.endDate = endDate;
    
    NSString *eventNote = [NSString stringWithFormat:@"%i-%@", (int)sessionToStore.sessionId, sessionToStore.sessionName];
    event.notes = eventNote;
    
    NSError *error = nil;
    [[CalendarEventSynchronizer sharedInstance].eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
}

- (BOOL)isEvent:(EKEvent *)eventToCompare isEqualToSession:(Session *)sessionToCompare performUpdateIfNeeded:(BOOL)needPerformUpdate
{
    BOOL equal = NO;
    NSString *sessionID = [NSString stringWithFormat:@"%i", (int)sessionToCompare.sessionId];
    NSString *eventSessionID = [eventToCompare.notes substringToIndex:2];
    
    if ([eventToCompare.startDate isEqualToDate:sessionToCompare.sessionDate] && [sessionID isEqualToString:eventSessionID]) {
        if (needPerformUpdate) {
            [self updateIfNeededEvent:eventToCompare withSession:sessionToCompare];
        }
        equal = YES;
    }
    return equal;
}

- (void)updateIfNeededEvent:(EKEvent *)eventToCompare withSession:(Session *)sessionToCompare
{
    BOOL needSave = NO;
    if (![eventToCompare.title isEqualToString:sessionToCompare.sessionName]) {
        eventToCompare.title = sessionToCompare.sessionName;
        needSave = YES;
    }
    if (![eventToCompare.location isEqualToString:sessionToCompare.sessionAddress]) {
        eventToCompare.location = sessionToCompare.sessionAddress;
        needSave = YES;
    }
    
    if (needSave) {
        NSError *error = nil;
        [[CalendarEventSynchronizer sharedInstance].eventStore saveEvent:eventToCompare span:EKSpanThisEvent commit:YES error:&error];
    }
}

- (EKCalendar *)getLocalEventCalendarsForType:(CalendarSynchronizerType)calendarType
{
    NSArray *allCalendars = [[CalendarEventSynchronizer sharedInstance].eventStore calendarsForEntityType:EKEntityTypeEvent];
    EKCalendar *requestedCalendar;
    
    if (!requestedCalendar) {
        if ([self isiCloudCalendarCreatedForType:calendarType]) {
            for (EKCalendar *calendar in allCalendars) {
                if ([calendar.title isEqualToString:[self calendarNameWithType:calendarType]]) {
                    requestedCalendar = calendar;
                    break;
                }
            }
            if (!requestedCalendar) {
                requestedCalendar = [self createCalendar:calendarType withSourceType:EKSourceTypeCalDAV];
            }
        } else {
            requestedCalendar = [self createCalendar:calendarType withSourceType:EKSourceTypeCalDAV];
        }
    }

    
    if (!requestedCalendar) {
        if ([self isLocalCalendarCreatedForType:calendarType]) {
            for (EKCalendar *calendar in allCalendars) {
                if ([calendar.title isEqualToString:[self calendarNameWithType:calendarType]]) {
                    requestedCalendar = calendar;
                    break;
                }
            }
            
            if (!requestedCalendar) {
                requestedCalendar = [self createCalendar:calendarType withSourceType:EKSourceTypeLocal];
            }
        } else {
            requestedCalendar = [self createCalendar:calendarType withSourceType:EKSourceTypeLocal];
        }
    }

    return requestedCalendar;
}

- (EKCalendar *)createCalendar:(CalendarSynchronizerType)calendarType withSourceType:(EKSourceType)sourceType
{
    EKCalendar *calendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:[CalendarEventSynchronizer sharedInstance].eventStore];
    calendar.title = [self calendarNameWithType:calendarType];
    
    if (calendarType == CalendarSynchronizerTypePersonalTrainer) {
        calendar.CGColor = [UIColor interfaceLightBlueColor].CGColor;
    } else if (calendarType == CalendarSynchronizerTypeClient) {
        calendar.CGColor = [UIColor interfaceYellowColor].CGColor;
    }
    
    for (EKSource *source in [CalendarEventSynchronizer sharedInstance].eventStore.sources) {
        if (sourceType == EKSourceTypeCalDAV) {
            if (source.sourceType == EKSourceTypeCalDAV && [source.title isEqualToString:@"iCloud"]) {
                calendar.source = source;
                break;
            }
        } else if (sourceType == EKSourceTypeLocal) {
            if (source.sourceType == EKSourceTypeLocal) {
                calendar.source = source;
                break;
            } else {
                return nil;
            }
        }
    }
        
    NSError *error = nil;
    BOOL result = [[CalendarEventSynchronizer sharedInstance].eventStore saveCalendar:calendar commit:YES error:&error];
    if (result) {
        if (sourceType == EKSourceTypeCalDAV) {
            [self setCreatedForiCloudCalendarForType:calendarType];
        } else if (sourceType == EKSourceTypeLocal) {
            [self setCreatedForLocalCalendarForType:calendarType];
        }
        NSLog(@"Saved calendar to event store. %@", calendar);
    } else {
        NSLog(@"Error saving calendar: %@.", error);
    }
    
    return calendar;
}

- (NSString *)calendarNameWithType:(CalendarSynchronizerType)calendarType
{
    NSString *calendarName;
    if (calendarType == CalendarSynchronizerTypeClient) {
        calendarName = ClientCalendarName;
    } else if (calendarType == CalendarSynchronizerTypePersonalTrainer) {
        calendarName = TrainerCalendarName;
    }
    return calendarName;
}

- (BOOL)isiCloudCalendarCreatedForType:(CalendarSynchronizerType)calendarType
{
    BOOL isCreated = NO;
    
    NSString *keyToCheck;
    if (calendarType == CalendarSynchronizerTypePersonalTrainer) {
        keyToCheck = TrainerCalendarCreatediCloud;
    } else if (calendarType == CalendarSynchronizerTypeClient) {
        keyToCheck = ClientCalendarCreatediCloud;
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:keyToCheck]) {
        isCreated = [[NSUserDefaults standardUserDefaults] boolForKey:keyToCheck];
    }
    
    return isCreated;
}

- (BOOL)isLocalCalendarCreatedForType:(CalendarSynchronizerType)calendarType
{
    BOOL isCreated = NO;
    
    NSString *keyToCheck;
    if (calendarType == CalendarSynchronizerTypePersonalTrainer) {
        keyToCheck = TrainerCalendarCreatedLocal;
    } else if (calendarType == CalendarSynchronizerTypeClient) {
        keyToCheck = ClientCalendarCreatedLocal;
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:keyToCheck]) {
        isCreated = [[NSUserDefaults standardUserDefaults] boolForKey:keyToCheck];
    }
    
    return isCreated;
}

- (void)setCreatedForiCloudCalendarForType:(CalendarSynchronizerType)calendarType
{
    if (calendarType == CalendarSynchronizerTypePersonalTrainer) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:TrainerCalendarCreatediCloud];
    } else if (calendarType == CalendarSynchronizerTypeClient) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ClientCalendarCreatediCloud];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setCreatedForLocalCalendarForType:(CalendarSynchronizerType)calendarType
{
    if (calendarType == CalendarSynchronizerTypePersonalTrainer) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:TrainerCalendarCreatedLocal];
    } else if (calendarType == CalendarSynchronizerTypeClient) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ClientCalendarCreatedLocal];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)prepareEventStore
{
    self.eventStore = [[EKEventStore alloc] init];
}

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self prepareEventStore];
    }
    return self;
}

@end