//
//  CalendarEventSynchronizer.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 11.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CalendarSynchronizerType) {
    CalendarSynchronizerTypeUndefined,
    CalendarSynchronizerTypeClient,
    CalendarSynchronizerTypePersonalTrainer,
};

@interface CalendarEventSynchronizer : NSObject

+ (instancetype)sharedInstance;

- (void)dispatchCurrentCalendar;

- (void)synchronizeCalendarEventsWithSessions:(NSArray *)sessions inRangeFromDate:(NSDate *)fromDate toDate:(NSDate *)endDate calendarType:(CalendarSynchronizerType)type;
- (void)requestAccess:(void (^)(BOOL granted, NSError *error))success;

@end
