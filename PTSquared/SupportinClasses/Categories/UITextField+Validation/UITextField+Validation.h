//
//  UITextField+Validation.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 20.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface UITextField (Validation)

- (void)textFieldDidChangedText;

@end
