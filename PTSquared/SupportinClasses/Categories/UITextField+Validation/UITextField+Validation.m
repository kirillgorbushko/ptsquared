//
//  UITextField+Validation.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 20.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "UITextField+Validation.h"

@implementation UITextField (Validation)

#pragma mark - Public

- (void)textFieldDidChangedText
{
    if (self.text.length) {
        if ([AppHelper isNSStringIsValidEmail:self.text applyFilter:NO]) {
            [self setGreenImage];
        } else {
            [self setRedImage];
        }
    } else {
        [self setWhiteImage];
    }
}

#pragma mark - Private

- (void)setGreenImage
{
    self.leftView.tintColor = [UIColor textFieldIconGreenColor];
}

- (void)setRedImage
{
    self.leftView.tintColor = [UIColor textFieldIconRedColor];
}

- (void)setWhiteImage
{
    self.leftView.tintColor = [UIColor whiteColor];
}

@end
