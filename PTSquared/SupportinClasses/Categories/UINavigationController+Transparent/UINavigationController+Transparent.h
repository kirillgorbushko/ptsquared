//
//  UINavigationController+Transparent.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 23.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Transparent)

- (void)presentTransparentNavigationBar;
- (void)hideTransparentNavigationBar;

@end
