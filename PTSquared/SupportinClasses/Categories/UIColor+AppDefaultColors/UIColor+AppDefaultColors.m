//
//  UIColor+AppDefaultColors.m
//  iRemember
//
//  Created by Mykhailo Glagola on 02.03.15.
//  Copyright (c) 2015 ThinkMobiles. All rights reserved.
//

#import "UIColor+AppDefaultColors.h"

@implementation UIColor (AppDefaultColors)

#pragma mark - LifeCycle

+ (UIColor *)transparentWhiteColor
{
    return [UIColor colorWithWhite:1.0 alpha:0.5];
}

+ (UIColor *)buttonHightlightedColor
{
    return [UIColor colorWithWhite:1.0 alpha:0.3];
}

+ (UIColor *)buttonFacebookColor
{
    return [UIColor colorWithRealRed:55 green:149 blue:194 alpha:1.];
}

+ (UIColor *)buttonFacebookHightlightedColor
{
    return [UIColor colorWithRealRed:55 green:149 blue:194 alpha:0.7];
}

+ (UIColor *)menuBlueColor
{
    return [UIColor colorWithRealRed:86 green:164 blue:195 alpha:0.3];
}

+ (UIColor *)dimmedColor
{
    return [UIColor colorWithRealRed:0 green:0 blue:0 alpha:0.6];
}

#pragma mark - InterfaceColors

+ (UIColor *)interfaceBurgundyColor
{
    return [UIColor colorWithRealRed:164 green:61 blue:69 alpha:1.];
}

+ (UIColor *)interfaceGreenColor
{
    return [UIColor colorWithRealRed:32 green:185 blue:152 alpha:1.];
}

+ (UIColor *)interfaceLightBlueColor
{
    return [UIColor colorWithRealRed:28 green:150 blue:235 alpha:1.];
}

+ (UIColor *)interfaceNavyColor
{
    return [UIColor colorWithRealRed:40 green:70 blue:205 alpha:1.];
}

+ (UIColor *)interfaceOrangeColor
{
    return [UIColor colorWithRealRed:182 green:90 blue:18 alpha:1.];
}

+ (UIColor *)interfacePurpleColor
{
    return [UIColor colorWithRealRed:127 green:0 blue:239 alpha:1.];
}

+ (UIColor *)interfaceRedColor
{
    return [UIColor colorWithRealRed:178 green:26 blue:25 alpha:1.];
}

+ (UIColor *)interfaceYellowColor
{
    return [UIColor colorWithRealRed:221 green:174 blue:19 alpha:1.];
}

+ (UIColor *)textFieldTintColor
{
    return [UIColor colorWithRealRed:183 green:199 blue:208 alpha:1.];
}

+ (UIColor *)textFieldIconGreenColor
{
    return [UIColor greenColor];
}

+ (UIColor *)textFieldIconRedColor
{
    return [UIColor redColor];
}

#pragma mark - Private

+ (UIColor *)colorWithRealRed:(int)red green:(int)green blue:(int)blue alpha:(float)alpha
{
    return [UIColor colorWithRed:((float)red)/255.f green:((float)green)/255.f blue:((float)blue)/255.f alpha:((float)alpha)/1.f];
}

@end
