//
//  UIColor+AppDefaultColors.h
//  iRemember
//
//  Created by Mykhailo Glagola on 02.03.15.
//  Copyright (c) 2015 ThinkMobiles. All rights reserved.
//

@interface UIColor (AppDefaultColors)

+ (UIColor *)transparentWhiteColor;
+ (UIColor *)buttonHightlightedColor;
+ (UIColor *)buttonFacebookHightlightedColor;
+ (UIColor *)buttonFacebookColor;
+ (UIColor *)menuBlueColor;
+ (UIColor *)dimmedColor;

+ (UIColor *)interfaceBurgundyColor;
+ (UIColor *)interfaceGreenColor;
+ (UIColor *)interfaceLightBlueColor;
+ (UIColor *)interfaceNavyColor;
+ (UIColor *)interfaceOrangeColor;
+ (UIColor *)interfacePurpleColor;
+ (UIColor *)interfaceRedColor;
+ (UIColor *)interfaceYellowColor;

+ (UIColor *)textFieldIconGreenColor;
+ (UIColor *)textFieldIconRedColor;
+ (UIColor *)textFieldTintColor;

@end
