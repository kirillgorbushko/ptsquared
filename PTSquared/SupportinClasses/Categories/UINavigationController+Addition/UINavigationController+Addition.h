//
//  UINavigationController+Addition.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 23.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, BackButtonStyle) {
    BackButtonStyleLight,
    BackButtonStyleDark
};

@interface UINavigationController (Addition)

- (void)setBackButtonStyle:(BackButtonStyle)style;
- (void)setRightButtonWithImageNamed:(NSString *)image andDelegate:(id)delegate;
- (void)setRightButtonWithText:(NSString *)textToShow andDelegate:(id)delegate;

- (void)setNavigationBarColor:(UIColor *)color;
- (void)setStatusBarColor:(UIColor *)color;

@end
