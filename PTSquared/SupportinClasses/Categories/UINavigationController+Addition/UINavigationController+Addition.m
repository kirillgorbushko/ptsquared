//
//  UINavigationController+Addition.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 23.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "UINavigationController+Addition.h"

static CGFloat const DefaultButtonWidth = 20;

@implementation UINavigationController (Addition)

#pragma mark - Public

- (void)setBackButtonStyle:(BackButtonStyle)style
{
    NSString *imageName;
    UIColor *barTintColor;
    switch (style) {
        case BackButtonStyleDark: {
            imageName = @"Back_Arrow_Black";
            barTintColor = [UIColor blackColor];
            break;
        }
        case BackButtonStyleLight: {
            imageName = @"Back_Arrow_White";
            barTintColor = [UIColor whiteColor];
            break;
        }
    }
    
    UIImage *img = [UIImage imageNamed:imageName];
    self.navigationBar.tintColor = barTintColor;
    [[UINavigationBar appearance] setTintColor:barTintColor];
    
    self.navigationItem.backBarButtonItem.title = @"";
    self.navigationBar.backIndicatorImage = img;
    self.navigationBar.backIndicatorTransitionMaskImage = img;
    
    if ([self.navigationBar.topItem isKindOfClass:[UINavigationItem class]]) {
        self.navigationBar.topItem.title = @"";
    }
}

/** if used this method should also use method with selector 'rightButtonPressed' */
- (void)setRightButtonWithImageNamed:(NSString *)image andDelegate:(id)delegate
{
    CGSize navBarSize = self.navigationBar.frame.size;
    CGRect buttonRect = CGRectMake(0, 0, DefaultButtonWidth,  navBarSize.height);
    UIButton *button = [[UIButton alloc] initWithFrame:buttonRect];
    [button setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [button addTarget:delegate action:NSSelectorFromString(@"rightButtonPressed") forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    ((UIViewController *)delegate).navigationItem.rightBarButtonItem = rightButton;
}

- (void)setRightButtonWithText:(NSString *)textToShow andDelegate:(id)delegate
{
    CGSize navBarSize = self.navigationBar.frame.size;
    CGRect buttonRect = CGRectMake(0, 0, DefaultButtonWidth * 3,  navBarSize.height);
    UIButton *button = [[UIButton alloc] initWithFrame:buttonRect];
    [button setTitle:textToShow forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:FontProximaNovaRegular size:14.f];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:delegate action:NSSelectorFromString(@"rightButtonPressed") forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    ((UIViewController *)delegate).navigationItem.rightBarButtonItem = rightButton;
}

- (void)setNavigationBarColor:(UIColor *)color
{
    self.navigationBar.translucent = NO;
    self.navigationBar.barTintColor = color;
    self.navigationBar.tintColor = color;
}

- (void)setStatusBarColor:(UIColor *)color
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    id statusBarWindow = [[UIApplication sharedApplication] valueForKey:@"_statusBarWindow"];
    NSString *statusBarWindowClassString = NSStringFromClass([statusBarWindow class]);
    if ([statusBarWindowClassString isEqualToString:@"UIStatusBarWindow"]) {
        NSArray *statusBarWindowSubviews = [statusBarWindow subviews];
        for (UIView *statusBarWindowSubview in statusBarWindowSubviews) {
            NSString *statusBarWindowSubviewClassString = NSStringFromClass([statusBarWindowSubview class]);
            if ([statusBarWindowSubviewClassString isEqualToString:@"UIStatusBar"]) {
                [statusBarWindowSubview setBackgroundColor:color];
            }
        }
    }
}

@end
