//
//  GoogleMapsProvider.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "GoogleMapsProvider.h"

@implementation GoogleMapsProvider

#pragma mark - Public

+ (void)setupMapOnView:(GMSMapView *)view setMarkerOnLatitude:(CLLocationDegrees)latitude andLongtitude:(CLLocationDegrees)longtitude
{
    GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:latitude longitude:longtitude zoom:16];
    view.camera = cameraPosition;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(latitude, longtitude);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = view;
}

@end
