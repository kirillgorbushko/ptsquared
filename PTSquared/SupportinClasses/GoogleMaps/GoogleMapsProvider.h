//
//  GoogleMapsProvider.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@class GMSMapView;

typedef void(^LocationName)(NSString *location, BOOL success, NSError *error);

@interface GoogleMapsProvider : NSObject

+ (void)setupMapOnView:(GMSMapView *)view setMarkerOnLatitude:(CLLocationDegrees)latitude andLongtitude:(CLLocationDegrees)longtitude;

@end
