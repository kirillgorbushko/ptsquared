//  Created by Kirill Gorbushko on 26.01.15.
//  Copyright (c) 2015 ThinkMobiles. All rights reserved.
//

#import "LeftImageViewTextField.h"
#import "UIColor+AppDefaultColors.h"

static CGFloat const TextOffset = 50;

@implementation LeftImageViewTextField

#pragma mark - Public

+ (void)setupTextField:(LeftImageViewTextField *)textField;
{
    if (textField.leftImageName.length) {
        UIImage *leftImage = [[UIImage imageNamed:textField.leftImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        textField.leftViewMode = UITextFieldViewModeAlways;
        textField.leftView = [[UIImageView alloc] initWithImage:leftImage];
        textField.leftView.tintColor = [UIColor whiteColor];
        textField.tintColor = [UIColor textFieldTintColor];
    }
}

- (void)setPlaceholderColor:(UIColor *)placholderColor withPlacholderText:(NSString *)text
{
    if ([self respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor blackColor];
        if (text.length) {
            self.placeholder = text;
        }
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    }
}

#pragma mark - Ovverided

- (CGRect)leftViewRectForBounds:(CGRect)bounds
{
    [super leftViewRectForBounds:bounds];
    CGRect leftViewFrame = self.leftView.frame;
    NSInteger yPosition = self.frame.size.height / 2 - leftViewFrame.size.height / 2;
    CGRect leftBounds = CGRectMake(0 + self.leftViewOffset, yPosition, leftViewFrame.size.width, leftViewFrame.size.height);
    return leftBounds;
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    [super placeholderRectForBounds:bounds];
    return CGRectInset(bounds,TextOffset, 0);
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    [super textRectForBounds:bounds];
    return CGRectInset(bounds, TextOffset, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    [super editingRectForBounds:bounds];
    return CGRectInset(bounds, TextOffset, 0);
}

#pragma mark - UIKeyInput

- (void)deleteBackward
{
    [super deleteBackward];
    
    if (self.subDelegate && [self.subDelegate respondsToSelector:@selector(textFieldDidDelete:)]){
        [self.subDelegate textFieldDidDelete:self];
    }
}

@end
