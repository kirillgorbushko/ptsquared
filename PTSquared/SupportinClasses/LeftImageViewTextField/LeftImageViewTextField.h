//
//  Created by Kirill Gorbushko on 26.01.15.
//  Copyright (c) 2015 ThinkMobiles. All rights reserved.
//

@protocol LeftImageViewTextFieldDelegate <NSObject>

@optional
- (void)textFieldDidDelete:(UITextField *)textField;
@end

IB_DESIGNABLE
@interface LeftImageViewTextField : UITextField

@property (weak, nonatomic) id <LeftImageViewTextFieldDelegate> subDelegate;

@property (assign, nonatomic) IBInspectable CGFloat leftViewOffset;
@property (copy, nonatomic) IBInspectable NSString *leftImageName;

+ (void)setupTextField:(LeftImageViewTextField *)textField;
- (void)setPlaceholderColor:(UIColor *)placholderColor withPlacholderText:(NSString *)text;

@end
