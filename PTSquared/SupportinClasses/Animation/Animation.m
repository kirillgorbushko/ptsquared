//
//  Animation.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Animation.h"

@implementation Animation

#pragma mark - Public

+ (CABasicAnimation *)fadeAnimFromValue:(CGFloat)fromValue to:(CGFloat)toValue delegate:(id)delegate
{
    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnimation.fromValue = @(fromValue);
    fadeAnimation.toValue = @(toValue);
    fadeAnimation.duration = 0.3f;
    if (delegate) {
        fadeAnimation.removedOnCompletion = NO;
        fadeAnimation.delegate = delegate;
    } else {
        fadeAnimation.removedOnCompletion = YES;
    }
    fadeAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    return fadeAnimation;
}

+ (void)imageChangeAnimationFromImage:(UIImage *)fromImage toNewImage:(UIImage *)toImage onImageView:(UIImageView *)imageView
{
    CABasicAnimation* fadeAnim = [CABasicAnimation animationWithKeyPath:@"contents"];
    fadeAnim.fromValue = (__bridge id)fromImage.CGImage;
    fadeAnim.toValue = (__bridge id)toImage.CGImage;
    fadeAnim.duration = 0.3f;
    fadeAnim.removedOnCompletion = YES;
    [imageView.layer addAnimation:fadeAnim forKey:@"contents"];
    imageView.layer.contents = (__bridge id)toImage.CGImage;
}

+ (void)setFadeTransitionAnimationForView:(UIView *)view subType:(NSString *)subType
{
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.type = kCATransitionFade;
    if (subType.length) {
        transition.subtype = subType;
        transition.type = kCATransitionMoveIn;
    }
    [view.window.layer addAnimation:transition forKey:kCATransition];
}

@end
