//
//  PTButton.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "PTButton.h"
#import "UIColor+AppDefaultColors.h"

@implementation PTButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setDefaultButtonStyle];
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    if (self.tag) {
        if (highlighted) {
            self.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
            self.tintColor = [UIColor grayColor];
        } else {
            self.backgroundColor = [UIColor clearColor];
            self.tintColor = [UIColor whiteColor];
        }
    } else {
        if (highlighted) {
            self.backgroundColor = [UIColor colorWithWhite:.7 alpha:1];
            self.tintColor = [UIColor grayColor];
        } else {
            self.backgroundColor = [UIColor clearColor];
            self.tintColor = [UIColor whiteColor];
        }
    }
}

#pragma mark - Public

- (void)setDefaultButtonStyle
{
    self.layer.borderColor = [UIColor transparentWhiteColor].CGColor;
    self.layer.borderWidth = 1.5;
}


@end
