//
//  Session.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 14.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Session.h"
#import "CalendarDataSource.h"

@implementation Session

#pragma mark - Public

+ (Session *)getNextSessionFromDictionary:(NSDictionary *)inputDictionary
{
    inputDictionary = [inputDictionary removeNullValues];
    
    CalendarDataSource *calendarDataSource = [[CalendarDataSource alloc] init];
    
    Session *session = [[Session alloc] init];
    session.sessionId = [[inputDictionary valueForKey:@"id"] integerValue];
    session.sessionTrainerID = [[inputDictionary valueForKey:@"trainer_id"] integerValue];
    session.sessionName = [inputDictionary valueForKey:@"name"];
    session.sessionType = [inputDictionary valueForKey:@"type"];
    session.sessionDate = [calendarDataSource dateFromStringWithUTCTimeZone:[inputDictionary valueForKey:@"date_time"]];
    session.sessionStartTime = [inputDictionary valueForKey:@"time"];
    session.sessionDuration = [[inputDictionary valueForKey:@"duration"] floatValue];
    session.sessionAddress = [inputDictionary valueForKey:@"address"];
    session.sessionLatitude = [[inputDictionary valueForKey:@"latitude"] floatValue];
    session.sessionLongtitude = [[inputDictionary valueForKey:@"longitude"] floatValue];
    session.sessionPrice = [[inputDictionary valueForKey:@"price"] integerValue];
    session.sessionIsPublic = [[inputDictionary valueForKey:@"is_public"] boolValue];
    session.sessionIsComplete = [[inputDictionary valueForKey:@"completed"] boolValue];
    session.sessionIsClientJoined = [[inputDictionary valueForKey:@"joined"] boolValue];
    session.sessionIsReoccuringSessionType = [[inputDictionary valueForKey:@"reoccurring"] boolValue];
    session.sessionCreatedDate = [calendarDataSource dateFromStringWithUTCTimeZone:[inputDictionary valueForKey:@"created_at"]];
    session.sessionUpdatedDate = [calendarDataSource dateFromStringWithUTCTimeZone:[inputDictionary valueForKey:@"updated_at"]];
    session.sessionReoccurence = [[inputDictionary valueForKey:@"reoccurence"] integerValue];
    session.sessionParentID = [[inputDictionary valueForKey:@"parent_id"] integerValue];
    
    return session;
}

+ (NSArray *)prepareArrayWithSessionFromResponseArray:(NSArray *)inputArray
{
    NSMutableArray *requestedArray = [[NSMutableArray alloc] init];
    if (inputArray.count) {
        for (NSDictionary *dic in inputArray) {
            Session *newSession = [Session getNextSessionFromDictionary:dic];
            [requestedArray addObject:newSession];
        }
    }
    return requestedArray;
}

@end
