//
//  Session.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 14.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Session : NSObject

@property (assign, nonatomic) NSInteger sessionId;
@property (assign, nonatomic) NSInteger sessionTrainerID;
@property (assign, nonatomic) NSInteger sessionPrice;
@property (assign, nonatomic) CLLocationDegrees sessionLatitude;
@property (assign, nonatomic) CLLocationDegrees sessionLongtitude;
@property (assign, nonatomic) CGFloat sessionDuration;
@property (copy, nonatomic) NSString *sessionStartTime;
@property (copy, nonatomic) NSString *sessionTrainerName;
@property (copy, nonatomic) NSString *sessionType;
@property (copy, nonatomic) NSString *sessionName;
@property (copy, nonatomic) NSString *sessionAddress;
@property (assign, nonatomic) BOOL sessionIsComplete;
@property (assign, nonatomic) BOOL sessionIsPublic;
@property (assign, nonatomic) BOOL sessionIsReoccuringSessionType;
@property (assign, nonatomic) BOOL sessionIsClientJoined;
@property (strong, nonatomic) NSDate *sessionDate;
@property (strong, nonatomic) NSDate *sessionCreatedDate;
@property (strong, nonatomic) NSDate *sessionUpdatedDate;
@property (assign, nonatomic) NSInteger sessionReoccurence;
@property (assign, nonatomic) NSInteger sessionParentID;

+ (Session *)getNextSessionFromDictionary:(NSDictionary *)inputDictionary;
+ (NSArray *)prepareArrayWithSessionFromResponseArray:(NSArray *)inputArray;

@end
