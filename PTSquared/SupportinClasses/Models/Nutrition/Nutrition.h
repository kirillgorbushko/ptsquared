//
//  Nutrition.h
//  PTSquared
//
//  Created by sirko on 5/13/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class Nutrition;

#import <Foundation/Foundation.h>

@interface Nutrition : NSObject

@property (assign, nonatomic) NSInteger nutritionObjectID;
@property (copy, nonatomic) NSString *nutritionName;
@property (copy, nonatomic) NSString *nutritionTypeName;
@property (assign, nonatomic) NSInteger nutritionTypeID;
@property (strong, nonatomic) NSString *nutritionDetails;
@property (assign, nonatomic) NSInteger nutritionDaysNumber;
@property (assign, nonatomic) NSInteger nutritionMealsPerDay;
@property (strong, nonatomic) NSArray *nutritionDays;
@property (strong, nonatomic) NSDate *nutritionSendedDate;
@property (strong, nonatomic) Nutrition *nutritionNutritionMealsDetails;

+ (Nutrition *)nutritionFromResponseDictionary:(NSDictionary *)response;
+ (NSArray *)getListOfNutritionFromInputArray:(NSArray *)inputData;

@end
