//
//  Nutrition.m
//  PTSquared
//
//  Created by sirko on 5/13/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Nutrition.h"
#import "NutritionDay.h"
#import "CalendarDataSource.h"

@implementation Nutrition

+ (Nutrition *)nutritionFromResponseDictionary:(NSDictionary *)response
{
    response = [response removeNullValues];
    
    CalendarDataSource *calendarDataSource = [[CalendarDataSource alloc] init];
    
    Nutrition *nutrition = [Nutrition new];
    nutrition.nutritionObjectID = [response[@"id"] integerValue];
    nutrition.nutritionName = response[@"name"];
    nutrition.nutritionTypeID = [response[@"nutrition_type_id"] integerValue];
    nutrition.nutritionTypeName = [response valueForKey:@"nutrition_type"];
    nutrition.nutritionDetails = response[@"details"];
    nutrition.nutritionDaysNumber = [response[@"days_number"] integerValue];
    nutrition.nutritionMealsPerDay = [response[@"meals_per_day"] integerValue];
    nutrition.nutritionSendedDate = [calendarDataSource dateFromStringWithUTCTimeZone:[response valueForKey:@"sended_date"]];
    if ([response valueForKey:@"nutrition"]) {
        nutrition.nutritionNutritionMealsDetails = [Nutrition nutritionFromResponseDictionary:[response valueForKey:@"nutrition"]];
    }
    
    
    nutrition.nutritionDays = [NutritionDay daysFromResponseArray:response[@"nutrition_days"]];
    return nutrition;
}

+ (NSArray *)getListOfNutritionFromInputArray:(NSArray *)inputData
{
    NSMutableArray *nutritions = [[NSMutableArray alloc] init];
    
    if (inputData.count) {
        for (NSDictionary *dic in inputData) {
            Nutrition *nutrition = [Nutrition nutritionFromResponseDictionary:dic];
            [nutritions addObject:nutrition];
        }
    }
    return nutritions;
}


@end
