//
//  NutritionDay.m
//  PTSquared
//
//  Created by sirko on 5/13/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NutritionDay.h"
#import "Meal.h"

@implementation NutritionDay

+ (NutritionDay *)dayFromResponseDictionary:(NSDictionary *)response
{
    response = [response removeNullValues];
    NutritionDay *day = [NutritionDay new];
    day.nutritionDayID = [response[@"id"] integerValue];
    day.nutritionDayNutritionID = [response[@"nutrition_id"] integerValue];
    day.nutritionDayOrder = [response[@"order"] integerValue];
    day.nutritionDayMeals = [Meal mealsFromResponseArray:response[@"meals"]];
    return day;
}

+ (NSArray *)daysFromResponseArray:(NSArray *)response;
{
    NSMutableArray *days = [NSMutableArray arrayWithArray:response];
    [days enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![obj isKindOfClass:[NSNull class]]) {
            [days replaceObjectAtIndex:idx withObject:[NutritionDay dayFromResponseDictionary:obj]];
        } else {
            [days removeObjectAtIndex:idx];
        }
    }];
    return days;
}

@end
