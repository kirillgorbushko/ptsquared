//
//  NutritionDay.h
//  PTSquared
//
//  Created by sirko on 5/13/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NutritionDay : NSObject

@property (assign, nonatomic) NSInteger nutritionDayID;
@property (assign, nonatomic) NSInteger nutritionDayNutritionID;
@property (assign, nonatomic) NSInteger nutritionDayOrder;
@property (strong, nonatomic) NSArray *nutritionDayMeals;

+ (NutritionDay *)dayFromResponseDictionary:(NSDictionary *)response;
+ (NSMutableArray *)daysFromResponseArray:(NSArray *)response;

@end
