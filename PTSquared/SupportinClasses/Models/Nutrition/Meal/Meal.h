//
//  Meal.h
//  PTSquared
//
//  Created by sirko on 5/13/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Meal : NSObject

@property (assign, nonatomic) NSInteger mealID;
@property (assign, nonatomic) NSInteger mealNutritionDayID;
@property (assign, nonatomic) NSInteger mealOrder;
@property (strong, nonatomic) NSString *mealDetails;
@property (strong, nonatomic) NSDate *mealsCreatedAt;
@property (strong, nonatomic) NSDate *mealsUpdatedAt;

+ (Meal *)mealFromResponseDictionary:(NSDictionary *)response;
+ (NSArray *)mealsFromResponseArray:(NSArray *)response;

@end
