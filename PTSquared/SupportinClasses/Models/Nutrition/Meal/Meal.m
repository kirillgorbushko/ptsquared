//
//  Meal.m
//  PTSquared
//
//  Created by sirko on 5/13/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Meal.h"
#import "CalendarDataSource.h"

@implementation Meal

+ (Meal *)mealFromResponseDictionary:(NSDictionary *)response
{
    response = [response removeNullValues];
    
    CalendarDataSource *calendarDataSource = [[CalendarDataSource alloc] init];
    
    Meal *meal = [Meal new];
    meal.mealID = [response[@"id"] integerValue];
    meal.mealNutritionDayID = [response[@"nutrition_day_id"] integerValue];
    meal.mealOrder = [response[@"order"] integerValue];
    meal.mealDetails = response[@"details"];
    meal.mealsCreatedAt = [calendarDataSource dateFromStringWithUTCTimeZone:response[@"created_at"]];
    meal.mealsUpdatedAt = [calendarDataSource dateFromStringWithUTCTimeZone:response[@"updated_at"]];
    return meal;
}

+ (NSArray *)mealsFromResponseArray:(NSArray *)response
{
    NSMutableArray *meals = [NSMutableArray arrayWithArray:response];
    [meals enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![obj isKindOfClass:[NSNull class]]) {
            [meals replaceObjectAtIndex:idx withObject:[Meal mealFromResponseDictionary:obj]];
        } else {
            [meals removeObjectAtIndex:idx];
        }
    }];
    return meals;
}

@end
