//
//  CalendarDay.h
//  testCalendar
//
//  Created by Kirill Gorbushko on 27.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface CalendarDay : NSObject 

@property (strong, nonatomic) NSDate *calendarDayDate;
@property (strong, nonatomic) NSArray *calendarSessions;

+ (CalendarDay *)getCalendarDayFromNSDictionary:(NSDictionary *)inputDictionary;

@end
