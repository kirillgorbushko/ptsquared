//
//  CalendarDay.m
//  testCalendar
//
//  Created by Kirill Gorbushko on 27.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "CalendarDay.h"
#import "CalendarDataSource.h"

@implementation CalendarDay

#pragma mark - Public

+ (CalendarDay *)getCalendarDayFromNSDictionary:(NSDictionary *)inputDictionary
{
    inputDictionary = [inputDictionary removeNullValues];
    
    CalendarDataSource *calendarDataSource = [[CalendarDataSource alloc] init];
    CalendarDay *newDay = [[CalendarDay alloc] init];
    
    newDay.calendarDayDate = [calendarDataSource dateFromStringWithUTCTimeZone:[inputDictionary valueForKey:@"date"]];
    
    return newDay;
}

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.calendarSessions = [[NSArray alloc] init];
    }
    return self;
}

@end