//
//  MembershipProfile.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 12.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "MembershipProfile.h"
#import "Session.h"

@implementation MembershipProfile

#pragma mark - Public

+ (MembershipProfile *)getMembershipProfileFromDictionary:(NSDictionary *)inputDictionary
{
    inputDictionary = [inputDictionary removeNullValues];
    
    MembershipProfile *newProfile = [[MembershipProfile alloc] init];
    newProfile.memberTrainerID = [[inputDictionary valueForKey:@"trainer_id"] integerValue];
    newProfile.memberTrainerFullName = [inputDictionary valueForKey:@"trainer_full_name"];
    newProfile.memberTrainerPhoneNumber = [inputDictionary valueForKey:@"trainer_phone"];
    newProfile.memberTrainerEmail = [inputDictionary valueForKey:@"trainer_email"];
    newProfile.memberTrainerSelectedColor = [inputDictionary valueForKey:@"trainer_color_name"];
    newProfile.memberTrainerColorHEXCode = [inputDictionary valueForKey:@"trainer_color"];
    newProfile.memberGroupSessionCount = [[inputDictionary valueForKey:@"group"] integerValue];
    newProfile.memnerIndividualSessionCount = [[inputDictionary valueForKey:@"individual"] integerValue];
    newProfile.membershipNextSessionInfo = [Session getNextSessionFromDictionary:[inputDictionary valueForKey:@"nextSession"]];
    newProfile.memberQRCodeUri = [inputDictionary valueForKey:@"qr_code_url"];
    newProfile.memberClientID = [[inputDictionary valueForKey:@"client_id"] integerValue];
    newProfile.memberTrainerBusinessName = [inputDictionary valueForKey:@"business_name"];
    
    return newProfile;
}

@end
