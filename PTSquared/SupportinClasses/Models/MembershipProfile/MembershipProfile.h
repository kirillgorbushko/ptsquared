//
//  MembershipProfile.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 12.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class Session;

@interface MembershipProfile : NSObject

@property (assign, nonatomic) NSInteger memberTrainerID;
@property (copy, nonatomic) NSString *memberTrainerFullName;
@property (copy, nonatomic) NSString *memberTrainerBusinessName;
@property (copy, nonatomic) NSString *memberTrainerPhoneNumber;
@property (copy, nonatomic) NSString *memberTrainerEmail;
@property (assign, nonatomic) NSInteger memberGroupSessionCount;
@property (assign, nonatomic) NSInteger memnerIndividualSessionCount;
@property (copy, nonatomic) NSString *memberTrainerSelectedColor;
@property (copy, nonatomic) NSString *memberTrainerColorHEXCode;
@property (copy, nonatomic) NSString *memberQRCodeUri;
@property (strong, nonatomic) Session *membershipNextSessionInfo;
@property (assign, nonatomic) NSInteger memberClientID;

+ (MembershipProfile *)getMembershipProfileFromDictionary:(NSDictionary *)inputDictionary;

@end
