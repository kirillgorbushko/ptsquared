//
//  Exercise.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 13.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Exercise.h"
#import "CalendarDataSource.h"

@implementation Exercise

#pragma mark - Public

+ (Exercise *)getExerciseFromResponseDictionary:(NSDictionary *)inputDictionary
{
    Exercise *newExersise = [[Exercise alloc] init];

    if (!inputDictionary || [inputDictionary isKindOfClass:[NSNull class]]) {
        return newExersise;
    }
    
    CalendarDataSource *calendarDataSource = [[CalendarDataSource alloc] init];
    inputDictionary = [inputDictionary removeNullValues];
    
    newExersise.exerciseID = [[inputDictionary valueForKey:@"id"] integerValue];
    newExersise.exerciseName = [inputDictionary valueForKey:@"name"];
    newExersise.exerciseIntervalType = [inputDictionary valueForKey:@"interval_type"];
    newExersise.exerciseValueType = [inputDictionary valueForKey:@"value_type"];
    newExersise.exerciseWorkoutID = [[inputDictionary valueForKey:@"workout_id"] integerValue];
    newExersise.exerciseRelationID = [[inputDictionary valueForKey:@"relation_id"] integerValue];
    newExersise.exerciseSetExerciseWorkoutID = [[inputDictionary valueForKey:@"exercise_workout_id"] integerValue];
    newExersise.exerciseSetIntervalCount = [[inputDictionary valueForKey:@"intervals_count"] integerValue];
    newExersise.exerciseSetValueCount = [[inputDictionary valueForKey:@"values_count"] integerValue];
    newExersise.exerciseSetOrder = [[inputDictionary valueForKey:@"order"] integerValue];
    newExersise.exerciseSetCreatedDate = [calendarDataSource dateFromStringWithUTCTimeZone:[inputDictionary valueForKey:@"created_at"]];
    newExersise.exerciseSetUpdatedDate = [calendarDataSource dateFromStringWithUTCTimeZone:[inputDictionary valueForKey:@"updated_at"]];
    newExersise.exerciseSets = [Exercise getListOfExerciseFromInputArray:[inputDictionary valueForKey:@"sets"]];
    
    return newExersise;
}

+ (NSArray *)getListOfExerciseFromInputArray:(NSArray *)inputData
{
    NSMutableArray *excersises = [[NSMutableArray alloc] init];
    
    if (inputData.count) {
        for (NSDictionary *dic in inputData) {
            Exercise *newExersise = [Exercise getExerciseFromResponseDictionary:dic];
            [excersises addObject:newExersise];
        }
    }
    return excersises;
}

@end
