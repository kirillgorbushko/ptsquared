//
//  Exercise.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 13.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface Exercise : NSObject

@property (assign, nonatomic) NSInteger exerciseID;
@property (copy, nonatomic) NSString *exerciseName;
@property (copy, nonatomic) NSString *exerciseIntervalType;
@property (copy, nonatomic) NSString *exerciseValueType;
@property (assign, nonatomic) NSInteger exerciseWorkoutID;
@property (assign, nonatomic) NSInteger exerciseRelationID;
@property (strong, nonatomic) NSArray *exerciseSets;

@property (assign, nonatomic) NSInteger exerciseSetExerciseWorkoutID;
@property (assign, nonatomic) NSInteger exerciseSetIntervalCount;
@property (assign, nonatomic) NSInteger exerciseSetValueCount;
@property (assign, nonatomic) NSInteger exerciseSetOrder;
@property (strong, nonatomic) NSDate *exerciseSetCreatedDate;
@property (strong, nonatomic) NSDate *exerciseSetUpdatedDate;

+ (Exercise *)getExerciseFromResponseDictionary:(NSDictionary *)inputDictionary;
+ (NSArray *)getListOfExerciseFromInputArray:(NSArray *)inputData;

@end
