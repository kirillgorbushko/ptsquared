//
//  Workout.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 13.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class Exercise;

@interface Workout : NSObject

@property (assign, nonatomic) NSInteger workoutID;
@property (assign, nonatomic) NSInteger workoutTrainerID;
@property (copy, nonatomic) NSString *workoutName;
@property (assign, nonatomic) CGFloat workoutDuration;
@property (assign, nonatomic) NSInteger workoutTypeID;
@property (copy, nonatomic) NSString *workoutTypeName;
@property (copy, nonatomic) NSString *workoutDetails;
@property (strong, nonatomic) NSDate *workoutCreatedDate;
@property (strong, nonatomic) NSDate *workoutUpdatedDate;
@property (strong, nonatomic) NSArray *workoutExersises;
@property (strong, nonatomic) NSDate *workoutSendedDate;
@property (strong, nonatomic) Workout *workoutDetailedExerciseList;

+ (Workout *)getWorkoutFromResponseDictionary:(NSDictionary *)inputDictionary;
+ (NSArray *)getListOfWorkoutsFromInputArray:(NSArray *)inputData;

@end
