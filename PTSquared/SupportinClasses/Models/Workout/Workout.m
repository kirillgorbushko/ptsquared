//
//  Workout.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 13.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Workout.h"
#import "Exercise.h"
#import "CalendarDataSource.h"

@implementation Workout

#pragma mark - Public

+ (Workout *)getWorkoutFromResponseDictionary:(NSDictionary *)inputDictionary
{
    inputDictionary = [inputDictionary removeNullValues];
    CalendarDataSource *calendarDataSource = [[CalendarDataSource alloc] init];
    
    Workout *newWorkoout = [[Workout alloc] init];
    
    newWorkoout.workoutID = [[inputDictionary valueForKey:@"id"] integerValue];
    newWorkoout.workoutTrainerID = [[inputDictionary valueForKey:@"trainer_id"] integerValue];
    newWorkoout.workoutName = [inputDictionary valueForKey:@"name"];
    newWorkoout.workoutDuration = [[inputDictionary valueForKey:@"duration"] integerValue];
    newWorkoout.workoutTypeID = [[inputDictionary valueForKey:@"workout_type_id"] integerValue];
    newWorkoout.workoutTypeName = [inputDictionary valueForKey:@"type"];
    newWorkoout.workoutDetails = [inputDictionary valueForKey:@"details"];
    newWorkoout.workoutCreatedDate = [calendarDataSource dateFromStringWithUTCTimeZone:[inputDictionary valueForKey:@"created_at"]];
    newWorkoout.workoutUpdatedDate = [calendarDataSource dateFromStringWithUTCTimeZone:[inputDictionary valueForKey:@"updated_at"]];
    newWorkoout.workoutExersises = [Exercise getListOfExerciseFromInputArray:[inputDictionary valueForKey:@"exercises"]];
    newWorkoout.workoutSendedDate = [calendarDataSource dateFromStringWithUTCTimeZone:[inputDictionary valueForKey:@"sended_date"]];
    if ([inputDictionary valueForKey:@"workout"]) {
        newWorkoout.workoutDetailedExerciseList = [Workout getWorkoutFromResponseDictionary:[inputDictionary valueForKey:@"workout"]];
    }

    return newWorkoout;
}

+ (NSArray *)getListOfWorkoutsFromInputArray:(NSArray *)inputData
{
    NSMutableArray *workouts = [[NSMutableArray alloc] init];
    
    if (inputData.count) {
        for (NSDictionary *dic in inputData) {
            Workout *newWorkout = [Workout getWorkoutFromResponseDictionary:dic];
            [workouts addObject:newWorkout];
        }
    }
    
    return workouts;
}

//#pragma mark - Ovverrides
//
//- (NSString *)description
//{
//    NSDictionary *description = @{
//                                  @"id" : @(self.workoutID),
//                                  @"trainer_id" : @(self.workoutTrainerID),
//                                  @"name" : self.workoutName,
//                                  @"duration" : @(self.workoutDuration),
//                                  @"workoutType" : @(self.workoutTypeID),
//                                  @"details" : self.workoutDetails,
//                                  @"exerciseCount" : @(self.workoutExersises.count)
//                                  };
//    return [NSString stringWithFormat:@"%@", description];
//}

@end
