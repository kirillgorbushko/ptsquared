//
//  Avatar.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 14.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Avatar.h"

@implementation Avatar

#pragma mark - Public

+ (Avatar *)getAvatarObjectFromResponseDictionary:(NSDictionary *)inputDictionary
{
    inputDictionary = [inputDictionary removeNullValues];
    
    Avatar *newAvatar = [[Avatar alloc] init];
    newAvatar.avatarID = [[inputDictionary valueForKey:@"id"] integerValue];
    newAvatar.avatarImagableID = [[inputDictionary valueForKey:@"imageable_id"] integerValue];
    newAvatar.avatarImagableType = [inputDictionary valueForKey:@"imageable_type"];
    newAvatar.avatarImageName = [inputDictionary valueForKey:@"image_name"];
    newAvatar.avatarImageUri = [inputDictionary valueForKey:@"image_url"];
    
    return newAvatar;
}

@end
