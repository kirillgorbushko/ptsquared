//
//  Avatar.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 14.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface Avatar : NSObject

@property (assign, nonatomic) NSInteger avatarID;
@property (assign, nonatomic) NSInteger avatarImagableID;
@property (copy, nonatomic) NSString *avatarImageName;
@property (copy, nonatomic) NSString *avatarImagableType;
@property (copy, nonatomic) NSString *avatarImageUri;

+ (Avatar *)getAvatarObjectFromResponseDictionary:(NSDictionary *)inputDictionary;

@end
