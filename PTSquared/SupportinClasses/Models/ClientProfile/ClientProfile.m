 //
//  ClientProfile.m
//  PTSquared
//
//  Created by sirko on 5/12/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ClientProfile.h"
#import "CalendarDataSource.h"
#import "Avatar.h"

static NSString *const ProfilePlistName = @"ProfileItems";
static NSString *const UnitOfLengthCM = @" cm";
static NSString *const UnitOfLengthMM = @" mm";
static NSString *const UnitOfLengthInch = @" inches";

@implementation ClientProfile

#pragma mark - Public

+ (ClientProfile *)profileFromResponseDictionary:(NSDictionary *)response
{
    response = [response removeNullValues];
    
    CalendarDataSource *calendarDataSource = [[CalendarDataSource alloc] init];
    ClientProfile *profile = [ClientProfile new];
    
    profile.objectID = response[@"id"];
    profile.clientID = response[@"client_id"];
    profile.clientInviterTrainerID = response[@"inviter_trainer_id"];
    profile.clientFirstName = response[@"first_name"];
    profile.clientLastName = response[@"last_name"];
    profile.clientPhone = response[@"phone"];
    profile.clientBirthday = [calendarDataSource dateFromStringWithUTCTimeZone:response[@"birthday"]];
    profile.clientAddress = response[@"address"];
    profile.clientHeight = [NSNumber numberWithInt:(int)[response[@"height"] integerValue]];
    profile.clientWeight = [NSNumber numberWithInt:(int)[response[@"weight"] integerValue]];
    profile.clientBodyMassIndex = response[@"body_mass_index"];
    profile.clientBodyUpperArm = response[@"body_upper_arm"];
    profile.clientBodyAbdomen= response[@"body_abdomen"];
    profile.clientBodyWaist = response[@"body_waist"];
    profile.clientBodyHips = response[@"body_hips"];
    profile.clientBodyThigh = response[@"body_thigh"];
    profile.clientSkinfoldsChest = response[@"skinfolds_chest"];
    profile.clientSkinfoldsAbdomen = response[@"skinfolds_abdomen"];
    profile.clientSkinfoldsThigh = response[@"skinfolds_thigh"];
    profile.clientSkinfoldsTriceps = response[@"skinfolds_triceps"];
    profile.clientSkinfoldsAxilla= response[@"skinfolds_axilla"];
    profile.clientSkinfoldsSubscapular = response[@"skinfolds_subscapular"];
    profile.clientSkinfoldsSuprailiac = response[@"skinfolds_suprailiac"];
    profile.clientSince = [calendarDataSource dateFromStringWithUTCTimeZone:response[@"client_since"]];
    profile.clientReferralType = response[@"referral_type"];
    profile.clientMedicalHistory = response[@"medical_history"];
    profile.clientGoal = response[@"goal"];
    profile.clientProfileID = response[@"profile_id"];
    profile.clientBeforeID = response[@"before_id"];
    profile.clientAfterID = response[@"after_id"];
    profile.clientPrivacyPolicy = [response[@"is_public"] boolValue];
    profile.clientCreatedAt = [calendarDataSource dateFromStringWithUTCTimeZone:response[@"created_at"]];
    profile.clientUpdatedAt = [calendarDataSource dateFromStringWithUTCTimeZone:response[@"updated_at"]];
    profile.clientBodyBefore = response[@"body_before"];
    profile.clientBodyAfter = response[@"body_after"];
    profile.clientAvatar = [Avatar getAvatarObjectFromResponseDictionary:response[@"avatar"]];
    profile.clientAvailableGroupSessions = [[NSNumberFormatter new] numberFromString:response[@"group_available"]];
    profile.clientAvailableIndividualSessions = [[NSNumberFormatter new] numberFromString:response[@"individual_available"]];
    profile.clientCurrentStatusForSessionID = [response valueForKey:@"status"];
    profile.clientCurrentSessionID = [[response valueForKey:@"session_id"] integerValue];
    profile.clientEmail = response[@"email"];
    return profile;
}

+ (NSMutableArray *)dataSourceFromResponseDictionary:(NSDictionary *)response
{
    NSMutableArray *dataSource = [NSMutableArray new];
    ClientProfile *profile = [ClientProfile profileFromResponseDictionary:response];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:ProfilePlistName ofType:@"plist"]];
    NSArray *content = [dictionary valueForKey:@"content"];
    
    for (NSDictionary *item in content) {
        NSMutableDictionary *newItem = [NSMutableDictionary dictionaryWithDictionary:@{@"itemName":item[@"itemName"], @"itemValues":[NSMutableDictionary new]}];
        [dataSource addObject:newItem];
    }

    dataSource[0][@"itemValues"] = @[
                                     @{[ClientProfile stringValueFromData:profile.clientHeight unitOfLength:@" cm"]:[ClientProfile stringValueFromData:profile.clientWeight unitOfLength:@" kg"]}
                                    ];
    dataSource[1][@"itemValues"] = @[@{@"":[ClientProfile stringValueFromData:[NSString stringWithFormat:@"%.1f", [profile.clientBodyMassIndex floatValue]] unitOfLength:@""]}];
    dataSource[2][@"itemValues"] = @[
                                     @{@"Upper Arm":[ClientProfile stringValueFromData:profile.clientBodyUpperArm unitOfLength:UnitOfLengthInch]},
                                     @{@"Abdomen":[ClientProfile stringValueFromData:profile.clientBodyAbdomen unitOfLength:UnitOfLengthInch]},
                                     @{@"Waist":[ClientProfile stringValueFromData:profile.clientBodyWaist unitOfLength:UnitOfLengthInch]},
                                     @{@"Hips":[ClientProfile stringValueFromData:profile.clientBodyHips unitOfLength:UnitOfLengthInch]},
                                     @{@"Thigh":[ClientProfile stringValueFromData:profile.clientBodyThigh unitOfLength:UnitOfLengthInch]}
                                    ];
    
    dataSource[3][@"itemValues"] = @[
                                     @{@"Chest":[ClientProfile stringValueFromData:profile.clientSkinfoldsChest unitOfLength:UnitOfLengthMM]},
                                     @{@"Abdomen":[ClientProfile stringValueFromData:profile.clientSkinfoldsAbdomen unitOfLength:UnitOfLengthMM]},
                                     @{@"Thigh":[ClientProfile stringValueFromData:profile.clientSkinfoldsThigh unitOfLength:UnitOfLengthMM]},
                                     @{@"Triceps":[ClientProfile stringValueFromData:profile.clientSkinfoldsTriceps unitOfLength:UnitOfLengthMM]},
                                     @{@"Axilla":[ClientProfile stringValueFromData:profile.clientSkinfoldsAxilla unitOfLength:UnitOfLengthMM]},
                                     @{@"Subscapular":[ClientProfile stringValueFromData:profile.clientSkinfoldsSubscapular unitOfLength:UnitOfLengthMM]},
                                     @{@"Suprailiac":[ClientProfile stringValueFromData:profile.clientSkinfoldsSuprailiac unitOfLength:UnitOfLengthMM]}
                                     ];
    
    dataSource[4][@"itemValues"] = @[@{@"":profile.clientSince ? [CalendarDataSource dateStringWithFormattedEndingFromDate:profile.clientSince] : @" "}];
    dataSource[5][@"itemValues"] = @[@{@"":profile.clientGoal ? profile.clientGoal : @"-"}];
    [dataSource addObject: @{
                             @"itemNames":@"", @"itemValues":@{
                                     @"photoBefore" : response[@"body_before"] ? response[@"body_before"] :@"-",
                                     @"photoAfter" : response[@"body_after"] ? response[@"body_after"] : @"-"
                                     }
                             }
     ];
    
    return dataSource;
}

+ (NSArray *)prepareArrayWithProfilesFromResponseArray:(NSArray *)inputArray
{
    NSMutableArray *requestedArray = [[NSMutableArray alloc] init];
    if (inputArray.count) {
        for (NSDictionary *dic in inputArray) {
            ClientProfile *newProfile = [ClientProfile profileFromResponseDictionary:dic];
            [requestedArray addObject:newProfile];
        }
    }
    return requestedArray;
}

#pragma mark - Private

+ (NSString *)stringValueFromData:(id)data unitOfLength:(NSString *)unit
{
    if ([data isKindOfClass:[NSNumber class]]) {
        return [[data stringValue] stringByAppendingString:unit];
    } else if ([data isKindOfClass:[NSString class]]) {
        return [data stringByAppendingString:unit];
    }
    return [@"0" stringByAppendingString:unit];
}

#pragma mark - Ovverrides

- (NSString *)description
{
    NSDictionary *dic = @{
                         @"status" : self.clientCurrentStatusForSessionID
                         };
    return [NSString stringWithFormat:@"%@", dic];
}

@end
