//
//  ClientProfile.h
//  PTSquared
//
//  Created by sirko on 5/12/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Avatar.h"

@interface ClientProfile : NSObject

@property (strong, nonatomic) NSNumber *objectID;
@property (strong, nonatomic) NSNumber *clientID;
@property (strong, nonatomic) NSNumber *clientInviterTrainerID;
@property (copy, nonatomic) NSString *clientFirstName;
@property (copy, nonatomic) NSString *clientLastName;
@property (copy, nonatomic) NSString *clientPhone;
@property (strong, nonatomic) NSDate *clientBirthday;
@property (copy, nonatomic) NSString *clientAddress;
@property (strong, nonatomic) NSNumber *clientHeight;
@property (strong, nonatomic) NSNumber *clientWeight;
@property (strong, nonatomic) NSNumber *clientBodyMassIndex;
@property (strong, nonatomic) NSNumber *clientBodyUpperArm;
@property (strong, nonatomic) NSNumber *clientBodyAbdomen;
@property (strong, nonatomic) NSNumber *clientBodyWaist;
@property (strong, nonatomic) NSNumber *clientBodyHips;
@property (strong, nonatomic) NSNumber *clientBodyThigh;
@property (strong, nonatomic) NSNumber *clientSkinfoldsChest;
@property (strong, nonatomic) NSNumber *clientSkinfoldsAbdomen;
@property (strong, nonatomic) NSNumber *clientSkinfoldsThigh;
@property (strong, nonatomic) NSNumber *clientSkinfoldsTriceps;
@property (strong, nonatomic) NSNumber *clientSkinfoldsAxilla;
@property (strong, nonatomic) NSNumber *clientSkinfoldsSubscapular;
@property (strong, nonatomic) NSNumber *clientSkinfoldsSuprailiac;
@property (strong, nonatomic) NSDate *clientSince;
@property (copy, nonatomic) NSString *clientReferralType;
@property (copy, nonatomic) NSString *clientMedicalHistory;
@property (copy, nonatomic) NSString *clientGoal;
@property (strong, nonatomic) NSNumber *clientProfileID;
@property (strong, nonatomic) NSNumber *clientBeforeID;
@property (strong, nonatomic) NSNumber *clientAfterID;
@property (assign, nonatomic) BOOL clientPrivacyPolicy;
@property (strong, nonatomic) NSDate *clientCreatedAt;
@property (strong, nonatomic) NSDate *clientUpdatedAt;
@property (copy, nonatomic) NSString *clientBodyBefore;
@property (copy, nonatomic) NSString *clientBodyAfter;
@property (strong, nonatomic) Avatar *clientAvatar;
@property (assign, nonatomic) NSInteger clientCurrentSessionID;
@property (copy, nonatomic) NSString *clientCurrentStatusForSessionID;

@property (strong, nonatomic) NSNumber *clientAvailableGroupSessions;
@property (strong, nonatomic) NSNumber *clientAvailableIndividualSessions;

@property (copy, nonatomic) NSString *clientEmail;

+ (ClientProfile *)profileFromResponseDictionary:(NSDictionary *)response;
+ (ClientProfile *)dataSourceFromResponseDictionary:(NSDictionary *)response;
+ (NSArray *)prepareArrayWithProfilesFromResponseArray:(NSArray *)inputArray;

@end
