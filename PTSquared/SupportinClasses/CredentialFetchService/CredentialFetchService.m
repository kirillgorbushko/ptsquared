//
//  AutoLogger.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 08.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "CredentialFetchService.h"
#import "KeychainItemWrapper.h"
#import "TrainerHomeViewController.h"
#import "ContainerViewController.h"
#import "ProfileManager.h"

static NSString *const KeychainIdentifier = @"PTSquareKeychainIdentifier";
static NSString *const KeyPassword = @"password";
static NSString *const KeyLogin = @"login";
static NSString *const KeyToken = @"KeyToken";

static NSString *const KeyFacebookID = @"KeyFacebookID";

@interface CredentialFetchService()

@property (strong, nonatomic) KeychainItemWrapper *wrapper;
@property (strong, nonatomic) NSDictionary *credentials;

@end

@implementation CredentialFetchService

#pragma mark - Public

+ (instancetype)service
{
    static CredentialFetchService *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[CredentialFetchService alloc] init];
    });
    return sharedManager;
}

- (void)fetchLoginInfoResult:(CredentialFetchResult)result
{
    NSInteger loginKey = 0;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:CredentialFetchLoginKey]) {
        loginKey = [[[NSUserDefaults standardUserDefaults] valueForKey:CredentialFetchLoginKey] integerValue];
    }
    
    self.credentials = [self getCredentialFromKeyChain];
    
    if ([self.credentials allKeys].count) {
        
        switch ((CredentialFetchKey)loginKey) {
            case CredentialFetchKeyClientServer: {
                [ProfileManager sharedManager].profileType = CLIENT;
                result(CredentialFetchKeyClientServer, [self.credentials valueForKey:KeyLogin], [self.credentials valueForKey:KeyPassword]);
                break;
            }
            case CredentialFetchKeyPersonalTrainer: {
                [ProfileManager sharedManager].profileType = PERSONAL_TRAINER;
                result(CredentialFetchKeyPersonalTrainer, [self.credentials valueForKey:KeyLogin], [self.credentials valueForKey:KeyPassword]);
                break;
            }
            case CredentialFetchKeyClientFacebook:{
                [ProfileManager sharedManager].profileType = CLIENT;
                NSString *facebookID = [[NSUserDefaults standardUserDefaults] valueForKey:KeyFacebookID];
                result(CredentialFetchKeyClientFacebook, facebookID, nil);
                break;
            }
            case CredentialFetchKeyUndefined: {
                result(CredentialFetchKeyUndefined, nil, nil);
                break;
            }
        }
        
    }
}

- (void)setClientLogginedWithFacebookId:(NSString *)facebookID
{
    [[NSUserDefaults standardUserDefaults] setValue:@(CredentialFetchKeyClientFacebook) forKey:CredentialFetchLoginKey];
    [[NSUserDefaults standardUserDefaults] setValue:@(CredentialFetchKeyClientFacebook) forKey:CredentialFetchLoginKey];
    [[NSUserDefaults standardUserDefaults] setValue:facebookID forKey:KeyFacebookID];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setClientLogginedWithClientName:(NSString *)userName password:(NSString *)userPassword
{
    [[NSUserDefaults standardUserDefaults] setValue:@(CredentialFetchKeyClientServer) forKey:CredentialFetchLoginKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self savePassword:userPassword forUserName:userName];
}

- (void)setPersonalTrainerLogginedWithPersonalTrainerName:(NSString *)userName password:(NSString *)userPassword
{
    [[NSUserDefaults standardUserDefaults] setValue:@(CredentialFetchKeyPersonalTrainer) forKey:CredentialFetchLoginKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self savePassword:userPassword forUserName:userName];
}

- (void)setNeedLoginState
{
    [[NSUserDefaults standardUserDefaults] setValue:@(CredentialFetchKeyUndefined) forKey:CredentialFetchLoginKey];
    
    [self savePassword:@"NULL" forUserName:@"NULL"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:KeyFacebookID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:KeychainIdentifier accessGroup:nil];
        self.credentials = [[NSDictionary alloc] init];
    }
    return self;
}

#pragma mark - Manging Piush Tokens

- (void)saveClientPushToken:(NSString *)token
{
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:KeyToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)getClientPushToken
{
    return (NSString *)[[NSUserDefaults standardUserDefaults] valueForKey:KeyToken];
}

#pragma mark - Private

- (void)savePassword:(NSString *)password forUserName:(NSString *)userName
{
    [self.wrapper setObject:userName forKey:(__bridge id)kSecAttrAccount];
    [self.wrapper setObject:password forKey:(__bridge id)kSecValueData];
}

- (NSDictionary *)getCredentialFromKeyChain
{
    NSDictionary *credentials = @{
                                  KeyLogin : [self.wrapper objectForKey:(__bridge id)kSecAttrAccount],
                                  KeyPassword : [[NSString alloc] initWithData:[self.wrapper objectForKey:(__bridge id)kSecValueData] encoding:NSUTF8StringEncoding]
                                  };
    return credentials;
}

@end
