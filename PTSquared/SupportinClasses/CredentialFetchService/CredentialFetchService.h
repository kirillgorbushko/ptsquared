//
//  AutoLogger.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 08.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CredentialFetchKey) {
    CredentialFetchKeyUndefined,
    CredentialFetchKeyPersonalTrainer,
    CredentialFetchKeyClientServer,
    CredentialFetchKeyClientFacebook
};

typedef void(^CredentialFetchResult)(CredentialFetchKey key, NSString *login, NSString *password);

static NSString *const CredentialFetchLoginKey = @"AutoLoginKey";

@interface CredentialFetchService : NSObject

+ (instancetype)service;

- (void)fetchLoginInfoResult:(CredentialFetchResult)result;

- (void)saveClientPushToken:(NSString *)token;
- (NSString *)getClientPushToken;

- (void)setClientLogginedWithFacebookId:(NSString *)facebookID;
- (void)setClientLogginedWithClientName:(NSString *)userName password:(NSString *)userPassword;
- (void)setPersonalTrainerLogginedWithPersonalTrainerName:(NSString *)userName password:(NSString *)userPassword;
- (void)setNeedLoginState;

@end
