//
//  BarcodeReader.m
//  QRCodeReader
//
//  Created by Kirill Gorbushko on 04.05.15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import "QRCodeReader.h"

@interface QRCodeReader()

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@property (strong, nonatomic) UIView *previewLayer;

@end

@implementation QRCodeReader

#pragma mark - Public

- (instancetype)initWithView:(UIView *)viewPreview;
{
    self = [super init];
    if (self) {
        self.previewLayer = viewPreview;
        self.isReading = NO;
#if !TARGET_IPHONE_SIMULATOR
        [self setupSession];
#endif
    }
    return self;
}

- (void)startReading
{
    [self.captureSession startRunning];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(readerDidChangeStatusTo:)]) {
        [self.delegate readerDidChangeStatusTo:@"Reading started"];
    }
}

- (void)stopReading
{
    [self.captureSession stopRunning];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(readerDidChangeStatusTo:)]) {
        [self.delegate readerDidChangeStatusTo:@"Reading stoped"];
    }
}

- (void)startStopReading
{
    if (self.isReading) {
        [self stopReading];
    } else {
        [self startReading];
    }
    
    self.isReading = !self.isReading;
}

+ (BOOL)isDeviceHasBackCamera
{
    BOOL deviceHasBackCamera = NO;
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    for (AVCaptureDevice *device in videoDevices) {
        if ( device.position == AVCaptureDevicePositionBack) {
            deviceHasBackCamera = YES;
        }
    }
    
    return deviceHasBackCamera;
}

+ (void)checkPermissionForCamera:(AccessGranted)status
{
    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType:completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    status(YES);
                });
            } else {
                status(NO);
            }
        }];
    }
}

#pragma mark - Private

- (void)setupSession
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(readerDidChangeStatusTo:)]) {
        [self.delegate readerDidChangeStatusTo:@"Preparing..."];
    }
    
    NSError *error;
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (input) {
        self.captureSession = [[AVCaptureSession alloc] init];
        if ([self.captureSession canAddInput:input]) {
            [self.captureSession addInput:input];
        }
        
        AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
        [self.captureSession addOutput:captureMetadataOutput];
        
        dispatch_queue_t dispatchQueue = dispatch_queue_create("com.thinkMobiles.captureQueue.barcodeReading.TRA.smart.service", DISPATCH_QUEUE_SERIAL);
        [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
        [captureMetadataOutput setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
        
        self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
        [self.videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [self.videoPreviewLayer setFrame:self.previewLayer.bounds];
        
        [self.previewLayer.layer addSublayer:self.videoPreviewLayer];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(readerDidChangeStatusTo:)]) {
            [self.delegate readerDidChangeStatusTo:@"Ready to start scanning"];
        }
    } else {
        NSLog(@"%@", [error localizedDescription]);
    }
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    if (metadataObjects && [metadataObjects count]) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(readerDidFinishCapturingWithResult:)]) {
                [self.delegate readerDidFinishCapturingWithResult:[metadataObj stringValue]];
            }
//            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            self.isReading = NO;
        }
    }
}


@end
