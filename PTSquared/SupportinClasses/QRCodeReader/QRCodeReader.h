//
//  BarcodeReader.h
//  QRCodeReader
//
//  Created by Kirill Gorbushko on 04.05.15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef void(^AccessGranted)(BOOL status);

@protocol QRCodeReaderDelagte <NSObject>

@optional
- (void)readerDidChangeStatusTo:(NSString *)status;

@required
- (void)readerDidFinishCapturingWithResult:(NSString *)result;

@end

@interface QRCodeReader : NSObject <AVCaptureMetadataOutputObjectsDelegate>

@property (weak, nonatomic) id <QRCodeReaderDelagte> delegate;
@property (nonatomic) BOOL isReading;

- (instancetype)initWithView:(UIView *)viewPreview;

- (void)startReading;
- (void)stopReading;

- (void)startStopReading;

+ (void)checkPermissionForCamera:(AccessGranted)status;
+ (BOOL)isDeviceHasBackCamera;

@end
