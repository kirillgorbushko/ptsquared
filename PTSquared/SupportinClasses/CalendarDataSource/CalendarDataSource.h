//
//  CalendarDataSource.h
//  testCalendar
//
//  Created by Kirill Gorbushko on 27.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarDataSource : NSObject

- (NSString *)monthNameFromDate:(NSDate *)date;
- (NSDate *)dateFromMonthWithMonthOffsetFromNow:(NSInteger)offset;
- (NSDictionary *)dateComponentsFromDate:(NSDate *)sourceDate;

- (NSInteger)getGMTIntervalFromDate:(NSDate *)date;
- (NSDate *)dateFromStringWithUTCTimeZone:(NSString *)string;
- (NSDate *)UTCDateFromLocalDate:(NSDate *)date;
+ (NSDate *)toLocalTime:(NSDate *)date;

+ (BOOL)isDate:(NSDate *)firstDate latestThanDate:(NSDate *)secondDate;
+ (BOOL)isDay:(NSDate *)firstDate isSameDayAs:(NSDate *)secondDate;

+ (NSDate *)dayBefore:(NSDate *)day;
+ (NSDate *)dayAfter:(NSDate *)day;
+ (NSInteger)weekDayNumberWithDate:(NSDate *)date;
+ (NSInteger)monthNumberFromDate:(NSDate *)date;

+ (NSDate *)dateWithOffset:(NSInteger)offset fromDate:(NSDate *)startDate;
+ (NSInteger)firstUserWeekDayInCalendar;

+ (NSDate *)dateFromString:(NSString *)string;
+ (NSDate *)zeroHourDateFromString:(NSString *)inputSource;
+ (NSString *)stringZeroInRequestFormat:(NSDate *)inputDate;
+ (NSString *)stringFromDate:(NSDate *)date;
+ (NSString *)stringFromDateInRequestFormat:(NSDate *)date;
+ (NSString *)dateStringWithFormattedEndingFromDate:(NSDate *)date;
+ (NSString *)timeFromDate:(NSDate *)date;

+ (NSInteger)numberofDay:(NSDate *)date;

#pragma mark - DataSource

- (NSArray *)daysForWeekWithOffsetFromNow:(NSInteger)offset sessionsDays:(NSArray *)days;
- (NSArray *)daysForMonthWithOffsetFromNow:(NSInteger)offset sessionsDays:(NSArray *)days;

@end
