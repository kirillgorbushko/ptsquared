//
//  CalendarDataSource.m
//  testCalendar
//
//  Created by Kirill Gorbushko on 27.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "CalendarDataSource.h"
#import "CalendarDay.h"
#import "Session.h"

static NSInteger const BiggestDisplayedCalendarElementsCount = 42;

@implementation CalendarDataSource

#pragma mark - Public

- (NSString *)monthNameFromDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM"];
    return [dateFormatter stringFromDate:date];
}

- (NSInteger)numberOfWeekDayFromDate:(NSDate *)date
{
    NSDateFormatter *myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"c"];
    NSInteger dayNumberInCurrentWeekOffset =[[myFormatter stringFromDate:date] integerValue];
    return dayNumberInCurrentWeekOffset;
}

+ (BOOL)isDate:(NSDate *)firstDate latestThanDate:(NSDate *)secondDate
{
    NSTimeInterval distanceBetweenDates = [firstDate timeIntervalSinceDate:secondDate];
    return distanceBetweenDates > 0;
}

+ (BOOL)isDay:(NSDate *)firstDate isSameDayAs:(NSDate *)secondDate
{
    NSDateComponents *componentsForFirstDate = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:firstDate];
    NSInteger dayFromFirstDate = [componentsForFirstDate day];
    NSInteger monthFormFirstDate = [componentsForFirstDate month];
    NSInteger yearFromFirstDate = [componentsForFirstDate year];
    
    NSDateComponents *componentsForSecondDate = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:secondDate];
    NSInteger dayFromSecondDate = [componentsForSecondDate day];
    NSInteger monthFromSecondDay = [componentsForSecondDate month];
    NSInteger yearFromSecondDay = [componentsForSecondDate year];
    
    if (dayFromFirstDate == dayFromSecondDate && monthFormFirstDate == monthFromSecondDay && yearFromFirstDate == yearFromSecondDay){
        return YES;
    } else {
        return NO;
    }
}

+ (NSDate *)dayBefore:(NSDate *)day
{
    NSCalendar *gregorianCalendar = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init];
    [deltaComps setDay:-1];
    
    NSDate *prevDate = [gregorianCalendar dateByAddingComponents:deltaComps toDate:day options:0];
    return prevDate;
}

+ (NSDate *)dayAfter:(NSDate *)day
{
    NSCalendar *gregorianCalendar = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init];
    [deltaComps setDay:1];
    
    NSDate *prevDate = [gregorianCalendar dateByAddingComponents:deltaComps toDate:day options:0];
    return prevDate;
}

+ (NSDate *)dateWithOffset:(NSInteger)offset fromDate:(NSDate *)startDate
{
    NSCalendar *gregorianCalendar = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init];
    [deltaComps setDay:offset];
    
    NSDate *prevDate = [gregorianCalendar dateByAddingComponents:deltaComps toDate:startDate options:0];
    return prevDate;
}

+ (NSInteger)weekDayNumberWithDate:(NSDate *)date
{
    NSDateFormatter *myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"c"];
    NSInteger dayNumberInCurrentWeekOffset = [[myFormatter stringFromDate:date] integerValue];
    
    NSInteger currentDayNumber = dayNumberInCurrentWeekOffset;
    return currentDayNumber;
}

+ (NSInteger)monthNumberFromDate:(NSDate *)date
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitMonth fromDate:date];
    return [components month];
}

+ (NSInteger)firstUserWeekDayInCalendar
{
    return [[NSCalendar currentCalendar] firstWeekday];
}

- (NSDictionary *)dateComponentsFromDate:(NSDate *)sourceDate
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:sourceDate];
    
    NSDictionary *dateByComponents = @{
                                       @"year" : @([components year]),
                                       @"month" : @([components month]),
                                       @"day" : @([components day])
                                       };
    return dateByComponents;
}

+ (NSString *)stringFromDate:(NSDate *)date
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMMM dd yyyy"];
    NSString *dateString = [format stringFromDate:date];
    return dateString;
}

+ (NSString *)stringFromDateInRequestFormat:(NSDate *)date
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"YYYY-MM-dd"];
    NSString *dateString = [[format stringFromDate:date] stringByAppendingString:@" 00:00:00"];
    return dateString;
}

+ (NSString *)stringZeroInRequestFormat:(NSDate *)inputDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:inputDate];
    
    return dateString;
}

+ (NSDate *)dateFromString:(NSString *)string
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    return [formatter dateFromString:string];
}

+ (NSDate *)zeroHourDateFromString:(NSString *)inputSource
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [formatter dateFromString:inputSource];
}

- (NSDate *)dateFromStringWithUTCTimeZone:(NSString *)string
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *date = [formatter dateFromString:string];
    return date;
}

- (NSDate *)UTCDateFromLocalDate:(NSDate *)date
{
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMTForDate:date];
    NSDate *dateInUTC = [date dateByAddingTimeInterval:timeZoneSeconds];
    return dateInUTC;
}

+ (NSString *)timeFromDate:(NSDate *)date
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"HH:mm a"];
    NSString *dateString = [[format stringFromDate:date] uppercaseString];
    NSString *hours = [dateString substringToIndex:2];
    NSInteger hourIn24Format = [hours integerValue];
    if (hourIn24Format > 12) {
        hourIn24Format -= 12;
    }
    NSString *twelveHoursFormattedString = [NSString stringWithFormat:@"%i%@", (int)hourIn24Format, [dateString substringFromIndex:2]];
    return twelveHoursFormattedString;
}

+ (NSString *)dateStringWithFormattedEndingFromDate:(NSDate *)date
{
    NSDateFormatter *prefixDateFormatter = [[NSDateFormatter alloc] init];
    [prefixDateFormatter setDateFormat:@"dd"];
    NSString *prefixDateString = [prefixDateFormatter stringFromDate:date];
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init];
    [monthDayFormatter setDateFormat:@"d"];
    int date_day = [[monthDayFormatter stringFromDate:date] intValue];
    NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
    NSString *suffix = [suffixes objectAtIndex:date_day];
    NSString *dateString = [prefixDateString stringByAppendingString:suffix];
    dateString = [dateString stringByAppendingString:@" of "];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMMM YYYY"];
    NSString *postFix = [format stringFromDate:date];
    dateString = [dateString stringByAppendingString:postFix];
    
    if ([dateString hasPrefix:@"0"]) {
        dateString = [dateString substringFromIndex:1];
    }
    
    return dateString;
}

#pragma mark - DataSource

- (NSArray *)daysForWeekWithOffsetFromNow:(NSInteger)offset sessionsDays:(NSArray *)days
{
    NSArray *daysForSelectedWeek = [self getWeekWithOffsetFromCurrentWeek:offset];
    NSMutableArray *dataSource = [self compositDays:daysForSelectedWeek withSessions:days];
    return dataSource;
}

- (NSArray *)daysForMonthWithOffsetFromNow:(NSInteger)offset sessionsDays:(NSArray *)days
{
    NSArray *daysForSelectedMonth = [self getMonthWithOffsetFromCurrentMonth:offset];
    NSMutableArray *dataSource = [self compositDays:daysForSelectedMonth withSessions:days];
    return dataSource;
}

- (NSDate *)dateFromMonthWithMonthOffsetFromNow:(NSInteger)offset
{
    NSDate *currentDate  = [NSDate date];
    NSCalendar *gregorianCalendar = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth)  fromDate:currentDate];
    
    NSDateComponents *dt = [[NSDateComponents alloc] init];
    [dt setWeekday:1];
    [dt setDay:1];
    [dt setMonth:[components month] + offset];
    [dt setYear:[components year]];
    [dt setHour:[self getGMTIntervalFromDate:currentDate]];
    
    NSDate *firstDayOfMonth = [gregorianCalendar dateFromComponents:dt];
    return firstDayOfMonth;
}

+ (NSInteger)numberofDay:(NSDate *)date
{
    NSDateComponents *comps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:date];
    NSInteger day = [comps day];
    return day;
}

+ (BOOL)isDay:(NSDate *)firstDate isExactrlySameDayAs:(NSDate *)secondDate
{
    NSString *sessionDate = [CalendarDataSource stringZeroInRequestFormat:firstDate];
    sessionDate = [[sessionDate componentsSeparatedByString:@" "] firstObject];
    
    NSString *requestDate = [CalendarDataSource stringZeroInRequestFormat:secondDate];
    requestDate = [[requestDate componentsSeparatedByString:@" "] firstObject];
    
    return [sessionDate isEqualToString:requestDate];
}

#pragma mark - Private

- (NSMutableArray *)compositDays:(NSArray *)days withSessions:(NSArray *)inputSessions
{
    NSMutableArray *dataSource = [[NSMutableArray alloc] init];
    NSMutableDictionary *sortedSessionByDay = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    
    for (NSDate *date in days) {
        NSMutableArray *sessions = [[NSMutableArray alloc] init];
        for (Session *selectedSession in inputSessions) {

            NSString *sessionDate = [dateFormatter stringFromDate:[selectedSession.sessionDate dateByAddingTimeInterval:timeZoneSeconds]];
            sessionDate = [[sessionDate componentsSeparatedByString:@" "] firstObject];

            NSString *requestDate = [dateFormatter stringFromDate:date];
            requestDate = [[requestDate componentsSeparatedByString:@" "] firstObject];
            
            if ([sessionDate isEqualToString:requestDate]) {
                [sessions addObject:selectedSession];
            }
            
        }
        [sortedSessionByDay setObject:sessions forKey:date];
    }
    
    NSArray *allKeys = [sortedSessionByDay allKeys];
    for (NSDate *date in days) {
        CalendarDay *newDay = [[CalendarDay alloc] init];
        for (NSDate *selectedDate in allKeys) {
            NSArray *values = [sortedSessionByDay objectForKey:selectedDate];
            if (values.count) {
                if ([CalendarDataSource isDay:selectedDate isSameDayAs:date]) {
                    newDay.calendarDayDate = date;
                    newDay.calendarSessions = values;
                }
            }
            if (!newDay.calendarDayDate) {
                newDay.calendarDayDate = date;
            }
        }
        [dataSource addObject:newDay];
    }
    return dataSource;
}

+ (NSDate *)toLocalTime:(NSDate *)date
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: date];
    return [NSDate dateWithTimeInterval: seconds sinceDate: date];
}

- (NSInteger)getGMTIntervalFromDate:(NSDate *)date
{
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    return gmtInterval / 3600;
}

- (NSArray *)getMonthWithOffsetFromCurrentMonth:(NSInteger)offset
{
    NSDate *currentDate  = [NSDate date];
    NSCalendar *gregorianCalendar = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth)  fromDate:currentDate];
    
    NSDateComponents *dt = [[NSDateComponents alloc] init];
    [dt setWeekday:1];
    [dt setDay:1];
    [dt setMonth:[components month] + offset];
    [dt setYear:[components year]];
    [dt setHour:[self getGMTIntervalFromDate:currentDate]];
    
    NSDate *firstDayOfMonth = [gregorianCalendar dateFromComponents:dt];
    
    NSDateComponents *comps = [gregorianCalendar components:NSCalendarUnitWeekday fromDate:firstDayOfMonth];
    NSInteger weekDay = [comps weekday];
    
    NSInteger offcet = -(weekDay - 2);
    
    NSMutableArray *selectedCalendarMonth = [[NSMutableArray alloc] init];
    for (NSInteger i = offcet; i < BiggestDisplayedCalendarElementsCount + offcet; i++) {
        [dt setDay:i];
        NSDate *newDateOFMonth = [gregorianCalendar dateFromComponents:dt];
        [selectedCalendarMonth addObject:newDateOFMonth];
    }
    
    return selectedCalendarMonth;
}

- (NSArray *)getWeekWithOffsetFromCurrentWeek:(NSInteger)offset
{
    NSDate *currentDate  = [NSDate date];
    NSCalendar *gregorianCalendar = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
    
    NSDateComponents *comps = [gregorianCalendar components:NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekdayOrdinal | NSCalendarUnitDay fromDate:currentDate];
    
    [comps setHour:[self getGMTIntervalFromDate:currentDate]];
    
    NSInteger dayNumberInCurrentWeekOffset = - [self numberOfWeekDayFromDate:currentDate] + 1;
    NSInteger currentDayNumber = [comps day] + offset * 7;
    
    NSMutableArray *selectedWeek = [[NSMutableArray alloc] init];
    for (NSInteger i = dayNumberInCurrentWeekOffset; i < 7 + dayNumberInCurrentWeekOffset; i++) {
        [comps setDay:(currentDayNumber + i)];
        
        NSDate *newDateOfWeek = [gregorianCalendar dateFromComponents:comps];
        [selectedWeek addObject:newDateOfWeek];
    }
    return selectedWeek;
}

@end
