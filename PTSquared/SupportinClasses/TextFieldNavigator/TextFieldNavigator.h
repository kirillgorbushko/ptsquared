//
//  TextFieldNavigator.h
//  Consent
//
//  Created by Kirill on 1/14/15.
//  Copyright (c) 2015 ThinkMobiles. All rights reserved.
//

@interface TextFieldNavigator : NSObject

+ (void)findNextTextFieldFromCurrent:(UITextField *)textField;

@end
