//
//  AutoLoginViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 10.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AutoLogin.h"
#import "CredentialFetchService.h"
#import "TrainerHomeViewController.h"
#import "TrainerNavigationController.h"
#import "ContainerViewController.h"
#import "FacebookManager.h"
#import "GoogleAnaliticsManager.h"

@interface AutoLogin ()

@end

@implementation AutoLogin

#pragma mark - Public

- (void)performAutoLoginIfPossible
{
    [[CredentialFetchService service] fetchLoginInfoResult:^(CredentialFetchKey key, NSString *login, NSString *password) {
        switch ((CredentialFetchKey)key) {
            case CredentialFetchKeyClientServer: {
                [self autoLoginToClientModeWithUserName:login password:password];
                break;
            }
            case CredentialFetchKeyClientFacebook: {
                [self autologintWithFacebookID:login];
                break;
            }
            case CredentialFetchKeyPersonalTrainer: {
                [self autoLoginToTrainerModeWithTrainerLogin:login password:password];
                break;
            }
            case CredentialFetchKeyUndefined:{
                [self markAutoLoginFinished:UNDEFINED];
                break;
            }
        }
    }];
}

#pragma mark - Private

- (void)autologintWithFacebookID:(NSString *)facebookID
{
    void (^UpdateToken)() = ^() {
        [[NetworkManager sharedManager] ptSquaredAPIUpdateToken:[NetworkManager sharedManager].deviceTokenForRemoteNotification withResult:^(BOOL success, NSError *error, id response) {
            if (success) {
                NSLog(@"%@", (NSString *)response);
            }
        }];
    };
    
    void (^PresentUI)() = ^() {
        [ProfileManager sharedManager].profileType = CLIENT;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ClientMode" bundle:nil];
        ContainerViewController *container = [storyboard instantiateViewControllerWithIdentifier:@"container"];
        container.isFirstLogin = NO;
        UIViewController *rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
        [rootViewController presentViewController:container animated:NO completion:nil];
        [self markAutoLoginFinished:CLIENT];
    };
    
    [[NetworkManager sharedManager] ptSquaredAPIClientSignInWithFacebookID:facebookID email:@"" facebookSignInResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            UpdateToken();
            [[ColorSchemeManager sharedManager] updateColorSchemeWithColor:[response valueForKey:@"main_color"]];
            [[CredentialFetchService service] setClientLogginedWithFacebookId:facebookID];
            PresentUI();
        } else {
            [self markAutoLoginFinished:UNDEFINED];
            [AppHelper alertViewWithMessage:AlertMessageUserNotConnectedWithFacebook];
        }
    }];

}

- (void)autoLoginToClientModeWithUserName:(NSString *)userName password:(NSString *)userPassword
{
    void (^PresentUI)() = ^() {
        [ProfileManager sharedManager].profileType = CLIENT;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ClientMode" bundle:nil];
        ContainerViewController *container = [storyboard instantiateViewControllerWithIdentifier:@"container"];
        container.isFirstLogin = NO;
        UIViewController *rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
        [rootViewController presentViewController:container animated:NO completion:nil];
        [self markAutoLoginFinished:CLIENT];
    };
    
    void (^UpdateToken)() = ^() {
        [[NetworkManager sharedManager] ptSquaredAPIUpdateToken:[NetworkManager sharedManager].deviceTokenForRemoteNotification withResult:^(BOOL success, NSError *error, id response) {
            if (success) {
                NSLog(@"%@", (NSString *)response);
            }
        }];
    };
    
    [[NetworkManager sharedManager] ptSquaredAPIClientSignInWithUsername:userName password:userPassword loginResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            [[GoogleAnaliticsManager sharedManager] trackClientUniqueDeviceLogin:userName];
            [[ColorSchemeManager sharedManager] updateColorSchemeWithColor:[response valueForKey:@"main_color"]];
            UpdateToken();
            PresentUI();
        } else {
            [self markAutoLoginFinished:UNDEFINED];
        }
    }];
}
- (void)autoLoginToTrainerModeWithTrainerLogin:(NSString *)login password:(NSString *)userPassword
{
    void (^PresentUI)() = ^() {
        [ProfileManager sharedManager].profileType = PERSONAL_TRAINER;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TrainerMode" bundle:nil];
        TrainerNavigationController *trainerNavigationController = [storyboard instantiateInitialViewController];
        UIViewController *rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
        [rootViewController presentViewController:trainerNavigationController animated:NO completion:nil];
        
        [self markAutoLoginFinished:PERSONAL_TRAINER];
    };
    
    void (^UpdateToken)() = ^() {
        [[NetworkManager sharedManager] ptSquaredAPIUpdateToken:[NetworkManager sharedManager].deviceTokenForRemoteNotification withResult:^(BOOL success, NSError *error, id response) {
            if (success) {
                NSLog(@"%@", (NSString *)response);
            }
        }];
    };
    
    [[NetworkManager sharedManager] ptSquaredAPITrainerSignInWithUsername:login password:userPassword loginResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            UpdateToken();
            [[GoogleAnaliticsManager sharedManager] trackTrainerUniqueDeviceLogin:login];
            [[ColorSchemeManager sharedManager] updateColorSchemeWithColor:[response valueForKey:@"main_color"]];
            PresentUI();
        } else {
            [self markAutoLoginFinished:UNDEFINED];
        }
    }];
}

- (void)markAutoLoginFinished:(ProfileType)type
{
    self.loginned = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(didFinishAutoLogin:)]){
        [self.delegate didFinishAutoLogin:type];
    }
}

@end