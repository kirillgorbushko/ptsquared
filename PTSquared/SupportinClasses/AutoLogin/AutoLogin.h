//
//  AutoLoginViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 10.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfileManager.h"

@protocol AutoLoginDelegate <NSObject>

@required
- (void)didFinishAutoLogin:(ProfileType)type;

@end

@interface AutoLogin : NSObject

@property (assign, nonatomic) BOOL loginned;
@property (weak, nonatomic) id <AutoLoginDelegate> delegate;

- (void)performAutoLoginIfPossible;

@end
