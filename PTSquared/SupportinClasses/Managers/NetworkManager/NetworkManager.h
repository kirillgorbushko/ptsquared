//
//  NetworkManager.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 06.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworkReachabilityManager.h"

typedef void(^Result)(BOOL success, NSError *error, id response);
typedef void(^DownloadingComplete)(BOOL success, UIImage *response);

@interface NetworkManager : NSObject

+ (instancetype)sharedManager;

@property (assign, nonatomic) __block AFNetworkReachabilityStatus networkStatus;
@property (copy, nonatomic) NSString *deviceTokenForRemoteNotification;
@property (assign, nonatomic) __block BOOL isUserLoggined;

#pragma mark - Reachability

- (void)startMonitoringNetwork;
- (void)stopMonitoringNetwork;

#pragma mark - Common

- (void)ptSquaredAPIUpdateToken:(NSString *)token withResult:(Result)resultTokenUpdate;
- (void)ptSquaredAPIGetImageWithPath:(NSString *)imagePath withCompletition:(DownloadingComplete)completitionHandler;

#pragma mark - Client

- (void)ptSquaredAPIClientSignOutWithResult:(Result)result;
- (void)ptSquaredAPIClientSignInWithUsername:(NSString *)userName password:(NSString *)userPassword loginResult:(Result)result;
- (void)ptSquaredAPIClientSignUpWithEmail:(NSString *)email password:(NSString *)password confirmPassword:(NSString *)confirmPssword validCode:(NSString *)validCode signUpResult:(Result)signUpresult;
- (void)ptSquaredAPIClientSignInWithFacebookID:(NSString *)facebookID email:(NSString *)email facebookSignInResult:(Result)facebookSignInResult;
- (void)ptSquaredAPIClientSignUpWithFacebookID:(NSString *)facebookID email:(NSString *)email validCode:(NSString *)validCode facebookSignUpResult:(Result)facebookSignUpResult;
- (void)ptSquaredAPIClientGetMembershipDetailsWithResult:(Result)result;
- (void)ptSquaredAPIClientProfileWithResult:(Result)result;
- (void)ptSquaredAPIClientGetMembershipMobileQRCodeForID:(NSInteger)userID withResult:(Result)qrCodeRequestResult;
- (void)ptSquaredAPIClientGetSessionAttendeeForSessionID:(NSInteger)sessionID operationresult:(Result)sessionAttendeeRequestResult;
- (void)ptSquaredAPIClientGetSessionDetailsForSessionID:(NSInteger)sessionID forDate:(NSDate *)requestedDate operationresult:(Result)sessionAttendeeRequestResult;
- (void)ptSquaredAPIClientJoinToSessionWithID:(NSInteger)sessionID isPublic:(BOOL)isPublic parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence operationResult:(Result)joinToSessionRequestResult;
- (void)ptSquaredAPIClientGetSessionInPeriodFrom:(NSDate *)startDate daysCount:(NSInteger )daysCount operationResult:(Result)trainerRequestDaysResult;
- (void)ptSquaredAPIClientGetColorWithTrainerID:(NSInteger)trainerID operationResult:(Result)clientRequestForColorResult;
- (void)ptSquaredAPIClientRequestSessionWithTrainerID:(NSInteger)trainerID dateTime:(NSDate *)dateTime operationResult:(Result)sessionRequestResult;
- (void)ptSquaredAPIClientForgotPasswordRequestForEmail:(NSString *)emailAddress operationResult:(Result)forgotPasswordRequestResult;

#pragma mark - Workout

- (void)ptSquaredAPIGetWorkoutList:(Result)workoutListRequestResult;
- (void)ptSquaredAPIGetWorkoutDetailsForWorkoutID:(NSInteger)workoutID operationResult:(Result)workoutDetailsRequestResult;
- (void)ptSquaredAPIPerformWorkoutRequestForTrainerWithID:(NSInteger)trainerID operationResult:(Result)workoutRequestResult;
- (void)ptSquaredAPIGetSessionFullWorkoutWithID:(NSInteger)sessionID operationResult:(Result)workoutDetailsRequestResult;

#pragma mark - Nutrition

- (void)ptSquaredAPIClientGetNutritionWithResult:(Result)nutritionRequestResult;
- (void)ptSquaredAPIClientGetNutritionForNutritionID:(NSInteger)nutritionID operationResult:(Result)nutritionForIDRequestResult;
- (void)ptSquaredAPIClientGetNutritionRequestForTrainerWithID:(NSInteger)trainerID operationResult:(Result)nutritionForTrainerIDRequestResult;
- (void)ptSquaredAPIClientGetNutritionDetailsForNutritionID:(NSInteger)nutritionID operationResult:(Result)nutritionForIDRequestResult;

#pragma mark - Trainer

- (void)ptSquaredAPITrainerSignOutWithResult:(Result)result;
- (void)ptSquaredAPITrainerSignInWithUsername:(NSString *)userName password:(NSString *)userPassword loginResult:(Result)result;
- (void)ptSquaredAPITrainerGetPaidStatusFromQRCodeFromSessionID:(NSInteger)sessionID parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence clientQrCode:(NSString *)clientQRCode operationResult:(Result)qrCodeFromSessionRequestResult;
- (void)ptSquaredAPITrainerGetSessionDetailsForSessionID:(NSInteger)sessionID sessionDate:(NSDate *)sessionDate operationresult:(Result)sessionAttendeeRequestResult;
- (void)ptSquaredAPITrainerGetSessionAttendeeForSessionID:(NSInteger)sessionID operationresult:(Result)sessionAttendeeRequestResult;
- (void)ptSquaredAPITrainerAddAttendeesFromIDsArray:(NSArray *)attendeesIDs forSessionID:(NSInteger)sessionID parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence result:(Result)addAttendeesRequestResult;
- (void)ptSquaredAPITrainerGetScheduleForDay:(NSDate *)requestedDay result:(Result)schedulerDayRequestResult;
- (void)ptSquaredAPITrainerGetAvailableSessionCountForClientID:(NSInteger)clientID operationResult:(Result)avaliableSessionRequestResult;
- (void)ptSquaredAPITrainerSetSessionCompletedForSessionID:(NSInteger)sessionID parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence operationResult:(Result)setSessionCompletedOperationResult;
- (void)ptSquaredAPITrainerDeductSessionWithID:(NSInteger)sessionID forClient:(NSInteger)clientID parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence operationResult:(Result)deductSessionRequestResult;
- (void)ptSquaredAPITrainerReceivePaymentForSessionWithID:(NSInteger)sessionID forClient:(NSInteger)clientID operationResult:(Result)deductSessionRequestResult;
- (void)ptSquaredAPITrainerGetSessionInPeriodFrom:(NSDate *)startDate daysCount:(NSInteger )daysCount operationResult:(Result)trainerRequestDaysResult;
- (void)ptSquaredAPITrainerGetColor:(Result)trainerRequestForColorResult;
- (void)ptSquaredAPITrainerSearchClientWithName:(NSString *)name count:(NSInteger)count offset:(NSInteger)offset searchResult:(Result)searchResult;
- (void)ptSquaredApiTrainerAddClientWithFirstName:(NSString *)firstName lastName:(NSString *)lastName email:(NSString *)email phone:(NSString *)phone sessionID:(NSInteger)sessionID  parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence addClientResult:(Result)addClientResult;
- (void)ptSquaredAPITrainerForgotPasswordRequestForEmail:(NSString *)emailAddress operationResult:(Result)forgotPasswordRequestResult;

@end
