//
//  NetworkManager.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 06.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworking.h"
#import "NetworkManagerDefines.h"
#import "CalendarDataSource.h"
#import "ColorSchemeManager.h"
#import "MembershipProfile.h"
#import "ClientProfile.h"
#import "Nutrition.h"
#import "Workout.h"
#import "Exercise.h"
#import "Session.h"
#import "CredentialFetchService.h"

static NSString *const NetworkManagerDomain = @"Network Manager";

@interface NetworkManager()

@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;
@property (strong, nonatomic) CalendarDataSource *calendarDataSource;

@end

@implementation NetworkManager

#pragma mark - Public

+ (instancetype)sharedManager
{
    static NetworkManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[NetworkManager alloc] init];
    });
    return sharedManager;
}

#pragma mark - Trainer

- (void)ptSquaredAPITrainerSignOutWithResult:(Result)result
{
    NSDictionary *parameters = @{
                                 @"device_token" : [[CredentialFetchService service] getClientPushToken].length ? [[CredentialFetchService service] getClientPushToken] : @""
                                 };

    [self performPOST:ptSquaredTrainerSignOutPath withParameters:parameters postResult:result];
}

- (void)ptSquaredAPITrainerSignInWithUsername:(NSString *)userName password:(NSString *)userPassword loginResult:(Result)result
{
    NSMutableDictionary *parameters = [@{
                                         @"login" : userName,
                                         } mutableCopy];
    if (userPassword.length){
        [parameters setValue:userPassword forKey:@"pass"];
    }
    
    [self performPOST:ptSquaredTrainerSingInPath withParameters:parameters postResult:result];
}

- (void)ptSquaredAPITrainerGetPaidStatusFromQRCodeFromSessionID:(NSInteger)sessionID parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence clientQrCode:(NSString *)clientQRCode operationResult:(Result)qrCodeFromSessionRequestResult
{
    NSDictionary *parameters = @{
                                 @"sessionId" : @(sessionID),
                                 @"parent_id" : @(parentID),
                                 @"date_time" : time,
                                 @"reoccurence" : @(reoccurence)
                                 };
    
    NSString *stringCleanPath = [clientQRCode stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self.manager POST:stringCleanPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        qrCodeFromSessionRequestResult (YES, nil, [response valueForKey:@"success"]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *message = @"Incorrect QRCode. Please try again.";

        if (operation.responseObject) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseObject options:kNilOptions error:&error];
            if ([response valueForKey:@"error"]) {
                message = [response valueForKey:@"error"];
            }
        }
        qrCodeFromSessionRequestResult(NO, error, message);
    }];
}

- (void)ptSquaredAPITrainerGetSessionDetailsForSessionID:(NSInteger)sessionID sessionDate:(NSDate *)sessionDate operationresult:(Result)sessionAttendeeRequestResult
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:sessionDate];

    NSString *requestPath = [ptSquaredTrainerSessionID stringByAppendingPathComponent:@(sessionID).stringValue];
    requestPath = [[requestPath stringByAppendingString:@"?date="] stringByAppendingString:dateString];
    
    [self.manager GET:requestPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        Session *requestedSessions = [Session getNextSessionFromDictionary:response];
        sessionAttendeeRequestResult (YES, nil, requestedSessions);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        sessionAttendeeRequestResult (NO, error, nil);
    }];
}

- (void)ptSquaredAPITrainerGetSessionAttendeeForSessionID:(NSInteger)sessionID operationresult:(Result)sessionAttendeeRequestResult
{
    NSString *requestPath = [ptSquaredTrainerSessionAttendees stringByAppendingPathComponent:@(sessionID).stringValue];
    
    [self.manager GET:requestPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *inputArray = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSArray *responseArray = [ClientProfile prepareArrayWithProfilesFromResponseArray:inputArray];
        sessionAttendeeRequestResult (YES, nil, responseArray);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        sessionAttendeeRequestResult (NO, error, nil);
    }];
}

- (void)ptSquaredAPITrainerGetScheduleForDay:(NSDate *)requestedDay result:(Result)schedulerDayRequestResult
{
    NSDictionary *parameters = [self.calendarDataSource dateComponentsFromDate:requestedDay];
    
    NSString *requestURL = [NSString stringWithFormat:@"%@?date=%@-%@-%@", ptSquaredTrainerSessionDayScheduler, [parameters valueForKey:@"year"], [parameters valueForKey:@"month"], [parameters valueForKey:@"day"]];
    
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *inputArray = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSArray *objects = [Session prepareArrayWithSessionFromResponseArray:inputArray];
        schedulerDayRequestResult (YES, nil, objects);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        schedulerDayRequestResult(NO, error, operation);
    }];
}

- (void)ptSquaredAPITrainerAddAttendeesFromIDsArray:(NSArray *)attendeesIDs forSessionID:(NSInteger)sessionID parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence result:(Result)addAttendeesRequestResult
{
    NSDictionary *parameters = @{
                                 @"sessionId" : @(sessionID),
                                 @"clients" : attendeesIDs,
                                 @"parent_id" : @(parentID),
                                 @"date_time" : time,
                                 @"reoccurence" : @(reoccurence)
                                 };
    [self performPOST:ptSquaredTrainerSessionAddAttendees withParameters:parameters postResult:addAttendeesRequestResult];
}

- (void)ptSquaredAPITrainerGetAvailableSessionCountForClientID:(NSInteger)clientID operationResult:(Result)avaliableSessionRequestResult
{
    NSString *requestPath = [ptSquaredTrainerSessionAvailable stringByAppendingPathComponent:@(clientID).stringValue];
    
    [self.manager POST:requestPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSString *response = [json valueForKey:@"sessions"];
        avaliableSessionRequestResult (YES, nil, response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        avaliableSessionRequestResult (NO, error, nil);
    }];
}

- (void)ptSquaredAPITrainerSetSessionCompletedForSessionID:(NSInteger)sessionID parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence operationResult:(Result)setSessionCompletedOperationResult
{
    NSString *requestPath = [ptSquaredTrainerSessionSetCompleted stringByAppendingPathComponent:@(sessionID).stringValue];
    
    NSDictionary *parameters = @{
                                 @"parent_id" : @(parentID),
                                 @"date_time" : time,
                                 @"reoccurence" : @(reoccurence)
                                 };
    
    [self.manager PUT:requestPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSString *response = [json valueForKey:@"success"];
        setSessionCompletedOperationResult (YES, nil, response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        setSessionCompletedOperationResult (NO, error, nil);
    }];
}

- (void)ptSquaredAPITrainerDeductSessionWithID:(NSInteger)sessionID forClient:(NSInteger)clientID parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence operationResult:(Result)deductSessionRequestResult
{
    NSDictionary *parameters = @{
                     @"sessionId" : @(sessionID).stringValue,
                     @"clientId" : @(clientID).stringValue,
                     @"parent_id" : @(parentID),
                     @"date_time" : time,
                     @"reoccurence" : @(reoccurence)
                     };
    
    [self.manager PUT:ptSquaredTrainerSessionDeductSession parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSDictionary *response = [json valueForKey:@"success"];
        Session *updatedSession = [Session getNextSessionFromDictionary:response];
        deductSessionRequestResult (YES, nil, updatedSession);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        deductSessionRequestResult (NO, error, nil);
    }];
}

- (void)ptSquaredAPITrainerReceivePaymentForSessionWithID:(NSInteger)sessionID forClient:(NSInteger)clientID operationResult:(Result)deductSessionRequestResult
{
    NSDictionary *parameters = @{
                                 @"sessionId" : @(sessionID).stringValue,
                                 @"clientId" : @(clientID).stringValue
                                 };
    
    [self.manager PUT:ptSquaredTrainerSessionReceivePayment parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSString *response = [json valueForKey:@"success"];
        deductSessionRequestResult (YES, nil, response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        deductSessionRequestResult (NO, error, nil);
    }];
}

- (void)ptSquaredAPITrainerGetSessionInPeriodFrom:(NSDate *)startDate daysCount:(NSInteger )daysCount operationResult:(Result)trainerRequestDaysResult
{
    NSString *date = [CalendarDataSource stringZeroInRequestFormat:startDate];
    date = [[date componentsSeparatedByString:@" "] firstObject];
    date = [date stringByAppendingString:@" 00:00:00"];

    NSString *requestURL = [NSString stringWithFormat:@"%@?date=%@&period=%li", ptSquaredTrainerSessionsPeriod, date, (long)daysCount];
    NSString *stringCleanPath = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self.manager GET:stringCleanPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *inputArray = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSArray *objects = [Session prepareArrayWithSessionFromResponseArray:inputArray];
        trainerRequestDaysResult (YES, nil, objects);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        trainerRequestDaysResult(NO, error, operation);
    }];
}

- (void)ptSquaredAPITrainerGetColor:(Result)trainerRequestForColorResult
{
    [self.manager GET:ptSquaredGetColor parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *responseData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
        NSString *response = [responseDictionary valueForKey:@"main_color"];
        if (!response) {
            response = DefaultColor;
        }
        trainerRequestForColorResult(YES, nil, response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        trainerRequestForColorResult (NO, error, nil);
    }];
}

- (NSString *)stringByRemovingControlCharactersFromString:(NSString *)string
{
    NSCharacterSet *controlChars = [NSCharacterSet controlCharacterSet];
    NSRange range = [string rangeOfCharacterFromSet:controlChars];
    if (range.location != NSNotFound) {
        NSMutableString *mutable = [NSMutableString stringWithString:string];
        while (range.location != NSNotFound) {
            [mutable deleteCharactersInRange:range];
            range = [mutable rangeOfCharacterFromSet:controlChars];
        }
        return mutable;
    }
    return string;
}

- (void)ptSquaredAPITrainerSearchClientWithName:(NSString *)name count:(NSInteger)count offset:(NSInteger)offset searchResult:(Result)searchResult
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"name" : name}];
    
    if (count) {
        [parameters setValue:@(count) forKey:@"count"];
    }
    
    if (offset) {
        [parameters setValue:@(offset) forKey:@"offset"];
    }
    
    [self.manager GET:ptSquaredTrainerSearchClient parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *responseArray = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        searchResult(YES, nil, [ClientProfile prepareArrayWithProfilesFromResponseArray:responseArray]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        searchResult(NO, error, nil);
    }];
}

- (void)ptSquaredApiTrainerAddClientWithFirstName:(NSString *)firstName lastName:(NSString *)lastName email:(NSString *)email phone:(NSString *)phone sessionID:(NSInteger)sessionID  parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence addClientResult:(Result)addClientResult
{
    NSDictionary *parameters = @{
                                 @"first_name":firstName,
                                 @"last_name":lastName,
                                 @"email":email,
                                 @"phone":phone,
                                 @"sessionId":@(sessionID).stringValue,
                                 @"parent_id" : @(parentID),
                                 @"date_time" : time,
                                 @"reoccurence" : @(reoccurence)
                                 };
    
    [self.manager POST:ptSquaredTrainerAddClient parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        addClientResult(YES, nil, json[@"success"]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *responsedString = error.localizedDescription;
        if (operation.responseObject) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseObject options:kNilOptions error:&error];
            responsedString = [response valueForKey:@"error"];
        }
        addClientResult(NO, error, responsedString);
    }];
    
}

#pragma mark - Client

- (void)ptSquaredAPIClientSignOutWithResult:(Result)result
{
    NSDictionary *parameters = @{
                                 @"device_token" : [[CredentialFetchService service] getClientPushToken].length ? [[CredentialFetchService service] getClientPushToken] : @""
                                 };
    [self performPOST:ptSquaredClientSignOutPath withParameters:parameters postResult:result];
}

- (void)ptSquaredAPIClientSignInWithUsername:(NSString *)userName password:(NSString *)userPassword loginResult:(Result)result
{
    NSDictionary *parameters = @{
                                 @"email" : userName,
                                 @"pass" : userPassword
                                 };
    
    [self performPOST: ptSquaredClientSignInPath withParameters:parameters postResult:result];
}

- (void)ptSquaredAPIClientSignUpWithEmail:(NSString *)email password:(NSString *)password confirmPassword:(NSString *)confirmPassword validCode:(NSString *)validationCode signUpResult:(Result)signUpresult
{
    NSDictionary *parameters = @{
                                 @"email" : email,
                                 @"pass" : password,
                                 @"pass_confirm" : confirmPassword,
                                 @"valid_code" : validationCode
                                 };
    
    [self performPOST:ptSquaredClientSignUpPath withParameters:parameters postResult:signUpresult];
}

- (void)ptSquaredAPIClientSignInWithFacebookID:(NSString *)facebookID email:(NSString *)email facebookSignInResult:(Result)facebookSignInResult
{
    NSDictionary *parameters = @{
                                 @"facebook_id" : facebookID,
                                 @"email" : email
                                 };
    [self performPOST:ptSquaredClientSignInFacebookPath withParameters:parameters postResult:facebookSignInResult];
}

- (void)ptSquaredAPIClientSignUpWithFacebookID:(NSString *)facebookID email:(NSString *)email validCode:(NSString *)validCode facebookSignUpResult:(Result)facebookSignUpResult
{
    NSDictionary *parameters = @{
                                 @"facebook_id" : facebookID,
                                 @"email" : email,
                                 @"valid_code" : validCode
                                 };
    
    [self performPOST:ptSquaredClientSignUpFacebookPath withParameters:parameters postResult:facebookSignUpResult];
}

- (void)ptSquaredAPIClientGetMembershipDetailsWithResult:(Result)result
{
    [self.manager GET:ptSquaredClientMembershipDetails parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        MembershipProfile *clientProfile = [MembershipProfile getMembershipProfileFromDictionary:response];
        if (error) {
            result(NO, error, nil);
        } else {
            result(YES, nil, clientProfile);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        result(NO, error, operation);
    }];
}

- (void)ptSquaredAPIClientGetMembershipMobileQRCodeForID:(NSInteger)userID withResult:(Result)qrCodeRequestResult
{
    NSString *requestURL = [ptSquaredClientMembershipMobileQR stringByAppendingPathComponent:@(userID).stringValue];
    
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        qrCodeRequestResult (YES, nil, [response valueForKey:@"success"]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        qrCodeRequestResult(NO, error, operation);
    }];
}

- (void)ptSquaredAPIClientProfileWithResult:(Result)result
{
    [self.manager GET:ptSquaredClientProfile parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        result(YES, nil, [ClientProfile dataSourceFromResponseDictionary:response]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        result(NO, error, nil);
    }];
}

- (void)ptSquaredAPIClientGetSessionAttendeeForSessionID:(NSInteger)sessionID operationresult:(Result)sessionDetailsRequestResult
{
    NSString *requestURL = [ptSquaredSessionAttendees stringByAppendingPathComponent:@(sessionID).stringValue];
    
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *inputArray = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSArray *responseArray = [ClientProfile prepareArrayWithProfilesFromResponseArray:inputArray];
        sessionDetailsRequestResult(YES, nil, responseArray);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        sessionDetailsRequestResult(NO, error, operation);
    }];
}

- (void)ptSquaredAPIClientGetSessionDetailsForSessionID:(NSInteger)sessionID forDate:(NSDate *)requestedDate operationresult:(Result)sessionAttendeeRequestResult
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:requestedDate];
    
    NSString *requestPath = [ptSquaredSessionDetails stringByAppendingPathComponent:@(sessionID).stringValue];
    requestPath = [[requestPath stringByAppendingString:@"?date="] stringByAppendingString:dateString];
    
    [self.manager GET:requestPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        Session *requestedSessions = [Session getNextSessionFromDictionary:response];
        sessionAttendeeRequestResult(YES, nil, requestedSessions);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        sessionAttendeeRequestResult(NO, error, operation);
    }];
}

- (void)ptSquaredAPIClientJoinToSessionWithID:(NSInteger)sessionID isPublic:(BOOL)isPublic parentID:(NSInteger)parentID sessionTime:(NSString *)time reoccurence:(NSInteger)reoccurence operationResult:(Result)joinToSessionRequestResult
{
    NSString *path = [ptSquaredClientJoinSession stringByAppendingPathComponent:@(sessionID).stringValue];
    
    NSDictionary *parameters = @{
                                 @"is_public":@(isPublic),
                                 @"parent_id" : @(parentID),
                                 @"date_time" : time,
                                 @"reoccurence" : @(reoccurence)
                                 };
    
    [self.manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSString *response = [json valueForKey:@"success"];
        joinToSessionRequestResult(YES, nil, response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *responsedString = error.localizedDescription;
        if (operation.responseObject) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseObject options:kNilOptions error:&error];
            responsedString = [response valueForKey:@"error"];
        }
        joinToSessionRequestResult(NO, error, responsedString);
    }];
}

- (void)ptSquaredAPIClientGetSessionInPeriodFrom:(NSDate *)startDate daysCount:(NSInteger )daysCount operationResult:(Result)trainerRequestDaysResult
{
    NSString *date = [CalendarDataSource stringZeroInRequestFormat:startDate];
    date = [[date componentsSeparatedByString:@" "] firstObject];
    date = [date stringByAppendingString:@" 00:00:00"];

    NSString *requestURL = [NSString stringWithFormat:@"%@?date=%@&period=%li", ptSquaredClientSessionsPeriod, date, (long)daysCount];
    NSString *stringCleanPath = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self.manager GET:stringCleanPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *inputArray = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSArray *objects = [Session prepareArrayWithSessionFromResponseArray:inputArray];
        trainerRequestDaysResult (YES, nil, objects);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        trainerRequestDaysResult(NO, error, operation);
    }];
}

- (void)ptSquaredAPIClientGetColorWithTrainerID:(NSInteger)trainerID operationResult:(Result)clientRequestForColorResult
{
    NSString *requestURL = [NSString stringWithFormat:@"%@?trainerId=%li", ptSquaredGetColor, (long)trainerID];
    
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *responseData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
        NSString *response = [responseDictionary valueForKey:@"main_color"];
        if (!response) {
            response = DefaultColor;
        }
        clientRequestForColorResult(YES, nil, response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        clientRequestForColorResult(NO, error, nil);
    }];
}

- (void)ptSquaredAPIClientRequestSessionWithTrainerID:(NSInteger)trainerID dateTime:(NSDate *)dateTime operationResult:(Result)sessionRequestResult
{
    NSDictionary *parameters = @{
                                 @"trainerId": @(trainerID).stringValue,
                                 @"dateTime": [CalendarDataSource stringFromDateInRequestFormat:dateTime]
                                 };
    
    [self.manager POST:ptSquaredClientSessionRequest parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSString *response = [json valueForKey:@"message"];
        sessionRequestResult(YES, nil, response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        sessionRequestResult(NO, error, nil);
    }];
}

#pragma mark - Common

- (void)ptSquaredAPIUpdateToken:(NSString *)token withResult:(Result)resultTokenUpdate
{
#if !TARGET_IPHONE_SIMULATOR
    if (!token.length) {
        return;
    }
    NSDictionary *parameters = @{
                                 @"device_token" : token
                                 };
    [self performPOST:ptSquaredUpdateToken withParameters:parameters postResult:resultTokenUpdate];
#else
    NSLog(@"PUSH not supported for simulator");
#endif
}

- (void)ptSquaredAPITrainerForgotPasswordRequestForEmail:(NSString *)emailAddress operationResult:(Result)forgotPasswordRequestResult
{
    NSDictionary *parameters = @{
                                 @"email" : emailAddress
                                 };
    
    [self performPOST:ptSquaredTrainerForgotPassword withParameters:parameters postResult:forgotPasswordRequestResult];
}

- (void)ptSquaredAPIClientForgotPasswordRequestForEmail:(NSString *)emailAddress operationResult:(Result)forgotPasswordRequestResult
{
    NSDictionary *parameters = @{
                                 @"email" : emailAddress
                                 };
    
    [self performPOST:ptSquaredClientForgotPassword withParameters:parameters postResult:forgotPasswordRequestResult];
}

#pragma mark - Workout

- (void)ptSquaredAPIGetWorkoutList:(Result)workoutListRequestResult
{
    [self.manager GET:ptSquaredWorkoutList parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *inputArray = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSArray *workouts = [Workout getListOfWorkoutsFromInputArray:inputArray];
        
        NSArray *sortedWorkouts = [workouts sortedArrayUsingComparator:^NSComparisonResult(Workout  * __nonnull workout1, Workout   * __nonnull workout2) {
            return [workout2.workoutSendedDate compare:workout1.workoutSendedDate];
        }];
        
        workoutListRequestResult (YES, nil, sortedWorkouts);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        workoutListRequestResult (NO, error, nil);
    }];
}

- (void)ptSquaredAPIGetWorkoutDetailsForWorkoutID:(NSInteger)workoutID operationResult:(Result)workoutDetailsRequestResult
{
    NSString *requestURL = [ptSquaredWorkoutDetails stringByAppendingPathComponent:@(workoutID).stringValue];
    
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *inputData = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        Workout *requestedWorkout = [Workout getWorkoutFromResponseDictionary:inputData];
        workoutDetailsRequestResult (YES, nil, requestedWorkout);

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        workoutDetailsRequestResult (NO, error, nil);
    }];
}

- (void)ptSquaredAPIPerformWorkoutRequestForTrainerWithID:(NSInteger)trainerID operationResult:(Result)workoutRequestResult
{
    NSString *requestURL = [ptSquaredWorkoutRequest stringByAppendingPathComponent:@(trainerID).stringValue];
    
    [self.manager POST:requestURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSString *resultResponse = [json valueForKey:@"message"];
        workoutRequestResult(YES, nil, resultResponse);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *responsedString = error.localizedDescription;
        if (operation.responseObject) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseObject options:kNilOptions error:&error];
            responsedString = [response valueForKey:@"error"];
        }
        workoutRequestResult (NO, error, responsedString);
    }];
}

- (void)ptSquaredAPIGetSessionFullWorkoutWithID:(NSInteger)sessionID operationResult:(Result)workoutDetailsRequestResult
{
    NSString *requestURL = [ptSquaredWorkoutSessionWorkout stringByAppendingPathComponent:@(sessionID).stringValue];
    
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        Workout *workout = [Workout getWorkoutFromResponseDictionary:json];

        workoutDetailsRequestResult (YES, nil, workout);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        workoutDetailsRequestResult (NO, error, nil);
    }];
}

#pragma mark - Nutrition

- (void)ptSquaredAPIClientGetNutritionWithResult:(Result)nutritionRequestResult
{
    [self.manager GET:ptSquaredClientNutrition parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSArray *nutritions = [Nutrition getListOfNutritionFromInputArray:response];
        
        NSArray *sortedNutritions = [nutritions sortedArrayUsingComparator:^NSComparisonResult(Nutrition   * __nonnull nutrition1, Nutrition   * __nonnull nutrition2) {
            return [nutrition2.nutritionSendedDate compare:nutrition1.nutritionSendedDate];
        }];

        if (error) {
            nutritionRequestResult(NO, error, nil);
        } else {
            nutritionRequestResult(YES, nil, sortedNutritions);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        nutritionRequestResult(NO, error, nil);
    }];
}

- (void)ptSquaredAPIClientGetNutritionForNutritionID:(NSInteger)nutritionID operationResult:(Result)nutritionForIDRequestResult
{
    NSString *path = [ptSquaredClientNutrition stringByAppendingPathComponent:@(nutritionID).stringValue];
    [self.manager GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            nutritionForIDRequestResult(NO, error, nil);
        } else {
            nutritionForIDRequestResult(YES, nil, [Nutrition nutritionFromResponseDictionary:response]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        nutritionForIDRequestResult(NO, error, operation);
    }];
}

- (void)ptSquaredAPIClientGetNutritionRequestForTrainerWithID:(NSInteger)trainerID operationResult:(Result)nutritionForTrainerIDRequestResult
{
    NSString *path = [ptSquaredClientNutritionRequest stringByAppendingPathComponent:@(trainerID).stringValue];
    [self.manager POST:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSString *response = [json valueForKey:@"message"];
        nutritionForTrainerIDRequestResult(YES, nil, response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *responsedString = error.localizedDescription;
        if (operation.responseObject) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseObject options:kNilOptions error:&error];
            responsedString = [response valueForKey:@"error"];
        }
        nutritionForTrainerIDRequestResult(NO, error, responsedString);
    }];
}

- (void)ptSquaredAPIClientGetNutritionDetailsForNutritionID:(NSInteger)nutritionID operationResult:(Result)nutritionForIDRequestResult
{
    NSString *path = [ptSquaredClientGetNutritionDetails stringByAppendingPathComponent:@(nutritionID).stringValue];
    [self.manager GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        NSDictionary *dicResponse = [response firstObject];
        if (error) {
            nutritionForIDRequestResult(NO, error, nil);
        } else {
            nutritionForIDRequestResult(YES, nil, [Nutrition nutritionFromResponseDictionary:dicResponse]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        nutritionForIDRequestResult(NO, error, operation);
    }];
}

- (void)ptSquaredAPIGetImageWithPath:(NSString *)imagePath withCompletition:(DownloadingComplete)completitionHandler
{
    [self.manager GET:imagePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        UIImage *image = [UIImage imageWithData:responseObject];
        if (image) {
            completitionHandler(YES, image);
        } else {
            completitionHandler(NO, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completitionHandler(NO, nil);
    }];
}

#pragma mark - Private

- (void)performPOST:(NSString *)postAddress withParameters:(NSDictionary *)parameters postResult:(Result)result
{
    __weak typeof(self) weakSelf = self;
    [self.manager POST:postAddress parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        if (response[@"success"]) {
            
            if ([postAddress isEqualToString:ptSquaredTrainerSingInPath] || [postAddress isEqualToString:ptSquaredClientSignInPath]) {
                weakSelf.isUserLoggined = YES;
            }
            
            result(YES, nil, response);
        } else {
            error = [AppHelper errorWithDescription:response[@"error"] inDomain:NetworkManagerDomain];
            result(NO, error, response[@"error"]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *responsedString = error.localizedDescription;
        if (operation.responseObject) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseObject options:kNilOptions error:&error];
            if (response){
                responsedString = [response valueForKey:@"error"];
            }
        }
        result(NO, error, responsedString);
    }];
}

- (void)performGET:(NSString *)getPath withParameters:(NSDictionary *)parameters getResult:(Result)result
{
    [self.manager GET:getPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        if (response[@"success"]) {
            result(YES, nil, response);
        } else {
            error = [AppHelper errorWithDescription:response[@"error"] inDomain:NetworkManagerDomain];
            result(NO, error, response[@"error"]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        result(NO, error, operation);
    }];
}

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self managerSetup];
        [self prepareLocalVariables];
    }
    return self;
}

#pragma mark - Setups

- (void)managerSetup
{
    self.manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:APIURL]];
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString *userAgent = [self.manager.requestSerializer  valueForHTTPHeaderField:@"User-Agent"];
    userAgent = [userAgent stringByAppendingPathComponent:@"pt2ios"];
    [self.manager.requestSerializer setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    self.manager.securityPolicy = securityPolicy;
}

- (void)prepareLocalVariables
{
    self.calendarDataSource = [[CalendarDataSource alloc] init];
}

#pragma mark - Reachability

- (void)startMonitoringNetwork
{
    __weak typeof(self) weakSelf = self;
    [self.manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        weakSelf.networkStatus = status;
    }];
    [self.manager.reachabilityManager startMonitoring];
}

- (void)stopMonitoringNetwork
{
    [self.manager.reachabilityManager stopMonitoring];
}


@end
