//
//  NetworkManagerDefines.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 07.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

static NSString *const APIURL =
//@"http://192.168.89.101:7795/";
@"http://projects.thinkmobiles.com:7795/";

#pragma mark - Common

static NSString *const ptSquaredSessionAttendees = @"/client/sessionAttendees";
static NSString *const ptSquaredSessionDetails = @"/client/sessionDetails";
static NSString *const ptSquaredGetColor = @"/trainerColor/color";
static NSString *const ptSquaredTrainerForgotPassword = @"/forgotPass";
static NSString *const ptSquaredClientForgotPassword = @"/forgotPassClient";
static NSString *const ptSquaredUpdateToken = @"/updateToken";

#pragma mark - Client

static NSString *const ptSquaredClientSignOutPath = @"/signOut";
static NSString *const ptSquaredClientSignInPath = @"/signInClient";
static NSString *const ptSquaredClientSignUpPath = @"/signUpClient";
static NSString *const ptSquaredClientSignUpFacebookPath = @"/signUpClientFb";
static NSString *const ptSquaredClientSignInFacebookPath = @"/signInClientFb";
static NSString *const ptSquaredClientMembershipDetails = @"/membership/profile";
static NSString *const ptSquaredClientMembershipMobileQR = @"/membership/mobileQr";
static NSString *const ptSquaredClientGetSchedule = @"/client/scheduler";
static NSString *const ptSquaredClientProfile = @"/client/profile";
static NSString *const ptSquaredClientJoinSession = @"/client/sessionJoin";
static NSString *const ptSquaredClientSessionsPeriod = @"/client/sessionPeriod";
static NSString *const ptSquaredClientSessionRequest = @"/client/sessionRequest";

#pragma mark - Workout

static NSString *const ptSquaredWorkoutList = @"/workout";
static NSString *const ptSquaredWorkoutDetails = @"/workout";
static NSString *const ptSquaredWorkoutRequest = @"/workout/request";
static NSString *const ptSquaredWorkoutSessionWorkout = @"/workout/getSessionFullWorkouts";

#pragma mark - Nutrition

static NSString *const ptSquaredClientNutrition = @"/nutrition";
static NSString *const ptSquaredClientNutritionRequest = @"/nutrition/request";
static NSString *const ptSquaredClientGetNutritionDetails = @"/nutrition/details";

#pragma mark - Trainer

static NSString *const ptSquaredTrainerSignOutPath = @"/signOut";
static NSString *const ptSquaredTrainerSingInPath = @"/signIn";
static NSString *const ptSquaredTrainerQRCode = @"/clientQr";
static NSString *const ptSquaredTrainerSessionMonthScheduler = @"/sessions/scheduler";
static NSString *const ptSquaredTrainerSessionDayScheduler = @"/sessions/date";
static NSString *const ptSquaredTrainerSessionID = @"/sessions";
static NSString *const ptSquaredTrainerSessionAttendees = @"/sessions/sessionAttendees";
static NSString *const ptSquaredTrainerSessionAddAttendees = @"/sessions/addAttendees";
static NSString *const ptSquaredTrainerSessionAvailable = @"/sessions/sessionsAvailable/";
static NSString *const ptSquaredTrainerSessionSetCompleted = @"/sessions/sessionCompleted";
static NSString *const ptSquaredTrainerSessionDeductSession = @"/sessions/sessionAttendees";
static NSString *const ptSquaredTrainerSessionReceivePayment = @"/sessions/receivePayment";
static NSString *const ptSquaredTrainerSessionsPeriod = @"sessions/sessionPeriod";
static NSString *const ptSquaredTrainerSearchClient = @"/client/search";
static NSString *const ptSquaredTrainerAddClient = @"/client/createClient";