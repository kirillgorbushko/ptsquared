//
//  GoogleAnaliticsManager.h
//  
//
//  Created by Kirill Gorbushko on 11.03.15.
//  Copyright (c) 2015 ThinkMobiles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GAI.h"

@interface GoogleAnaliticsManager : NSObject

@property (strong, nonatomic) id <GAITracker> tracker;

+ (instancetype)sharedManager;

#pragma mark - Client

- (void)trackClientRegistration;
- (void)trackClientUniqueDeviceLogin:(NSString *)clientEmail;
- (void)trackClientUniqueLogin;
- (void)trackClientFacebookSharing;
- (void)trackClientWorkoutRequestWithClientID:(NSInteger)clientID;
- (void)trackClientNutritionRequestWithClientID:(NSInteger)clientID;
- (void)trackClientSessionRequestWithClientID:(NSInteger)clientID;

- (void)trackClientTimeBetweenAppLaunch;
- (void)saveLaunchTime;
- (void)saveCloseTime;

#pragma mark - Trainer

- (void)trackTrainerQRCodeScanForSessionID:(NSInteger)sessionID scannedQRCodeTimes:(NSInteger)countOfScannedCode;
- (void)trackTrainerUniqueDeviceLogin:(NSString *)trainerEmail;
- (void)trackTrainerUniqueLogin;

@end
