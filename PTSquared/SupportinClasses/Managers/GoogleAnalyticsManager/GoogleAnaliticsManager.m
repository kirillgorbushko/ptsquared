//
//  GoogleAnaliticsManager.m
//  
//
//  Created by Kirill Gorbushko on 11.03.15.
//  Copyright (c) 2015 ThinkMobiles. All rights reserved.
//

#import "GoogleAnaliticsManager.h"
#import "GAIDictionaryBuilder.h"

static NSString *const GATrackingID = @"UA-64695549-1";

static NSString *const TrainerKey = @"TrainersLoginned";
static NSString *const ClientKey = @"ClientLoginned";
static NSString *const KEYTimeAppWasOpened = @"KEYTimeAppWasOpened";
static NSString *const KEYTimeAppWasClosed = @"KEYTimeAppWasClosed";

@implementation GoogleAnaliticsManager

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[GAI sharedInstance] trackerWithTrackingId:GATrackingID];
        self.tracker = [[GAI sharedInstance] defaultTracker];
        [GAI sharedInstance].dispatchInterval = 20;
    }
    return self;
}

#pragma mark - Public

+ (instancetype)sharedManager
{
    static GoogleAnaliticsManager *sharedManager = nil;
    static dispatch_once_t tocken;
    dispatch_once(&tocken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

#pragma mark - Client

- (void)trackClientRegistration
{
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Client" action:@"Application Launch" label:@"Client registration" value:@(1)] build]];
}

- (void)trackClientUniqueDeviceLogin:(NSString *)clientEmail;
{
    NSMutableArray *trainerIDs = [[[NSUserDefaults standardUserDefaults] valueForKey:ClientKey] mutableCopy];
    if (![trainerIDs containsObject:clientEmail]) {
        [trainerIDs addObject:clientEmail];
        
        [[NSUserDefaults standardUserDefaults] setObject:trainerIDs forKey:ClientKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Client" action:@"Client device uqique login" label:clientEmail value:@(1)] build]];
    }
}

- (void)trackClientUniqueLogin
{
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Client" action:@"Client Unique Login" label:@"Client unique login" value:@(1)] build]];
}

- (void)trackClientFacebookSharing
{
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Client" action:@"Sharing" label:@"Client facebook share" value:@(1)] build]];
}

- (void)trackClientWorkoutRequestWithClientID:(NSInteger)clientID
{
    NSString *clientIDString = [NSString stringWithFormat:@"%i", (int)clientID];
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Client" action:@"Client workout request" label:clientIDString value:@(1)] build]];
}

- (void)trackClientNutritionRequestWithClientID:(NSInteger)clientID
{
    NSString *clientIDString = [NSString stringWithFormat:@"%i", (int)clientID];
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Client" action:@"Client nutrition request" label:clientIDString value:@(1)] build]];
}

- (void)trackClientSessionRequestWithClientID:(NSInteger)clientID
{
    NSString *clientIDString = [NSString stringWithFormat:@"%i", (int)clientID];
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Client" action:@"Client session request" label:clientIDString value:@(1)] build]];
}

- (void)trackClientTimeBetweenAppLaunch
{
    if ([[NSUserDefaults standardUserDefaults] valueForKeyPath:KEYTimeAppWasClosed]) {
        NSDate *date = [[NSUserDefaults standardUserDefaults] valueForKeyPath:KEYTimeAppWasClosed];
        NSTimeInterval interval= [self getIntervalInMinutesTillNowFrom:date];
        NSNumber *number = [NSNumber numberWithInteger:ceilf(interval)];
        
        [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application" action:@"Application time between launch" label:@"Client" value:number] build]];
    }
}

- (void)saveLaunchTime
{
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:KEYTimeAppWasOpened];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)saveCloseTime
{
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:KEYTimeAppWasClosed];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Trainer

- (void)trackTrainerQRCodeScanForSessionID:(NSInteger)sessionID scannedQRCodeTimes:(NSInteger)countOfScannedCode
{
    NSString *sessionIDIDString = [NSString stringWithFormat:@"%i", (int)sessionID];
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Trainer" action:@"Trainer scan QR code" label:sessionIDIDString value:@(countOfScannedCode)] build]];
}

- (void)trackTrainerUniqueDeviceLogin:(NSString *)trainerEmail;
{
    NSMutableArray *trainerIDs = [[[NSUserDefaults standardUserDefaults] valueForKey:TrainerKey] mutableCopy];
    if (![trainerIDs containsObject:trainerEmail]) {
        [trainerIDs addObject:trainerEmail];
        
        [[NSUserDefaults standardUserDefaults] setObject:trainerIDs forKey:TrainerKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Trainer" action:@"Trainer unique login" label:trainerEmail value:@(1)] build]];
    }
}

- (void)trackTrainerUniqueLogin
{
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Application Trainer" action:@"Trainer unique login" label:@"Trainer unique login" value:@(1)] build]];
}

#pragma mark - Private

- (NSTimeInterval)getIntervalInMinutesTillNowFrom:(NSDate *)createdDate
{
    NSTimeInterval distanceBetweenDates = [createdDate timeIntervalSinceDate:[NSDate date]];
    CGFloat minutesInAHour = 60;
    NSTimeInterval minutesBetweenDates = ABS(distanceBetweenDates) / minutesInAHour;
    return minutesBetweenDates;
}

@end