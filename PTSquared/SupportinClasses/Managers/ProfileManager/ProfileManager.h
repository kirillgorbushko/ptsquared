//
//  ProfileManager.h
//  PTSquared
//
//  Created by Kirill on 11/11/14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

typedef enum {
    UNDEFINED,
    PERSONAL_TRAINER,
    CLIENT
} ProfileType;

@interface ProfileManager : NSObject

@property (assign, nonatomic) ProfileType profileType;

@property (assign, nonatomic) NSInteger profileTrainerID;
@property (assign, nonatomic) NSInteger profileClientID;
@property (copy, nonatomic) NSString *profileTrainerName;
@property (copy, nonatomic) NSString *profileTrainerBussinessName;

+ (instancetype)sharedManager;

@end