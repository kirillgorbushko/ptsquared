//
//  ProfileManager.m
//  PTSquared
//
//  Created by Kirill on 11/11/14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

#import "ProfileManager.h"

@implementation ProfileManager

@synthesize profileTrainerID;
@synthesize profileClientID;
@synthesize profileTrainerName;
@synthesize profileTrainerBussinessName;

#pragma mark - Initialisation

+ (instancetype)sharedManager
{
    static ProfileManager *sharedManger = nil;
    static dispatch_once_t token;
    dispatch_once (&token, ^ {
        sharedManger = [[ProfileManager alloc] init];
    });
    return sharedManger;
}

@end
