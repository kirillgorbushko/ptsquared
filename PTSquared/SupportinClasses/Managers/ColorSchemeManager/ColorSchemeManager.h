//
//  ColorShcemeManager.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 06.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const NotificationKeyColorSchemeUpdated = @"NotificationKeyColorSchemeUpdated";

typedef NS_ENUM(NSUInteger, ColorSheme) {
    ColorShemeUndefined,
    ColorShemeBurgundy,
    ColorShemeGreen,
    ColorShemeLightBlue,
    ColorShemeNavy,
    ColorShemeOrange,
    ColorShemePurple,
    ColorShemeRed,
    ColorShemeYellow
};

static NSString *const DefaultColor = @"Blue";

@interface ColorSchemeManager : NSObject

@property (assign, nonatomic) ColorSheme selectedColorSheme;

+ (instancetype)sharedManager;

- (UIImage *)backgroundImage;
- (UIColor *)interfaceColor;

- (UIImage *)defaultBackgroundImage;

- (void)updateColorSchemeWithColor:(NSString *)colorName;

- (void)resetColorScheme;

- (void)updateColorsService;

@end