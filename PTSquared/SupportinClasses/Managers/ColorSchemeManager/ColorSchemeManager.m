//
//  ColorShcemeManager.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 06.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ColorSchemeManager.h"
#import "ProfileManager.h"

typedef void(^Completion)(BOOL needUpdate);

static NSString *const ColorBurgundy = @"Burgundy";
static NSString *const ColorBlue = @"Blue";
static NSString *const ColorGreen = @"Green";
static NSString *const ColorNavy = @"Navy";
static NSString *const ColorOrange = @"Orange";
static NSString *const ColorPurple = @"Purple";
static NSString *const ColorRed = @"Red";
static NSString *const ColorYellow = @"Yellow";

@interface ColorSchemeManager()

@property (strong, nonatomic) NSDictionary *backgroundFilesNames;
@property (strong, nonatomic) __block UIImage *colorSchemeImage;

@end

@implementation ColorSchemeManager

#pragma mark - Public

+ (instancetype)sharedManager
{
    static ColorSchemeManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[ColorSchemeManager alloc] init];
    });
    return sharedManager;
}

- (void)resetColorScheme
{
    [ColorSchemeManager sharedManager].selectedColorSheme = ColorShemeUndefined;
    self.colorSchemeImage = nil;
}

- (UIImage *)backgroundImage
{
    NSString *imageName;
    
    if (self.colorSchemeImage) {
        return self.colorSchemeImage;
    }
    
    switch (self.selectedColorSheme) {
        case ColorShemeBurgundy: {
            imageName = [self.backgroundFilesNames valueForKey:ColorBurgundy];
            break;
        }
        case ColorShemeLightBlue: {
            imageName = [self.backgroundFilesNames valueForKey:ColorBlue];
            break;
        }
        case ColorShemeGreen: {
            imageName = [self.backgroundFilesNames valueForKey:ColorGreen];
            break;
        }
        case ColorShemeNavy: {
            imageName = [self.backgroundFilesNames valueForKey:ColorNavy];
            break;
        }
        case ColorShemeOrange: {
            imageName = [self.backgroundFilesNames valueForKey:ColorOrange];
            break;
        }
        case ColorShemePurple: {
            imageName = [self.backgroundFilesNames valueForKey:ColorPurple];
            break;
        }
        case ColorShemeRed: {
            imageName = [self.backgroundFilesNames valueForKey:ColorRed];
            break;
        }
        case ColorShemeYellow: {
            imageName = [self.backgroundFilesNames valueForKey:ColorYellow];
            break;
        }
        case ColorShemeUndefined: {
            imageName = [self.backgroundFilesNames valueForKey:DefaultColor];
            break;
        }
    }
    UIImage *backgroundImage = [UIImage imageNamed:imageName];
    return backgroundImage;
}

- (UIColor *)interfaceColor
{
    UIColor *color;
    switch (self.selectedColorSheme) {
        case ColorShemeBurgundy: {
            color = [UIColor interfaceBurgundyColor];
            break;
        }
        case ColorShemeLightBlue: {
            color = [UIColor interfaceLightBlueColor];
            break;
        }
        case ColorShemeGreen: {
            color = [UIColor interfaceGreenColor];
            break;
        }
        case ColorShemeNavy: {
            color = [UIColor interfaceNavyColor];
            break;
        }
        case ColorShemeOrange: {
            color = [UIColor interfaceOrangeColor];
            break;
        }
        case ColorShemePurple: {
            color = [UIColor interfacePurpleColor];
            break;
        }
        case ColorShemeRed: {
            color = [UIColor interfaceRedColor];
            break;
        }
        case ColorShemeYellow: {
            color = [UIColor interfaceYellowColor];
            break;
        }
        case ColorShemeUndefined: {
            color = [UIColor interfaceLightBlueColor];
            break;
        }
    }
    return color;
}

- (UIImage *)defaultBackgroundImage
{
    NSString *imageName = [self.backgroundFilesNames valueForKey:DefaultColor];
    UIImage *defaultImage = [UIImage imageNamed:imageName];
    return defaultImage;
}

- (void)updateColorSchemeWithColor:(NSString *)colorName
{
    if ([colorName isKindOfClass:[NSNull class]]) {
        return;
    }
    if ([colorName isEqualToString:ColorBurgundy]) {
        self.selectedColorSheme = ColorShemeBurgundy;
    } else if ([colorName isEqualToString:ColorBlue]) {
        self.selectedColorSheme = ColorShemeLightBlue;
    } else if ([colorName isEqualToString:ColorGreen]) {
        self.selectedColorSheme = ColorShemeGreen;
    } else if ([colorName isEqualToString:ColorNavy]) {
        self.selectedColorSheme = ColorShemeNavy;
    } else if ([colorName isEqualToString:ColorOrange]) {
        self.selectedColorSheme = ColorShemeOrange;
    } else if ([colorName isEqualToString:ColorPurple]) {
        self.selectedColorSheme = ColorShemePurple;
    } else if ([colorName isEqualToString:ColorRed]) {
        self.selectedColorSheme = ColorShemeRed;
    } else if ([colorName isEqualToString:ColorYellow]) {
        self.selectedColorSheme = ColorShemeYellow;
    } else {
        self.selectedColorSheme = ColorShemeUndefined;
    }
}

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self fetchData];
    }
    return self;
}

#pragma mark - Private

- (void)fetchData
{
    self.backgroundFilesNames = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ColorSheme" ofType:@"plist"]];
}

- (void)updateColorsService
{
    if ([ProfileManager sharedManager].profileType == PERSONAL_TRAINER) {
        [self updateColorSchemeForTrainer: ^(BOOL needUpdate){
            if (needUpdate) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationKeyColorSchemeUpdated object:nil];
            }
        }];
    } else if ([ProfileManager sharedManager].profileType == CLIENT) {
        [self updateColorSchemeWithTrainerID:[ProfileManager sharedManager].profileTrainerID completion: ^(BOOL needUpdate){
            if (needUpdate) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationKeyColorSchemeUpdated object:nil];
            }
        }];
    }
}

- (void)updateColorSchemeWithTrainerID:(NSInteger)trainerID completion:(Completion)completitionBlock
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIClientGetColorWithTrainerID:trainerID operationResult:^(BOOL success, NSError *error, id response) {
        BOOL needSendUpdate = NO;
        if (success) {
            weakSelf.colorSchemeImage = [UIImage imageNamed:response];
            ColorSheme currentOne = weakSelf.selectedColorSheme;
            [weakSelf updateColorSchemeWithColor:response];
            
            if (currentOne != weakSelf.selectedColorSheme) {
                needSendUpdate = YES;
            }
        }
        if (completitionBlock) {
            completitionBlock(needSendUpdate);
        }
    }];
}

- (void)updateColorSchemeForTrainer:(Completion)completitionBlock
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPITrainerGetColor:^(BOOL success, NSError *error, id response) {
        BOOL needSendUpdate = NO;
        if (success) {
            weakSelf.colorSchemeImage = [UIImage imageNamed:[weakSelf.backgroundFilesNames valueForKey:response]];
            ColorSheme currentOne = weakSelf.selectedColorSheme;
            [weakSelf updateColorSchemeWithColor:response];
            
            if (currentOne != weakSelf.selectedColorSheme) {
                needSendUpdate = YES;
            }
        }
        if (completitionBlock) {
            completitionBlock(needSendUpdate);
        }
    }];
}

@end
