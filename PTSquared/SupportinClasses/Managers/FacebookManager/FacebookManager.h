//
//  FacebookManager.h
//  PTSquared
//
//  Created by sirko on 5/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

typedef void(^CompletionHandler)(BOOL success, id result, NSError *error);

@interface FacebookManager : NSObject

+ (void)facebookLoginWithCompletion:(CompletionHandler)completion;
+ (void)facebookLogoutWithResult:(CompletionHandler)completion;

+ (void)facebookShareLinkWithTitle:(NSString*)title description:(NSString*)description link:(NSURL*)link imageURL:(NSURL*)imageURL delegate:(id)delegate;
+ (void)facebookPOSTImage:(UIImage *)image withDescription:(NSString *)description completitionHandler:(CompletionHandler)completion;
+ (void)facebookPostMessageWithSessionType:(NSString *)sessionType trainer:(NSString *)trainerName trainerBussiness:(NSString *)trainerBussines isPublicSession:(BOOL)isPublic completitionHandler:(CompletionHandler)completion;

+ (void)facebookUserDetailsWithCompletitionHandler:(CompletionHandler)completion;

@end
