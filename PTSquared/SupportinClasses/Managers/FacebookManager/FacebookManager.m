//
//  FacebookManager.m
//  PTSquared
//
//  Created by sirko on 5/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "FacebookManager.h"

static NSString *const FacebookReadPermissions = @"public_profile";
static NSString *const FacebookEmailPermissions = @"email";
static NSString *const FacebookPublishAction = @"publish_actions";
static NSString *const FacebookDomain = @"Facebook";
static NSString *const IndividualSession = @"individual";

@implementation FacebookManager

#pragma mark - Public

+ (void)facebookLoginWithCompletion:(CompletionHandler)completion
{
    NSArray *requiredPermissions = @[FacebookReadPermissions, FacebookEmailPermissions];
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logInWithReadPermissions:requiredPermissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            if (completion) {
                completion(NO, nil, error);
            }
        } else if (result.isCancelled){
            if (completion) {
                NSError *cancelError = [AppHelper errorWithDescription:@"Login to Facebook was canceled" inDomain:FacebookDomain];                            completion(NO, nil, cancelError);
            }
        } else if ([result.grantedPermissions containsObject:@"email"]) {
            [FacebookManager performLoginWthCompletition:completion];
        } else if (![result.grantedPermissions containsObject:@"email"]) {
            NSError *emailError = [AppHelper errorWithDescription:@"Cant get user Email. Please configure you facebook accout" inDomain:FacebookDomain];            if (completion) {
                completion(NO, nil, emailError);
            }
        }
    }];
}

+ (void)facebookLogoutWithResult:(CompletionHandler)completion
{
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    if (completion) {
        completion(YES, @"success", nil);
    }
}

+ (void)facebookShareLinkWithTitle:(NSString*)title description:(NSString*)description link:(NSURL*)link imageURL:(NSURL*)imageURL delegate:(id)delegate
{
    FBSDKShareLinkContent *content = [FBSDKShareLinkContent new];
    content.contentTitle = title;
    content.contentDescription = description;
    content.contentURL = link;
    content.imageURL = imageURL;
    
    [FBSDKShareDialog showFromViewController:delegate withContent:content delegate:delegate];
}

+ (void)facebookPostMessageWithSessionType:(NSString *)sessionType trainer:(NSString *)trainerName trainerBussiness:(NSString *)trainerBussines isPublicSession:(BOOL)isPublic completitionHandler:(CompletionHandler)completion
{
    if (isPublic) {
        sessionType = [NSString stringWithFormat:@"public %@", sessionType];
    }
    
    void (^PerformPost)() = ^() {
        NSString *message = [NSString stringWithFormat:@"I will be attending %@ %@ session with %@ from %@", [sessionType isEqualToString:IndividualSession] ? @"an" : @"a", sessionType, trainerName, trainerBussines];
        NSDictionary *parameters = @{
                                     @"message" : message
                                     };
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/feed" parameters: parameters HTTPMethod:@"POST"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (completion) {
                if (!error) {
                    completion (YES, [result valueForKey:@"id"], nil);
                } else {
                    completion (NO, nil, error);
                }
            }
        }];
    };

    if ([[FBSDKAccessToken currentAccessToken] hasGranted:FacebookPublishAction]) {
        PerformPost();
    } else {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithPublishPermissions:@[FacebookPublishAction] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (!error){
                PerformPost();
            } else {
                if (completion) {
                    completion (NO, nil, error);
                }
            }
        }];
    }

}

+ (void)facebookPOSTImage:(UIImage *)image withDescription:(NSString *)description completitionHandler:(CompletionHandler)completion
{
    void (^PerformPost)() = ^() {
        NSDictionary *parameters = @{
                                     @"message" : description,
                                     @"picture" : UIImageJPEGRepresentation(image, 0.8f)
                                     };
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/photos" parameters: parameters HTTPMethod:@"POST"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (completion) {
                if (!error) {
                    completion (YES, [result valueForKey:@"id"], nil);
                } else {
                    completion (NO, nil, error);
                }
            }
        }];
    };

    if ([[FBSDKAccessToken currentAccessToken] hasGranted:FacebookPublishAction]) {
        PerformPost();
    } else {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithPublishPermissions:@[FacebookPublishAction] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (!error){
                PerformPost();
            } else {
                if (completion) {
                    completion (NO, nil, error);
                }
            }
        }];
    }
}

+ (void)facebookUserDetailsWithCompletitionHandler:(CompletionHandler)completion
{
    void (^PerformRequest)() = ^() {
        [FacebookManager performLoginWthCompletition:completion];
    };
    
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:FacebookReadPermissions] && [[FBSDKAccessToken currentAccessToken] hasGranted:FacebookEmailPermissions]) {
        PerformRequest();
    } else {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithReadPermissions:@[FacebookReadPermissions, FacebookEmailPermissions] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (!error){
                PerformRequest();
            } else {
                if (completion) {
                    completion (NO, nil, error);
                }
            }
        }];
    }
}

#pragma mark - Private

+ (void)performLoginWthCompletition:(CompletionHandler)completion
{
    FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:nil];
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (result) {
            completion (YES, result, nil);
        } else {
            completion(NO, nil, error);
        }
    }];
    [connection start];
}

@end
