//
//  Snapshot.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Snapshot : NSObject

+ (UIImage *)screenshot;
+ (UIImage *)screenshotOfView:(UIView *)viewForScrenshot;

@end
