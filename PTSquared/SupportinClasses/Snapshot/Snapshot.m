//
//  Snapshot.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Snapshot.h"

@implementation Snapshot

#pragma mark - Public

+ (UIImage *)screenshot
{
    UIView *topView = [AppHelper topView];
    UIGraphicsBeginImageContextWithOptions(topView.bounds.size, NO, [UIScreen mainScreen].scale);
    [topView drawViewHierarchyInRect:topView.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)screenshotOfView:(UIView *)viewForScrenshot
{
    UIView *topView = viewForScrenshot;
    UIGraphicsBeginImageContextWithOptions(viewForScrenshot.frame.size, NO, [UIScreen mainScreen].scale);
    [topView drawViewHierarchyInRect:viewForScrenshot.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


@end
