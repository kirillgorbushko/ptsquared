//
//  ClientLoginViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ClientLoginViewController.h"
#import "UINavigationController+Transparent.h"
#import "UINavigationController+Addition.h"

@interface ClientLoginViewController()

@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;

@end

@implementation ClientLoginViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareNavigationBar];
}

#pragma mark - Private

- (void)prepareNavigationBar
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController presentTransparentNavigationBar];
    [self.navigationController setBackButtonStyle:BackButtonStyleLight];
}

@end
