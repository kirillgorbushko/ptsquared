//
//  ValidationViewController.h
//  PTSquared
//
//  Created by sirko on 5/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface ValidationViewController : UIViewController <UITextFieldDelegate>

@end
