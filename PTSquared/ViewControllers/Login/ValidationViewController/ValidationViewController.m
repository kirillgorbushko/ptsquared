//
//  ValidationViewController.m
//  PTSquared
//
//  Created by sirko on 5/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ValidationViewController.h"
#import "LeftImageViewTextField.h"
#import "UINavigationController+Addition.h"
#import "GoogleAnaliticsManager.h"
#import "FacebookManager.h"
#import "ProfileManager.h"
#import "Animation.h"
#import "ContainerViewController.h"
#import "CredentialFetchService.h"

@interface ValidationViewController () 

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *validationCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;

@end

@implementation ValidationViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self unregisterForKeyboardNotification];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.scrollView.contentSize = [UIScreen mainScreen].bounds.size;
}

#pragma mark - IBActions

- (IBAction)submitButtonTouchUpInside:(id)sender
{    
    if (!self.validationCodeTextField.text.length) {
        [AppHelper alertViewWithMessage:AlertMessageEmptyFacebookValidationCode];
        return;
    }
    
    void (^UpdateToken)() = ^() {
        [[NetworkManager sharedManager] ptSquaredAPIUpdateToken:[NetworkManager sharedManager].deviceTokenForRemoteNotification withResult:^(BOOL success, NSError *error, id response) {
            if (success) {
                NSLog(@"%@", (NSString *)response);
            }
        }];
    };
    
    __weak typeof(self) weakSelf = self;
    void (^PerformLogIn)(NSString *email, NSString *facebookUserID) = ^(NSString *email, NSString *facebookUserID) {
        [[NetworkManager sharedManager] ptSquaredAPIClientSignInWithFacebookID:facebookUserID email:email facebookSignInResult:^(BOOL success, NSError *error, id response) {
            [AppHelper hideLoader];

            if (success) {
                UpdateToken();
                [weakSelf signInAtFirstTime:[[response valueForKey:@"first_login"] boolValue]];
                [[ColorSchemeManager sharedManager] updateColorSchemeWithColor:[response valueForKey:@"main_color"]];
                [[CredentialFetchService service] setClientLogginedWithFacebookId:facebookUserID];
            } else {
                [AppHelper alertViewWithMessage:AlertMessageUserNotConnectedWithFacebook];
            }
        }];
    };
    
    void (^FacebookSignUp)(id result) = ^(id result) {
        [[NetworkManager sharedManager] ptSquaredAPIClientSignUpWithFacebookID:result[@"id"] email:result[@"email"] validCode:self.validationCodeTextField.text facebookSignUpResult:^(BOOL success, NSError *error, id response) {
            if (success) {
                PerformLogIn(result[@"email"], result[@"id"]);
            } else {
                [AppHelper hideLoader];
                [AppHelper alertViewWithMessage:AlertMessageInvalidFacebookValidationCode];
            }
        }];
    };
    
    [AppHelper showLoader];
    [FacebookManager facebookLoginWithCompletion:^(BOOL success, id result, NSError *error) {
        if (success) {
            FacebookSignUp(result);
        } else {
            [AppHelper hideLoader];
            [AppHelper alertViewWithMessage:AlertMessageUserNotConnectedWithFacebook];
        }
    }];
}

- (void)signInAtFirstTime:(BOOL)firstTime
{
    [Animation setFadeTransitionAnimationForView:self.view subType:kCATransitionFromRight];

    if ([ProfileManager sharedManager].profileType == CLIENT) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ClientMode" bundle:nil];
        ContainerViewController *container = [storyboard instantiateViewControllerWithIdentifier:@"container"];
        container.isFirstLogin = firstTime;
        if (firstTime) {
            [[GoogleAnaliticsManager sharedManager] trackClientUniqueLogin];
        }
        [self.navigationController presentViewController:container animated:NO completion:nil];
    }
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}


#pragma mark - Keyboard

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboadWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)unregisterForKeyboardNotification
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    } @catch (NSException *exc) {
        NSLog(@"Cant remove observer - %@", exc);
    }
}

- (void)keyboadWillShow:(NSNotification*)notification
{
    CGRect screen = [UIScreen mainScreen].bounds;
    CGRect keyboard = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect textFieldFrame = [self.view convertRect:self.validationCodeTextField.frame fromView:[AppHelper topView]];
    
    if (CGRectGetMaxY(textFieldFrame) > (CGRectGetHeight(screen) - CGRectGetHeight(keyboard))) {
        CGPoint offset = CGPointMake(0.f, CGRectGetMaxY(textFieldFrame)-(CGRectGetHeight(screen) - CGRectGetHeight(keyboard)));
        [UIView animateWithDuration:0.25f animations:^{
            [self.scrollView setContentOffset:offset animated:YES];
            [self.scrollView layoutSubviews];
        }];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.validationCodeTextField resignFirstResponder];
    [UIView animateWithDuration:0.25f animations:^{
        [self.scrollView setContentOffset:CGPointMake(0.f, 0.f) animated:YES];
        [self.scrollView layoutSubviews];
    }];
    return NO;
}

#pragma mark - Private

- (void)navigateToClientHomeViewController
{
    [ProfileManager sharedManager].profileType = CLIENT;
    [Animation setFadeTransitionAnimationForView:self.view subType:kCATransitionFromRight];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ClientMode" bundle:nil];
    ContainerViewController *container = [storyboard instantiateViewControllerWithIdentifier:@"container"];
    [self.navigationController presentViewController:container animated:NO completion:nil];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)prepareUI
{
    [self.navigationController setBackButtonStyle:BackButtonStyleLight];
//    [self.submitButton setDefaultButtonStyle];
    [LeftImageViewTextField setupTextField:self.validationCodeTextField];
}

@end
