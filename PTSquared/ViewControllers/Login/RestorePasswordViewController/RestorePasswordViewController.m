//
//  RestorePasswordViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 12.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "RestorePasswordViewController.h"
#import "ColorSchemeManager.h"
#import "UINavigationController+Addition.h"
#import "LeftImageViewTextField.h"
#import "ProfileManager.h"
#import "UITextField+Validation.h"

@interface RestorePasswordViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *restoreButton;

@end

@implementation RestorePasswordViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self removeNotification];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.scrollView.contentSize = [UIScreen mainScreen].bounds.size;
}

#pragma mark - IBActions

- (IBAction)restorePasswordButtonPress:(id)sender
{
    if ([AppHelper isNSStringIsValidEmail:self.emailTextField.text applyFilter:NO]) {
        [self performRestoreRequest:self.emailTextField.text];
    }
}

#pragma mark - TextFieldValidation

- (IBAction)textFieldDidChangeTextEvent:(UITextField *)sender
{
    [sender textFieldDidChangedText];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField endEditing:YES];
    return YES;
}

#pragma mark - Keyboard

- (void)setupNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeNotification
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    } @catch (NSException *ex){
        NSLog(@"Cant remove observer for keyboard - %@",ex.description);
    }
}

- (void)keyboardOnScreen:(NSNotification *)notification
{
    if (IS_IPHONE_4_OR_LESS) {
        self.scrollView.contentOffset = CGPointMake(0, 100);
    }
}

- (void)keyboardHide:(NSNotification *)notification
{
    self.scrollView.contentOffset = CGPointZero;
}

#pragma mark - Private

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
}

- (void)performRestoreRequest:(NSString *)emailToRestore
{
    [self.view endEditing:YES];
    [AppHelper showLoader];
    
    void (^ForgotPasswordRequest)(BOOL success, id response) = ^(BOOL success, id response) {
        if (success) {
            [AppHelper alertViewWithMessage:response[@"success"]];
        } else {
            [AppHelper alertViewWithMessage:response];
        }
    };
    
    if ([ProfileManager sharedManager].profileType == PERSONAL_TRAINER) {
        [[NetworkManager sharedManager] ptSquaredAPITrainerForgotPasswordRequestForEmail:emailToRestore operationResult:^(BOOL success, NSError *error, id response) {
            [AppHelper hideLoader];
            ForgotPasswordRequest(success, response);
        }];
    } else {
        [[NetworkManager sharedManager] ptSquaredAPIClientForgotPasswordRequestForEmail:emailToRestore operationResult:^(BOOL success, NSError *error, id response) {
            [AppHelper hideLoader];
            ForgotPasswordRequest(success, response);
        }];
    }
}

- (void)prepareUI
{
    [self.navigationController setBackButtonStyle:BackButtonStyleLight];
    [LeftImageViewTextField setupTextField:self.emailTextField];
}

@end