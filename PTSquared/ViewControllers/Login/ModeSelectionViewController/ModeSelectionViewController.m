//
//  ModeSelectionViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 23.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ModeSelectionViewController.h"
#import "UINavigationController+Addition.h"
#import "SignInViewController.h"
#import "ProfileManager.h"

@interface ModeSelectionViewController ()

@property (weak, nonatomic) IBOutlet UIButton *ptLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *clientLoginButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation ModeSelectionViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareNavigationBar];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"trainerSignIn"]) {
        [ProfileManager sharedManager].profileType = PERSONAL_TRAINER;
    } else if ([segue.identifier isEqualToString:@"client"]){
        [ProfileManager sharedManager].profileType = CLIENT;
    }
}

#pragma mark - Private

- (void)prepareNavigationBar
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.navigationController setBackButtonStyle:BackButtonStyleLight];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

@end
