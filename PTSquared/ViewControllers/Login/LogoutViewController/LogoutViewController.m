//
//  LogoutViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "LogoutViewController.h"
#import "ModeSelectionViewController.h"
#import "ContainerViewController.h"
#import "ProfileManager.h"
#import "FacebookManager.h"
#import "CredentialFetchService.h"
#import "Animation.h"
#import "SwitchViewController.h"

@interface LogoutViewController ()

@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;

@property (assign, nonatomic) BOOL showAnimation;

@end

@implementation LogoutViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [AppHelper prepareInformationButtons:@[self.confirmButton, self.cancelButton]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
    self.view.layer.opacity = 1.f;

    self.backgroundView.image = self.backgroundImage;
}

#pragma mark - IBActions

- (IBAction)confirmButton:(id)sender
{
    __weak typeof(self) weakSelf = self;
    [AppHelper showLoader];
    
    if ([ProfileManager sharedManager].profileType == CLIENT) {
        [[NetworkManager sharedManager] ptSquaredAPIClientSignOutWithResult:^(BOOL success, NSError *error, id response) {
            [FacebookManager facebookLogoutWithResult:^(BOOL success, id result, NSError *error) {
                if (success) {
                    [weakSelf logOut];
                } else {
                    [AppHelper alertViewWithMessage:AlertMessageLogoutError];
                }
            }];
        }];
    } else {
        [[NetworkManager sharedManager] ptSquaredAPITrainerSignOutWithResult:^(BOOL success, NSError *error, id response) {
            [weakSelf logOut];
        }];

    }
}

- (IBAction)cancelButton:(id)sender
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"hide"];
    self.view.layer.opacity = 0.f;
}

#pragma mark - AnimationsDelegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"hide"]) {
        [self.view.layer removeAnimationForKey:@"hide"];
        [self dismissViewControllerAnimated:NO completion:^{
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
        }];
    }
}

#pragma mark - Private

- (void)logOut
{
    __weak typeof(self) weakSelf = self;
    [Animation setFadeTransitionAnimationForView:weakSelf.view subType:nil];

    UIViewController *rootViewController = self.view.window.rootViewController;
    
    if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController;
        UIViewController *container = [navigationController presentedViewController];
        [weakSelf dismissViewControllerAnimated:NO completion:^ {
            [AppHelper hideLoader];
            [AppHelper resetAllSettings];
            [container dismissViewControllerAnimated:NO completion:^{
                [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
            }];
            [Animation setFadeTransitionAnimationForView:container.view subType:nil];
        }];
    } else if ([rootViewController isKindOfClass:[SwitchViewController class]]) {
        SwitchViewController *switcher = (SwitchViewController *)rootViewController;
        UIViewController *container = [switcher presentedViewController];
        [weakSelf dismissViewControllerAnimated:NO completion:^ {
            [AppHelper hideLoader];
            [AppHelper resetAllSettings];
            [container dismissViewControllerAnimated:NO completion:^{
                [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
            }];
            [Animation setFadeTransitionAnimationForView:container.view subType:nil];
        }];
    }
    
}

@end