//
//  TrainerSignInViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 23.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "SignInViewController.h"
#import "UINavigationController+Transparent.h"
#import "UINavigationController+Addition.h"
#import "LeftImageViewTextField.h"
#import "TextFieldNavigator.h"
#import "ContainerViewController.h"
#import "TrainerHomeViewController.h"
#import "TrainerNavigationController.h"
#import "CredentialFetchService.h"
#import "FacebookManager.h"
#import "ProfileManager.h"
#import "UIColor+AppDefaultColors.h"
#import "UITextField+Validation.h"
#import "Animation.h"
#import "GoogleAnaliticsManager.h"

static NSInteger const EmailTextFieldPTOffset = 15;
static NSInteger const EmailTextFieldClientOffset = 40;

@interface SignInViewController()

@property (weak, nonatomic) IBOutlet LeftImageViewTextField *mailTextField;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIButton *logInWithFacebookButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerLoginTextFieldConstraint;

@end

@implementation SignInViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareInterface];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareNavigationBar];
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        [self setupNotification];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self removeNotification];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.scrollView.contentSize = [UIScreen mainScreen].bounds.size;
}

#pragma mark - IBActions

- (IBAction)signInButtonPress:(id)sender
{
    if (![NetworkManager sharedManager].networkStatus) {
        [AppHelper alertViewWithMessage:AlertMessageNoInternetConnection];
        return;
    }
    
#if DEBUG
    if ([ProfileManager sharedManager].profileType == PERSONAL_TRAINER) {
        self.mailTextField.text =
        @"trainer@mail.com"; //local
//        @"gagan.gill@signitysolutions.com";
//        @"test@yopmail.com";
//        @"hakan@buzinga.com.au";
//        @"kiril.gorbushko@thinkmobiles.com";
        self.passwordTextField.text =
        @"secret"; //local
//        @"123456";
//        @"buzinga";

    } else if ([ProfileManager sharedManager].profileType == CLIENT) {
        self.mailTextField.text =
        @"edward_ntn@inbox.ru"; //local
//        @"alex@mail.com"; //local
//        @"1@yopmail.com";
//        @"gagan.gill@signitysolutions.com";
//        @"bzingatester@gmail.com";
//        @"edward_ntn@inbox.ru";
//        @"iphone@yopmail.com";
        self.passwordTextField.text =
        @"secret"; //local
//        @"Development238";
//        @"123456";
//        @"buzinga";
//        @"111111";
//        @"123456";
    }
#endif
    
    if (!self.mailTextField.text.length || !self.passwordTextField.text.length) {
        [AppHelper alertViewWithMessage:AlertMessageInvalidInputParameters];
        return;
    } else if (![AppHelper isNSStringIsValidEmail:self.mailTextField.text applyFilter:NO]) {
        [AppHelper alertViewWithMessage:AlertMessageInvalidEmail];
        return;
    }
    
    [AppHelper showLoader];
    __weak typeof(self) weakSelf = self;
    
    if ([AppHelper isNSStringIsValidEmail:self.mailTextField.text applyFilter:NO]) {
        
        void (^UpdateToken)() = ^() {
            [[NetworkManager sharedManager] ptSquaredAPIUpdateToken:[NetworkManager sharedManager].deviceTokenForRemoteNotification withResult:^(BOOL success, NSError *error, id response) {
                if (success) {
                    NSLog(@"%@", (NSString *)response);
                }
            }];
        };

        if ([ProfileManager sharedManager].profileType == PERSONAL_TRAINER) {
            [[NetworkManager sharedManager] ptSquaredAPITrainerSignInWithUsername:self.mailTextField.text password:self.passwordTextField.text loginResult:^(BOOL success, NSError *error, id response) {
                [AppHelper hideLoader];
                if (success) {
                    
                    [[GoogleAnaliticsManager sharedManager] trackTrainerUniqueDeviceLogin:weakSelf.mailTextField.text];
                    
                    [[CredentialFetchService service] setPersonalTrainerLogginedWithPersonalTrainerName:self.mailTextField.text password:self.passwordTextField.text];
                    [[ColorSchemeManager sharedManager] updateColorSchemeWithColor:[response valueForKey:@"main_color"]];
                    [weakSelf signInAtFirstTime:[[response valueForKey:@"first_login"] boolValue]];
                } else {
                    [AppHelper alertViewWithMessage:response];
                }
            }];
        } else if ([ProfileManager sharedManager].profileType == CLIENT) {
            [[NetworkManager sharedManager] ptSquaredAPIClientSignInWithUsername:self.mailTextField.text password:self.passwordTextField.text loginResult:^(BOOL success, NSError *error, id response) {
                [AppHelper hideLoader];
                if (success) {
                    UpdateToken();
                    
                    [[GoogleAnaliticsManager sharedManager] trackClientUniqueDeviceLogin:self.mailTextField.text];
                    [[CredentialFetchService service] setClientLogginedWithClientName:self.mailTextField.text password:self.passwordTextField.text];
                    [[ColorSchemeManager sharedManager] updateColorSchemeWithColor:[response valueForKey:@"main_color"]];
                    [weakSelf signInAtFirstTime:[[response valueForKey:@"first_login"] boolValue]];
                } else {
                    [AppHelper alertViewWithMessage:response];
                }
            }];
        }
    }
}

- (IBAction)logInWithFacebookButtonPress:(id)sender
{
    void (^UpdateToken)() = ^() {
        [[NetworkManager sharedManager] ptSquaredAPIUpdateToken:[NetworkManager sharedManager].deviceTokenForRemoteNotification withResult:^(BOOL success, NSError *error, id response) {
            if (success) {
                NSLog(@"%@", (NSString *)response);
            }
        }];
    };
    
    __weak typeof(self) weakSelf = self;
    void (^PerformLogIn)(NSString *email, NSString *facebookUserID) = ^(NSString *email, NSString *facebookUserID) {
        [[NetworkManager sharedManager] ptSquaredAPIClientSignInWithFacebookID:facebookUserID email:email facebookSignInResult:^(BOOL success, NSError *error, id response) {
            if (success) {
                UpdateToken();
                [weakSelf signInAtFirstTime:[[response valueForKey:@"first_login"] boolValue]];
                [[ColorSchemeManager sharedManager] updateColorSchemeWithColor:[response valueForKey:@"main_color"]];
                [[CredentialFetchService service] setClientLogginedWithFacebookId:facebookUserID];
            } else {
                [AppHelper alertViewWithMessage:AlertMessageLoginErrorFacebook];
            }
            [AppHelper hideLoader];
        }];
    };
    void (^FacebookLogin)() = ^ () {
        [FacebookManager facebookUserDetailsWithCompletitionHandler:^(BOOL success, id result, NSError *error) {
            if (success) {
                PerformLogIn([result valueForKey:@"email"], [result valueForKey:@"id"]);
            } else {
                [AppHelper hideLoader];
                [AppHelper alertViewWithMessage:AlertMessageUserNotConnectedWithFacebook];
            }
        }];
    };
    [AppHelper showLoader];
    [FacebookManager facebookLoginWithCompletion:^(BOOL success, id result, NSError *error) {
        if (success) {
            FacebookLogin();
        } else {
            [AppHelper hideLoader];
            [AppHelper alertViewWithMessage:AlertMessageUserNotConnectedWithFacebook];
        }
    }];
}

#pragma mark - TextFieldValidation

- (IBAction)textFieldDidChangeTextEvent:(UITextField *)sender
{
    [sender textFieldDidChangedText];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [TextFieldNavigator findNextTextFieldFromCurrent:textField];
    if (textField.returnKeyType == UIReturnKeyDone) {
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.scrollView.contentOffset = CGPointZero;
        }];
        return YES;
    }
    return NO;
}

#pragma mark - Keyboard

- (void)setupNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeNotification
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    } @catch (NSException *ex){
        NSLog(@"Cant remove observer for keyboard - %@",ex.description);
    }
}

- (void)keyboardOnScreen:(NSNotification *)notification
{
    CGFloat offset = 200;
    if (IS_IPHONE_5) {
        offset = 100;
    }
    self.scrollView.contentOffset = CGPointMake(0, offset);
}

- (void)keyboardHide:(NSNotification *)notification
{
    self.scrollView.contentOffset = CGPointZero;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark - Private

- (void)signInAtFirstTime:(BOOL)firstTime
{
    [Animation setFadeTransitionAnimationForView:self.view subType:kCATransitionFromRight];
    
    if ([ProfileManager sharedManager].profileType == PERSONAL_TRAINER) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TrainerMode" bundle:nil];
        TrainerHomeViewController *trainerHomeViewController = [storyboard instantiateInitialViewController];
        if (firstTime) {
            [[GoogleAnaliticsManager sharedManager] trackTrainerUniqueLogin];
        }
        [self.navigationController presentViewController:trainerHomeViewController animated:NO completion:nil];
    } else if ([ProfileManager sharedManager].profileType == CLIENT) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ClientMode" bundle:nil];
        ContainerViewController *container = [storyboard instantiateViewControllerWithIdentifier:@"container"];
        container.isFirstLogin = firstTime;
        if (firstTime) {
            [[GoogleAnaliticsManager sharedManager] trackClientUniqueLogin];
        }
        [self.navigationController presentViewController:container animated:NO completion:nil];
    }
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)prepareNavigationBar
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController presentTransparentNavigationBar];
    [self.navigationController setBackButtonStyle:BackButtonStyleLight];
}

- (void)prepareInterface
{
    [LeftImageViewTextField setupTextField:self.mailTextField];
    [LeftImageViewTextField setupTextField:self.passwordTextField];
    
    if ([ProfileManager sharedManager].profileType == CLIENT) {
        self.logInWithFacebookButton.hidden = NO;
        self.centerLoginTextFieldConstraint.constant = EmailTextFieldClientOffset;
    } else if ([ProfileManager sharedManager].profileType == PERSONAL_TRAINER) {
        self.centerLoginTextFieldConstraint.constant = EmailTextFieldPTOffset;
    }
}

@end
