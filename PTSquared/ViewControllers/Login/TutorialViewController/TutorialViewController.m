//
//  PTCMTutorialViewController.m
//  PTSquared
//
//  Created by Stas Volskyi on 10.11.14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

#import "TutorialViewController.h"
#import "ContainerViewController.h"
#import "Animation.h"
#import "TutorialCell.h"
#import "FrontTutorialCollectionViewCell.h"
#import "ProfileManager.h"
#import "UIColor+AppDefaultColors.h"

static NSString *const PTTCellIdentifier = @"tutorialCell";
static NSString *const FrontCell = @"frontCell";
static NSInteger const PTTOffcet = 50;

@interface TutorialViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *backCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *frontCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *tutorialPageControl;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *getStartedButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topPositionContraint;

@property (strong, nonatomic) NSMutableArray *tutorialImages;
@property (strong, nonatomic) NSArray *textForTitle;

@end

@implementation TutorialViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self configureDataSource];
    [self configurePageControl];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.layer.opacity = 0.f;
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
    self.view.layer.opacity = 1.f;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    if (IS_IPHONE_4_OR_LESS) {
        self.topPositionContraint.constant = 100;
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        self.frontCollectionView.layoutMargins = UIEdgeInsetsZero;
        self.backCollectionView.layoutMargins = UIEdgeInsetsZero;
    } else {
        self.backCollectionView.contentInset = UIEdgeInsetsZero;
        self.frontCollectionView.contentInset = UIEdgeInsetsZero;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
}

#pragma mark - IBAction

- (IBAction)getStartedButtonPressed:(id)sender
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"fadeAnimation"];
    self.view.layer.opacity = 0.f;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.tutorialImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.backCollectionView) {
        TutorialCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PTTCellIdentifier forIndexPath:indexPath];
        if (!cell) {
            cell = [[TutorialCell alloc] init];
        }
        cell.titleLabel.text = self.textForTitle[indexPath.row];
        return cell;
        
    } else {
        FrontTutorialCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:FrontCell forIndexPath:indexPath];
        if (!cell) {
            cell = [[FrontTutorialCollectionViewCell alloc] init];
        }
        cell.imageView.image = self.tutorialImages[indexPath.row];
        return cell;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.backCollectionView) {

    return self.backCollectionView.bounds.size;
    } else {
return self.frontCollectionView.bounds.size;

    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    for (NSIndexPath *indexPath in self.backCollectionView.indexPathsForVisibleItems) {
        TutorialCell *cell = (TutorialCell *)[self.backCollectionView cellForItemAtIndexPath:indexPath];
        if (ABS(cell.frame.origin.x - self.backCollectionView.contentOffset.x) <= PTTOffcet) {
            self.tutorialPageControl.currentPage = indexPath.row;
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSArray *cells = [self.backCollectionView visibleCells];
    CGFloat offsetPercent = scrollView.contentOffset.x/(scrollView.contentSize.width-CGRectGetWidth(scrollView.bounds));
    CGPoint offset = CGPointZero;
    
    offset.x = (self.frontCollectionView.contentSize.width - CGRectGetWidth(self.frontCollectionView.bounds)) * offsetPercent;
    self.frontCollectionView.contentOffset = offset;
    [cells enumerateObjectsUsingBlock:^(TutorialCell *cell, NSUInteger idx, BOOL *stop) {
        CGPoint cellPoint = [self.view convertPoint:cell.center fromView:cell];
        cellPoint.x = cellPoint.x - CGRectGetMinX(cell.frame);
        CGFloat percent = cellPoint.x / CGRectGetWidth(self.backCollectionView.bounds);
        [cell updateTextPosition:percent];
    }];
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"fadeAnimation"]) {
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Private

- (void)addMotionEffects
{
    UIInterpolatingMotionEffect *xInterpolation = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    xInterpolation.minimumRelativeValue = @(-25);
    xInterpolation.maximumRelativeValue = @(25);
    UIInterpolatingMotionEffect *yInterpolation = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    yInterpolation.minimumRelativeValue = @(-35);
    yInterpolation.maximumRelativeValue = @(35);
    
    UIMotionEffectGroup *group = [[UIMotionEffectGroup alloc] init];
    group.motionEffects = @[xInterpolation, yInterpolation];
    
    [self.backCollectionView addMotionEffect:group];
}

- (void)configureDataSource
{
    if (!self.tutorialImages) {
        self.tutorialImages = [[NSMutableArray alloc] init];
    }
    [self addImagesToDataSource];
    
    self.textForTitle = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorialData" ofType:@"plist"]];
}

- (void)configurePageControl
{
    self.tutorialPageControl.numberOfPages = self.tutorialImages.count;
}

- (void)addImagesToDataSource
{
    for (int i = 0; i < 4; i++) {
        NSString *fileName = [NSString stringWithFormat:@"Tutorial_Screen_%li", (long)(i + 1)];
        [self.tutorialImages addObject:[UIImage imageNamed:fileName]];
    }
}

@end