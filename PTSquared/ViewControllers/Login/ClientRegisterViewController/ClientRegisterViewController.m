//
//  ClientRegisterViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ClientRegisterViewController.h"
#import "LeftImageViewTextField.h"
#import "TextFieldNavigator.h"
#import "FacebookManager.h"
#import "SignInViewController.h"
#import "UITextField+Validation.h"
#import "GoogleAnaliticsManager.h"

@interface ClientRegisterViewController()

@property (weak, nonatomic) IBOutlet LeftImageViewTextField *emailTextField;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *validationCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginTextFiledTopConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;
@property (weak, nonatomic) IBOutlet UIView *separatorOne;
@property (weak, nonatomic) IBOutlet UIView *separatorTwo;
@property (weak, nonatomic) IBOutlet UIView *separatorThree;

@end

@implementation ClientRegisterViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareInterface];
    [self updateUIIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotification];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.scrollView.contentSize = [UIScreen mainScreen].bounds.size;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self removeNotification];
}

#pragma mark - IBActions

- (IBAction)signInButtonPress:(id)sender
{
    NSString *errorMessage = [self errorMessageForCurrentState];
    if (errorMessage) {
        [AppHelper alertViewWithMessage:errorMessage];
    } else {
        [self.view endEditing:YES];
        [AppHelper showLoader];
        [[NetworkManager sharedManager] ptSquaredAPIClientSignUpWithEmail:self.emailTextField.text password:self.passwordTextField.text confirmPassword:self.confirmPasswordTextField.text validCode:self.validationCodeTextField.text signUpResult:^(BOOL success, NSError *error, id response) {
            [AppHelper hideLoader];
            if (success) {
                [[GoogleAnaliticsManager sharedManager] trackClientRegistration];
                [AppHelper alertViewWithMessage:AlertMessageSuccessfullRegistration delegate:self];
            } else {
                [AppHelper alertViewWithMessage:response];
            }
        }];
    }
}

#pragma mark - TextFieldValidation

- (IBAction)textFieldDidChangeTextEvent:(UITextField *)sender
{
    [sender textFieldDidChangedText];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self pushSignInViewController];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [TextFieldNavigator findNextTextFieldFromCurrent:textField];
    if (textField.returnKeyType == UIReturnKeyDone) {
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.scrollView.contentOffset = CGPointZero;
        }];
        return YES;
    }
    return NO;
}

#pragma mark - Keyboard

- (void)setupNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeNotification
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    } @catch (NSException *ex){
        NSLog(@"Cant remove observer for keyboard - %@",ex.description);
    }
}

- (void)keyboardOnScreen:(NSNotification *)notification
{
    self.scrollView.contentOffset = CGPointMake(0, 100);
}

- (void)keyboardHide:(NSNotification *)notification
{
    self.scrollView.contentOffset = CGPointZero;
}

#pragma mark - Private

- (void)pushSignInViewController
{
    SignInViewController *signInViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"trainerSignIn"];
    [self.navigationController pushViewController:signInViewController animated:YES];
}

- (void)prepareInterface
{
    [LeftImageViewTextField setupTextField:self.emailTextField];
    [LeftImageViewTextField setupTextField:self.passwordTextField];
    [LeftImageViewTextField setupTextField:self.confirmPasswordTextField];
    [LeftImageViewTextField setupTextField:self.validationCodeTextField];
}

- (void)updateUIIfNeeded
{
    if (IS_IPHONE_4_OR_LESS) {
        CGFloat delta = self.logoTopConstraint.constant;
        self.logoTopConstraint.constant /= 4;
        delta -= self.logoTopConstraint.constant;
        self.loginTextFiledTopConstraint.constant -= delta;
    }
}

- (NSString *)errorMessageForCurrentState
{
    NSString *errorMessage;
    
    if (![AppHelper isNSStringIsValidEmail:self.emailTextField.text applyFilter:NO]) {
        errorMessage = AlertMessageInvalidEmail;
    } else if (!self.passwordTextField.text.length) {
        errorMessage = AlertMessageEmptyPassword;
    } else if (self.passwordTextField.text.length < 6) {
        errorMessage = AlertMessagePasswordTooSmall;
    } else if (!self.confirmPasswordTextField.text.length) {
        errorMessage = AlertMessageInvalidPassword;
    } else if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        errorMessage = AlertMessageInvalidPassword;
    } else if (!self.validationCodeTextField.text.length) {
        errorMessage = AlertMessageInvalidValidationCode;
    }
    
    return errorMessage;
}

@end
