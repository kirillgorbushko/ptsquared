//
//  BaseDynamicColorViewController.m
//  PT2
//
//  Created by Kirill Gorbushko on 03.08.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#define mustOverride() @throw [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"%s must be overridden in a subclass/category", __PRETTY_FUNCTION__] userInfo:nil]
#define setMustOverride() NSLog(@"%@ - method not implemented", NSStringFromClass([self class])); mustOverride()

#import "BaseDynamicColorViewController.h"

@implementation BaseDynamicColorViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareInterfaceColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prepareInterfaceColor) name:NotificationKeyColorSchemeUpdated object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public required

- (void)prepareInterfaceColor
{
    //dummy
    setMustOverride();
}

@end