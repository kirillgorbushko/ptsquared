//
//  BaseDynamicColorViewController.h
//  PT2
//
//  Created by Kirill Gorbushko on 03.08.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

@interface BaseDynamicColorViewController : UIViewController

- (void)prepareInterfaceColor;

@end
