//
//  SessionDetailsViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class Session;

#import "FacebookManager.h"
#import "GoogleMapsProvider.h"

@interface SessionDetailsViewController : BaseDynamicColorViewController <GMSMapViewDelegate, FBSDKSharingDelegate, UIAlertViewDelegate>

//@property (assign, nonatomic) NSInteger sessionID;
@property (strong, nonatomic) NSString *sessionType;
//@property (assign, nonatomic) BOOL isPublic;
@property (strong, nonatomic) NSDictionary *dataSouce;

@property (strong, nonatomic) Session *activeSession;

@end
