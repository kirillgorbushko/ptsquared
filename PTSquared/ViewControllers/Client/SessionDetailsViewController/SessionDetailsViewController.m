//
//  SessionDetailsViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "SessionDetailsViewController.h"
#import "ClientNavigationController.h"
#import "UINavigationController+Addition.h"
#import "AttendeesViewController.h"
#import "Snapshot.h"
#import "Session.h"
#import "UIImage+ImageEffects.h"
#import "ClientNotificationViewController.h"
#import "Session.h"
#import "CalendarDataSource.h"
#import "ProfileManager.h"
#import "GoogleAnaliticsManager.h"
#import "Animation.h"
#import "CalendarDataSource.h"

static CGFloat const ExtendedBottomSpaceForAddressLabel = 15;

static NSString *const SessionTypeIndividual = @"individual";
static NSString *const SessionTypeGroup = @"group";

static NSString *const SessionTitleGroup = @"Group Session";
static NSString *const SessionTitlePublic = @"Public Session";
static NSString *const SessionTitleIndividual = @"Individual Session";

@interface SessionDetailsViewController () 

@property (weak, nonatomic) IBOutlet UILabel *sessionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionStartTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *attendeesButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceAddressLabelConstraint;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;

@property (strong, nonatomic) Session *session;

@end

@implementation SessionDetailsViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self receiveSessionDetails];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setBackButtonStyle:BackButtonStyleDark];
    [self prepareUIForCurrentSessionType];
}

#pragma mark - GoogleMap

- (void)prepareGoogleMapView
{
    CLLocationDegrees latitude = self.session.sessionLatitude;
    CLLocationDegrees longtitude = self.session.sessionLongtitude;
    [GoogleMapsProvider setupMapOnView:self.mapView setMarkerOnLatitude:latitude andLongtitude:longtitude];
}

#pragma mark - IBActions

- (IBAction)shareWithFacebookButtonPressed:(id)sender
{
    [AppHelper showLoader];

    NSString *trainerName = [ProfileManager sharedManager].profileTrainerName;
    NSString *trainerBussinesName = [ProfileManager sharedManager].profileTrainerBussinessName;
    [FacebookManager facebookPostMessageWithSessionType:self.sessionType trainer:trainerName trainerBussiness:trainerBussinesName isPublicSession:self.session.sessionIsPublic completitionHandler:^(BOOL success, id result, NSError *error) {
        [AppHelper hideLoader];
        if (success) {
            [[GoogleAnaliticsManager sharedManager] trackClientFacebookSharing];
            [AppHelper alertViewWithMessage:AlertMessageFacebookSharingSuccessful];
        } else {
            [AppHelper alertViewWithMessage:AlertMessageFacebookSharingFinishedWithError];
        }
    }];
}

- (IBAction)showAttendeesButtonPressed:(id)sender
{
    AttendeesViewController *attendeesViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"attendeesViewController"];
    attendeesViewController.sessionID = self.activeSession.sessionId;
    attendeesViewController.backgroundImage = [Snapshot screenshot];
    attendeesViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
    attendeesViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
    self.parentViewController.parentViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.navigationController presentViewController:attendeesViewController animated:NO completion:nil];
}

- (IBAction)joinToSessionButtonPressed:(id)sender
{
    [AppHelper alertViewWithMessage:NotificationMessageJoinToSession delegate:self otherButtonTitles:@"Cancel"];
}

- (void)performJoinAction
{
    UIImage *bluredImage = [[Snapshot screenshot] applyDarkEffect];
    [AppHelper showLoader];
    __weak typeof(self) weakSelf = self;
    
    NSString *stingDate = [CalendarDataSource stringZeroInRequestFormat:self.activeSession.sessionDate];
    
    [[NetworkManager sharedManager] ptSquaredAPIClientJoinToSessionWithID:self.activeSession.sessionId isPublic:self.activeSession.sessionIsPublic parentID:self.activeSession.sessionParentID sessionTime:stingDate reoccurence:self.activeSession.sessionReoccurence operationResult:^(BOOL success, NSError *error, id response) {
        [AppHelper hideLoader];
        NSString *message = response;
        if (success) {
            [weakSelf updateUIForPublicJoinedSession];
        } else {
            message = response;
        }
        [ClientNotificationViewController presentInformationViewControllerFromViewController:weakSelf withText:message backgraundImage:bluredImage];
    }];
}

#pragma mark - Animation Delegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.joinButton.layer animationForKey:@"hideJoinButton"]) {
        [self.joinButton.layer removeAllAnimations];
        self.joinButton.hidden = YES;
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (!buttonIndex) {
        [self performJoinAction];
    }
}

#pragma mark - FBSDKSharingDelegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    [AppHelper alertViewWithMessage:AlertMessageFacebookSharingSuccessful];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSString *message = [NSString stringWithFormat:@"%@-%@", AlertMessageFacebookSharingFinishedWithError, error.localizedDescription];
    [AppHelper alertViewWithMessage:message];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    [AppHelper alertViewWithMessage:AlertMessageFacebookSharingCanceled];
}

#pragma mark - Private

- (void)prepareUIForCurrentSessionType
{
    NSString *title;
    
    if ([self.sessionType isEqualToString:SessionTypeIndividual]) {
        title = SessionTitleIndividual;
        self.shareButton.hidden = NO;
        self.attendeesButton.hidden = YES;
        self.priceLabel.hidden = YES;
        self.bottomSpaceAddressLabelConstraint.constant = ExtendedBottomSpaceForAddressLabel;
    } else if (self.activeSession.sessionIsPublic) {
        title = SessionTitlePublic;
        self.shareButton.hidden = YES;
        self.attendeesButton.hidden = YES;
        self.joinButton.hidden = NO;
    } else {
        title = SessionTitleGroup;
        self.shareButton.hidden = NO;
        self.attendeesButton.hidden = NO;
    }

    [((ClientNavigationController *)self.navigationController) setTitleLabelToNavigationBar:title];
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
}

- (void)receiveSessionDetails
{
    __weak __typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIClientGetSessionDetailsForSessionID:self.activeSession.sessionId forDate:self.activeSession.sessionDate operationresult:^(BOOL success, NSError *error, id response) {
        if (success) {
            weakSelf.session = response;
            [weakSelf updateUI];
        }
    }];
}

- (void)updateUI
{
    __weak __typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.sessionNameLabel.text = weakSelf.session.sessionName;
        weakSelf.sessionStartTimeLabel.text = [CalendarDataSource timeFromDate:weakSelf.session.sessionDate];
        weakSelf.addressLabel.text = weakSelf.session.sessionAddress;
        weakSelf.priceLabel.text = [NSString stringWithFormat:@"$%ld", (long)weakSelf.session.sessionPrice];
        weakSelf.activeSession.sessionIsPublic = weakSelf.session.sessionIsPublic;
        
        if (weakSelf.session.sessionIsClientJoined) {
            [weakSelf updateSessionType];
        }
        
        [weakSelf prepareGoogleMapView];
    });
}

- (void)updateUIForPublicJoinedSession
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.shareButton.hidden = NO;
        weakSelf.attendeesButton.hidden = NO;
        [weakSelf.shareButton.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
        [weakSelf.attendeesButton.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
        
        [weakSelf.joinButton.layer addAnimation:[Animation fadeAnimFromValue:1. to:0 delegate:weakSelf] forKey:@"hideJoinButton"];
    });
}

- (void)updateSessionType
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.shareButton.hidden = NO;
        weakSelf.joinButton.hidden = YES;
        
        if ([[weakSelf.sessionType lowercaseString] isEqualToString:SessionTypeIndividual]) {
            weakSelf.attendeesButton.hidden = YES;
        } else {
            weakSelf.attendeesButton.hidden = NO;
        }
    });
}

@end
