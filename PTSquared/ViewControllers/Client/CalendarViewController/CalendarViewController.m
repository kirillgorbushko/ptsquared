//
//  CalendarViewController.m
//  testCalendar
//
//  Created by Kirill Gorbushko on 27.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "CalendarViewController.h"
#import "CalendarCell.h"
#import "CalendarDataSource.h"
#import "CalendarDay.h"
#import "Session.h"

static NSInteger const RowNumber = 6;
static NSInteger const ColumnNumber = 7;

@interface CalendarViewController() 

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (assign, nonatomic) NSInteger currentMonth;
@property (strong, nonatomic) NSIndexPath *todayIndexPath;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@end

@implementation CalendarViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateTodayCellIfNeeded];
    [self prepareDateFormatter];
}

#pragma mark - Custom accessors

 - (void)setCalendarData:(NSArray *)calendarData
{
    _calendarData = calendarData;
    [self.collectionView reloadData];
    [self getDateName];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectItemAtIndexPath:inCell:)]) {
        [self.delegate didSelectItemAtIndexPath:indexPath inCell:((CalendarCell *)[collectionView cellForItemAtIndexPath:indexPath])];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectItemFromNextMonthWithDate:)] && [self.delegate respondsToSelector:@selector(didSelectItemFromPreviosMonthWithDate:)]) {
        NSDate *selectedDate = ((CalendarDay *)self.calendarData[indexPath.row]).calendarDayDate;
        NSInteger selectedMonthNumber = [CalendarDataSource monthNumberFromDate:selectedDate];
        if (selectedMonthNumber > self.currentMonth) {
            [self.delegate didSelectItemFromNextMonthWithDate:selectedDate];
        } else if (selectedMonthNumber < self.currentMonth) {
            [self.delegate didSelectItemFromPreviosMonthWithDate:selectedDate];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDate *dateForCell = ((CalendarDay *)self.calendarData[indexPath.row]).calendarDayDate;
    CalendarCell *cell = (CalendarCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    if ([CalendarDataSource isDay:dateForCell isSameDayAs:[NSDate date]]) {
        cell.daySelectionView.backgroundColor = [UIColor blackColor];
        cell.daySelectionView.hidden = NO;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.calendarData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CalendarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"calendarCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[CalendarCell alloc] init];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize collectionViewSize = self.collectionView.bounds.size;
    CGFloat cellHeight = (collectionViewSize.height - RowNumber + 1) / RowNumber;
    CGFloat cellWidth = (collectionViewSize.width - ColumnNumber + 1) / ColumnNumber;
    CGSize cellsize = CGSizeMake(cellWidth, cellHeight);
    
    return cellsize;
}

#pragma mark - Public

- (CalendarCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (CalendarCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
}

- (CalendarCell *)cellForDayNumber:(NSInteger)day
{
    __block CalendarCell *cell;
    
    [self.calendarData enumerateObjectsUsingBlock:^(CalendarDay *calendarDay, NSUInteger idx, BOOL *stop) {
        
        if ([CalendarDataSource monthNumberFromDate:calendarDay.calendarDayDate] == self.currentMonth) {
            NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:calendarDay.calendarDayDate];
            if (components.day == day) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
                cell = [self cellForRowAtIndexPath:indexPath];
                if (cell && [self.delegate respondsToSelector:@selector(shouldUpdateSelectedIndexPath:)]) {
                    [self.delegate performSelector:@selector(shouldUpdateSelectedIndexPath:) withObject:indexPath];
                }
            }
        }
        
    }];
    
    return cell;
}

#pragma mark - Private

- (void)configureCell:(CalendarCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CalendarDay *selectedDay = (CalendarDay *)self.calendarData[indexPath.row];
    CalendarDay *prevDayDayUTC;
    if (indexPath.row) {
        prevDayDayUTC = (CalendarDay *)self.calendarData[indexPath.row - 1];
    }
    CalendarDay *nextDayDayUTC;
    if (indexPath.row != [self.collectionView numberOfItemsInSection:0] - 1) {
        nextDayDayUTC = (CalendarDay *)self.calendarData[indexPath.row + 1];
    }
    NSDate *dateForCell = selectedDay.calendarDayDate;
    cell.dayNumberLabel.text = [NSString stringWithFormat:@"%i", (int)[CalendarDataSource numberofDay:dateForCell]];
    
    if ([CalendarDataSource monthNumberFromDate:dateForCell] != self.currentMonth) {
        cell.dayNumberLabel.textColor = [UIColor grayColor];
    }
    if ([CalendarDataSource isDay:dateForCell isSameDayAs:[NSDate date]]) {
        cell.daySelectionView.backgroundColor = [UIColor blackColor];
        cell.daySelectionView.hidden = NO;
        self.todayIndexPath = indexPath;
        if (self.delegate && [self.delegate respondsToSelector:@selector(setIndexPathForToday:)]) {
            [self.delegate setIndexPathForToday:indexPath];
        }
    }
    if (self.selectedDate && [CalendarDataSource isDay:self.selectedDate isSameDayAs:dateForCell] && [CalendarDataSource monthNumberFromDate:dateForCell] == self.currentMonth) {
        [cell setCellSelected:YES];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(shouldUpdateSelectedIndexPath:)]) {
            [self.delegate shouldUpdateSelectedIndexPath:indexPath];
        }
    }
    
    NSMutableArray *sessions = [[NSMutableArray alloc] init];
    [sessions addObjectsFromArray:selectedDay.calendarSessions];
    [sessions addObjectsFromArray:prevDayDayUTC.calendarSessions];
    [sessions addObjectsFromArray:nextDayDayUTC.calendarSessions];
    
    __block NSArray *currentDaySessions = [self verificateSessionWithArray:sessions withDate:dateForCell];

//        //additional request due to timeZone difference - temp solution 

//    if (indexPath.row == [self.collectionView numberOfItemsInSection:0] - 1) {
//        NSDateComponents *components = [[NSDateComponents alloc] init];
//        [components setDay:-1];
//        NSDate *prevDay = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:dateForCell options:0];
//        __weak typeof(self) weakSelf = self;
//        [[NetworkManager sharedManager] ptSquaredAPIClientGetSessionInPeriodFrom:prevDay daysCount:3 operationResult:^(BOOL success, NSError *error, id response) {
//            if (success) {
//                for (Session *session in response) {
//                    [sessions addObject:session];
//                }
//                currentDaySessions = [weakSelf verificateSessionWithArray:sessions withDate:dateForCell];
//                [cell setCellSelectedWithSessions:currentDaySessions];
//            }
//        }];
//        [cell setCellSelectedWithSessions:currentDaySessions];
//
//    } else {
        [cell setCellSelectedWithSessions:currentDaySessions];
//    }
}

- (void)prepareDateFormatter
{
    self. dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [self.dateFormatter setTimeZone:timeZone];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
}

- (NSArray *)verificateSessionWithArray:(NSArray *)input withDate:(NSDate *)requestedDate
{
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *dateInLocalTimezone = [requestedDate dateByAddingTimeInterval:timeZoneSeconds];
    
    NSMutableArray *sessions = [[NSMutableArray alloc] init];
    for (Session *session in input) {
        NSString *sessionDate = [self.dateFormatter stringFromDate:[session.sessionDate dateByAddingTimeInterval:timeZoneSeconds]];
        sessionDate = [[sessionDate componentsSeparatedByString:@" "] firstObject];
        
        NSString *requestDate = [self.dateFormatter stringFromDate:dateInLocalTimezone];
        requestDate = [[requestDate componentsSeparatedByString:@" "] firstObject];
        
        if ([sessionDate isEqualToString:requestDate]) {
            [sessions addObject:session];
        }
    }
    return sessions;
}


- (void)getDateName
{
    NSInteger middleOFSelectedMonth = self.calendarData.count / 2;
    NSDate *middleDate = ((CalendarDay *)self.calendarData[middleOFSelectedMonth]).calendarDayDate;
    self.currentMonth = [CalendarDataSource monthNumberFromDate:middleDate];
}

- (void)updateTodayCellIfNeeded
{
    if (self.todayIndexPath) {
        CalendarCell *cell = (CalendarCell *)[self.collectionView cellForItemAtIndexPath:self.todayIndexPath];
        cell.daySelectionView.backgroundColor = [UIColor blackColor];
        cell.daySelectionView.hidden = NO;
    }
}

@end
