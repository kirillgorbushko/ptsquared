//
//  CalendarViewController.h
//  testCalendar
//
//  Created by Kirill Gorbushko on 27.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class CalendarCell;

@protocol CalendarViewControllerDelegate <NSObject>

@optional
- (void)didSelectItemAtIndexPath:(NSIndexPath *)indexPath inCell:(CalendarCell *)cell;
- (void)shouldUpdateSelectedIndexPath:(NSIndexPath *)indexPath;
- (void)setIndexPathForToday:(NSIndexPath *)indexPath;

/** should use in pair */
- (void)didSelectItemFromPreviosMonthWithDate:(NSDate *)selectedDate;
- (void)didSelectItemFromNextMonthWithDate:(NSDate *)selectedDate;

@end

@interface CalendarViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) id <CalendarViewControllerDelegate> delegate;

@property (strong, nonatomic) NSDate *selectedDate;
@property (assign, nonatomic) NSInteger dataSourceOffset;
@property (strong, nonatomic) __block NSArray *calendarData;

- (CalendarCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (CalendarCell *)cellForDayNumber:(NSInteger)day;

@end
