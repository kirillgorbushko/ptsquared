//
//  NutritionViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

typedef NS_ENUM(NSUInteger, ClientViewType) {
    ClientViewTypeWorkout,
    ClientViewTypeNutrition
};

@interface NutritionAndWorkoutViewController : BaseDynamicColorViewController

@property (assign, nonatomic) ClientViewType viewType;

@end
