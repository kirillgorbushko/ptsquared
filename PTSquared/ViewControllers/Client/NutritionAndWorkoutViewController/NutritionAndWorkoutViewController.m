//
//  NutritionViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NutritionAndWorkoutViewController.h"
#import "ClientNavigationController.h"
#import "NutritionAndWorkoutTableViewCell.h"
#import "NutritionDetailsViewController.h"
#import "UINavigationController+Addition.h"
#import "Animation.h"
#import "WorkoutDetailesViewController.h"
#import "Snapshot.h"
#import "UIImage+ImageEffects.h"
#import "Nutrition.h"
#import "ProfileManager.h"
#import "ClientNotificationViewController.h"
#import "Workout.h"
#import "GoogleAnaliticsManager.h"

static NSString *const TableViewCellIdentifier = @"nutritionAndWorkoutCell";
static NSString *const NutritionDetailsSegueIdentifier = @"details";

@interface NutritionAndWorkoutViewController()

@property (weak, nonatomic) IBOutlet UIButton *requestButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;

@property (strong, nonatomic) NSArray *dataSource;

@end

@implementation NutritionAndWorkoutViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateUIElements];
    [self setupDataSource];
    [self registerNotificationForUpdate];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updateUIElements];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self unregisterNotifications];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NutritionAndWorkoutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[NutritionAndWorkoutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NutritionAndWorkoutTableViewCell *cell = (NutritionAndWorkoutTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    if (self.viewType) {
        NutritionDetailsViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"nutritionDetails"];
        viewController.selectedNutrition = self.dataSource[indexPath.row];
        [self.navigationController pushViewController:viewController animated:YES];
    } else {
        WorkoutDetailesViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"workoutDetails"];
        viewController.selectedWorkout = self.dataSource[indexPath.row];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)configureCell:(NutritionAndWorkoutTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (self.viewType) {
        Nutrition *selectedNutrition = self.dataSource[indexPath.row];
        cell.planNameLabel.text = [selectedNutrition.nutritionName capitalizedString];
    } else {
        Workout *selectedWorkout = self.dataSource[indexPath.row];
        cell.planNameLabel.text = [selectedWorkout.workoutName capitalizedString];
    }
}

#pragma mark - IBActions

- (IBAction)requestPlanButtonPressed:(id)sender
{
    UIImage *image = [[Snapshot screenshot] applyDarkEffect];
    [AppHelper showLoader];
    __weak typeof(self) weakSelf = self;
    NSInteger trainerID = [ProfileManager sharedManager].profileTrainerID;
    NSInteger clientID = [ProfileManager sharedManager].profileClientID;
    
    if (self.viewType) {
        [[NetworkManager sharedManager] ptSquaredAPIClientGetNutritionRequestForTrainerWithID:trainerID operationResult:^(BOOL success, NSError *error, id response) {
            [weakSelf showResponseAlertWithMessage:response isSuccess:success backgroundImage:image];
            if (success) {
                [[GoogleAnaliticsManager sharedManager] trackClientNutritionRequestWithClientID:clientID];
            }
        }];
    } else {
        [[NetworkManager sharedManager] ptSquaredAPIPerformWorkoutRequestForTrainerWithID:trainerID operationResult:^(BOOL success, NSError *error, id response) {
            [weakSelf showResponseAlertWithMessage:response isSuccess:success backgroundImage:image];
            if (success) {
                [[GoogleAnaliticsManager sharedManager] trackClientWorkoutRequestWithClientID:clientID];
            }
        }];
    }
}

#pragma mark - Notifications

- (void)registerNotificationForUpdate
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveUpdateNotification) name:NotificationKeyDidReceivedNewWorkoutCategory object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveUpdateNotification) name:NotificationKeyDidReceivedNewNutritionCategory object:nil];
}

- (void)unregisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveUpdateNotification
{
    [self setupDataSource];
}

#pragma mark - Private

- (void)showResponseAlertWithMessage:(NSString *)message isSuccess:(BOOL)success backgroundImage:(UIImage *)image
{
    [AppHelper hideLoader];
    if (success) {
        [ClientNotificationViewController presentInformationViewControllerFromViewController:self withText:message backgraundImage:image];
    } else {
        [ClientNotificationViewController presentInformationViewControllerFromViewController:self withText:NotificationMessageRequestWasSentWithError backgraundImage:image];
    }
}

- (void)prepareUIElements
{
    if (self.viewType) {
        [self.requestButton setTitle:@"Request Nutrition plan" forState:UIControlStateNormal];
    } else {
        [self.requestButton setTitle:@"Request Workout" forState:UIControlStateNormal];
    }
}

- (void)updateUIElements
{
    [((ClientNavigationController *)self.navigationController) setTitleLabelToNavigationBar:@"Workout"];

    if (self.viewType) {
        [((ClientNavigationController *)self.navigationController) setTitleLabelToNavigationBar:@"Nutrition"];
    } 
}

- (void)setupDataSource
{
    __weak typeof(self) weakSelf = self;

    void (^ProceedWithResponse)(NSArray *) = ^(NSArray *response){
        weakSelf.dataSource = response;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        });
    };
    
    if (self.viewType) {
        [[NetworkManager sharedManager] ptSquaredAPIClientGetNutritionWithResult:^(BOOL success, NSError *error, id response) {
            if (success) {
                ProceedWithResponse(response);
            }
        }];
    } else {
        [[NetworkManager sharedManager] ptSquaredAPIGetWorkoutList:^(BOOL success, NSError *error, id response) {
            if (success) {
                ProceedWithResponse(response);
            }
        }];
    }
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
    [self.tableView reloadData];
}

@end