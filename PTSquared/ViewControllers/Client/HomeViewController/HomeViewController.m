//
//  HomeViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 23.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "HomeViewController.h"
#import "ClientNavigationController.h"
#import "MembershipProfile.h"
#import "ProfileManager.h"
#import "Session.h"
#import "Animation.h"
#import "CalendarDataSource.h"
#import "GoogleAnaliticsManager.h"
#import "CalendarEventSynchronizer.h"
#import "AppDelegate.h"

@interface HomeViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *codeImageView;
@property (weak, nonatomic) IBOutlet UILabel *trainerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *individualSessionLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupSessionLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextPlannedSessionLabel;
@property (weak, nonatomic) IBOutlet UILabel *trainerPhone;
@property (weak, nonatomic) IBOutlet UILabel *trainerEmail;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;
@property (weak, nonatomic) IBOutlet UIView *separatorViewOne;
@property (weak, nonatomic) IBOutlet UIView *separatorViewTwo;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;

@end

@implementation HomeViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    [[GoogleAnaliticsManager sharedManager] trackClientTimeBetweenAppLaunch];
    
    [[CalendarEventSynchronizer sharedInstance] requestAccess:^(BOOL granted, NSError *error) {
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNeedUpdateUserProfile) name:NotificationKeyNeedUpdateUserProfileData object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateUI];
    [self updateNavigationBar];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - IbActions

- (void)didReceiveNeedUpdateUserProfile
{
    [self updateUI];
}

#pragma mark - Private

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
    self.separatorViewOne.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
    self.separatorViewTwo.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
}

- (void)updateUI
{
    [self startAnimatingActivityIndicator];
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIClientGetMembershipDetailsWithResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            [weakSelf displayDataFromProfile:response];
        }
    }];
}

- (void)updateNavigationBar
{
    UIView *ptLoginView = [self.navigationController.navigationBar viewWithTag:PTCNImageTag];
    if (!ptLoginView) {
        [((ClientNavigationController *)self.navigationController) setTitleImageToNavigationBar:[UIImage imageNamed:@"PT_logo_Coloured"]];
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}

- (void)displayDataFromProfile:(MembershipProfile *)selectedProfile
{    
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (selectedProfile.memberTrainerID) {
            [ProfileManager sharedManager].profileTrainerID = selectedProfile.memberTrainerID;
        }
        [ProfileManager sharedManager].profileTrainerName = selectedProfile.memberTrainerFullName;
        [ProfileManager sharedManager].profileTrainerBussinessName = selectedProfile.memberTrainerBusinessName;
        if (selectedProfile.memberClientID) {
            [ProfileManager sharedManager].profileClientID = selectedProfile.memberClientID;
        }
        
        weakSelf.trainerEmail.text = selectedProfile.memberTrainerEmail;
        weakSelf.trainerNameLabel.text = [selectedProfile.memberTrainerFullName capitalizedString];
        weakSelf.trainerPhone.text =  selectedProfile.memberTrainerPhoneNumber;
        weakSelf.individualSessionLabel.text = [NSString stringWithFormat:@"%ld", (long)selectedProfile.memnerIndividualSessionCount];
        weakSelf.groupSessionLabel.text = [NSString stringWithFormat:@"%ld", (long)selectedProfile.memberGroupSessionCount];
        [weakSelf setQRCodeForClientWithURL:selectedProfile.memberQRCodeUri];
        
        if (selectedProfile.membershipNextSessionInfo.sessionDate) {
            weakSelf.nextPlannedSessionLabel.text = [CalendarDataSource dateStringWithFormattedEndingFromDate:selectedProfile.membershipNextSessionInfo.sessionDate];
        } else {
            weakSelf.nextPlannedSessionLabel.text = @"No session planned";
        }
    });
}

- (void)setQRCodeForClientWithURL:(NSString *)urlToQRCode
{
    if (self.codeImageView.image) {
        return;
    }
    
    __weak typeof (self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIGetImageWithPath:urlToQRCode withCompletition:^(BOOL success, UIImage *response) {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.codeImageView.image = response;
                [weakSelf stopAnimatingActivityIndicator];
            });
        } else {
            weakSelf.imageLogo.hidden = NO;
        }
    }];
}

- (void)startAnimatingActivityIndicator
{
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.imageLogo.hidden = NO;
    });
}

- (void)stopAnimatingActivityIndicator
{
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.imageLogo.hidden = YES;
    });
}

@end
