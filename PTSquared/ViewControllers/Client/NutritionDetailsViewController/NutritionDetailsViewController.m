//
//  NutritionDetailsViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NutritionDetailsViewController.h"
#import "ClientNavigationController.h"
#import "UINavigationController+Addition.h"
#import "ProfileHeaderTableViewCell.h"
#import "NutritionWorkoutInfoViewController.h"
#import "NutritionTableViewCell.h"
#import "Snapshot.h"
#import "Nutrition.h"
#import "Meal.h"
#import "NutritionDay.h"
#import "WorkoutHeadTableViewCell.h"

static NSString *const TableViewCellHeader = @"topCell";
static NSString *const TableViewHeadCellIdentifier = @"headCell";
static NSString *const TableViewCellIdentifierDefault = @"detailedPlanDescriptionCell";

static NSInteger const TableViewCellDefaultHeight = 35.f;
static NSInteger const DefaultCellTextIndent = 30.f;

@interface NutritionDetailsViewController () 

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;

@property (strong, nonatomic) NSArray *nutritionDaysArray;

@end

@implementation NutritionDetailsViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateNavigationController];
    [self receiveDataSource];
}

#pragma mark - Custom Accessors

- (void)setNutritionDaysArray:(NSArray *)nutritionDaysArray
{
    _nutritionDaysArray = nutritionDaysArray;
    [self.tableView reloadData];
}

#pragma mark - IBActions

- (void)rightButtonPressed
{
    NutritionWorkoutInfoViewController *infoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"nutritionInfo"];
    infoViewController.inputData = self.selectedNutrition;
    infoViewController.infoType = ClientViewTypeNutrition;
    infoViewController.backgroundImage = [Snapshot screenshot];
    infoViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
    infoViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
    self.parentViewController.parentViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.navigationController presentViewController:infoViewController animated:NO completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *meals = ((NutritionDay *)self.nutritionDaysArray[section]).nutritionDayMeals;
    return meals.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.nutritionDaysArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NutritionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifierDefault forIndexPath:indexPath];
    if (!cell) {
        cell = [[NutritionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifierDefault];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *meals = ((NutritionDay *)self.nutritionDaysArray[indexPath.section]).nutritionDayMeals;
    Meal *selectedMeal = meals[indexPath.row];
    
    if (!selectedMeal.mealDetails) {
        return TableViewCellDefaultHeight;
    }
    
    NSDictionary *attributes = @{NSFontAttributeName : [UIFont fontWithName:FontProximaNovaSemibold size:16]};
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:selectedMeal.mealDetails attributes:attributes];
    [attributedString setAttributes:attributes range:NSMakeRange(0, selectedMeal.mealDetails.length)];

    CGFloat width = CGRectGetWidth([UIScreen mainScreen].bounds) - DefaultCellTextIndent;
    CGRect rect = [attributedString boundingRectWithSize:CGSizeMake(width, 10000.f) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    return CGRectGetHeight(rect) + TableViewCellDefaultHeight;
}

#pragma mark - UITableViewDelegate

- (void)configureCell:(NutritionTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSArray *meals = ((NutritionDay *)self.nutritionDaysArray[indexPath.section]).nutritionDayMeals;
    Meal *selectedMeal = meals[indexPath.row];
    cell.descriptionLabel.text = selectedMeal.mealDetails;
    NSString *sectionName = [NSString stringWithFormat:@"Meal %i", (int)indexPath.row + 1];
    cell.headerLabel.text = sectionName;
    
    if (indexPath.row == meals.count - 1) {
        [cell setStyleForLastCell];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:17.];
    cell.textLabel.textColor = [UIColor whiteColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    WorkoutHeadTableViewCell *header = [tableView dequeueReusableCellWithIdentifier:TableViewHeadCellIdentifier];
    NSString *sectionName = [NSString stringWithFormat:@"DAY %i", (int)section + 1];
    header.headerTitleLabel.text = sectionName;
    
    
    CAGradientLayer *headerGradient = [CAGradientLayer layer];
    CGRect gradientFrame = header.frame;
    headerGradient.frame = gradientFrame;
    headerGradient.colors = @[(id)[[[ColorSchemeManager sharedManager] interfaceColor] colorWithAlphaComponent:0.5f].CGColor, (id)[UIColor clearColor].CGColor];
    [header.layer insertSublayer:headerGradient below:header.headerTitleLabel.layer];
    
    return header;
}

#pragma mark - Private

- (void)updateNavigationController
{
    [self updateNavigationControllerTitleLabel:self.selectedNutrition.nutritionName];
    [self.navigationController setBackButtonStyle:BackButtonStyleDark];
    [self.navigationController setRightButtonWithImageNamed:@"Nutrition_Plan_Info_Icon" andDelegate:self];
}

- (void)updateNavigationControllerTitleLabel:(NSString *)title
{
    if (title.length) {
        [((ClientNavigationController *)self.navigationController) setTitleLabelToNavigationBar:title];
    } else {
        [((ClientNavigationController *)self.navigationController) setTitleLabelToNavigationBar:@"Nutrition Details"];
    }
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
    [self.tableView reloadData];
}

- (void)receiveDataSource
{
    self.nutritionDaysArray = self.selectedNutrition.nutritionNutritionMealsDetails.nutritionDays;
}

@end
