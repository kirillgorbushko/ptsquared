//
//  NutritionDetailsViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 28.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//
@class Nutrition;

@interface NutritionDetailsViewController : BaseDynamicColorViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) Nutrition *selectedNutrition;

@end
