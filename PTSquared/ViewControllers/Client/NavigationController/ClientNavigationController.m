//
//  PTClientNavController.m
//  PTSquared
//
//  Created by Kirill on 11/11/14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

#import "ClientNavigationController.h"
#import "Animation.h"

static CGFloat const PTCNMinMenuActionOffset = 120;
static CGFloat const PTCNMenuOffset = 60;
static CGFloat const AnimationDuration = 0.4f;
static CGFloat const SpringAnimationOffset = 25;

@interface ClientNavigationController ()

@property (assign, nonatomic) BOOL isMenuOpened;
@property (strong, nonatomic) UIButton *menuButton;
@property (assign, nonatomic) CGPoint startLocation;

@property (strong, nonatomic) UIView *dimmedView;
@property (assign, nonatomic) BOOL isAnimationForDimmedViewRequired;

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *navigationTitleLabel;

@end

@implementation ClientNavigationController

#pragma mark - LifeCycle

- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        [self configureNavigationBarStyle];
        [self configureNavigationBarItems:(UIViewController *)rootViewController];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        [self prepareDimmedView];
    }
    return self;
}

#pragma mark - IBActions

- (void)menuButtonPress
{
    [self setPositionForContainerFromPoint:self.view.center];
}

#pragma mark - Public

- (void)configureNavigationBarItems:(UIViewController *)rootViewController
{
    UIImage *buttonImage = [UIImage imageNamed:@"Menu_Icon"];
    self.menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height)];
    [self.menuButton setImage:buttonImage forState:UIControlStateNormal];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:self.menuButton];
    [self.menuButton addTarget:self action: @selector(menuButtonPress) forControlEvents:UIControlEventTouchUpInside];
    rootViewController.navigationItem.leftBarButtonItem = menuBarButton;
    [self configurePanGesture];
}

- (void)hideMenuButton
{
    [self.menuButton removeFromSuperview];
}

- (void)unhideMenuButtonAtViewController:(UIViewController *)rootViewController
{
    [self configureNavigationBarItems:rootViewController];
}

- (void)setTitleImageToNavigationBar:(UIImage *)image
{
    CGPoint navBarCenter = self.navigationBar.center;
    CGRect imageFrame = CGRectMake(navBarCenter.x - image.size.width / 2, navBarCenter.y - image.size.height / 2 - [[UIApplication sharedApplication] statusBarFrame].size.height, image.size.width, image.size.height);
    self.imageView = [[UIImageView alloc] initWithImage:image];
    self.imageView.frame = imageFrame;
    self.imageView.tag = PTCNImageTag;
    [self.navigationBar addSubview:self.imageView];
}

- (void)setTitleLabelToNavigationBar:(NSString *)titleText
{
    if (titleText.length) {
        if (!self.navigationTitleLabel.superview) {
            [self addTitleLabelToNavigationBar];
        }
        self.navigationTitleLabel.text = titleText;
    }
}

#pragma mark - Private

- (void)setPositionForContainerFromPoint:(CGPoint)point
{
    CAKeyframeAnimation *moveAnimation;
    if (!self.isMenuOpened) {
        moveAnimation = [self animationForOpenMenuFromPoint:point];
        [self.view.layer addAnimation:moveAnimation forKey:nil];
        self.view.layer.position = [self calculateOpenMenuPosition];
        
        if (self.isAnimationForDimmedViewRequired) {
            self.dimmedView.hidden = NO;
            [self.dimmedView.layer addAnimation: [Animation fadeAnimFromValue:0. to:1. delegate:nil] forKey:nil];
            self.dimmedView.layer.opacity = 1.f;
            self.isAnimationForDimmedViewRequired = NO;
        }
    } else {
        moveAnimation = [self animationForClosedMenuFromPoint:point];
        [self.view.layer addAnimation:moveAnimation forKey:nil];
        self.view.layer.position = [self calculateClosedMenuPosition];
        
            [self.dimmedView.layer addAnimation: [Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"hideDimmedView"];
            self.dimmedView.layer.opacity = 0.f;
        self.isAnimationForDimmedViewRequired = YES;
    }
    self.isMenuOpened = !self.isMenuOpened;
}

- (void)prepareDimmedView
{
    CGFloat topBarSize = [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationBar.frame.size.height;
    CGRect frameForDimmedView = CGRectMake(0, topBarSize, self.view.frame.size.width, self.view.frame.size.height - topBarSize);
    
    self.dimmedView = [[UIView alloc] initWithFrame:frameForDimmedView];
    self.dimmedView.backgroundColor = [UIColor dimmedColor];
    
    [self.view addSubview:self.dimmedView];
    self.dimmedView.hidden = YES;
    
    self.isAnimationForDimmedViewRequired = YES;
}

- (void)addTitleLabelToNavigationBar
{
    CGSize navBarSize = self.navigationBar.frame.size;
    self.navigationTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, navBarSize.width - navBarSize.height * 2,  navBarSize.height)];
    self.navigationTitleLabel.minimumScaleFactor = 0.5;
    self.navigationTitleLabel.adjustsFontSizeToFitWidth = YES;
    self.navigationTitleLabel.center = CGPointMake(navBarSize.width / 2, navBarSize.height / 2);
    self.navigationTitleLabel.backgroundColor = [UIColor clearColor];
    self.navigationTitleLabel.textColor = [UIColor blackColor];
    self.navigationTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationTitleLabel.numberOfLines = 0;
    self.navigationTitleLabel.tag = PTCNLabelTag;
    [self.navigationBar addSubview:self.navigationTitleLabel];
}

- (void)configureNavigationBarStyle
{
    self.navigationBar.translucent = NO;
    self.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationBar.tintColor = [UIColor whiteColor];
}

- (CGPoint)calculateOpenMenuPosition
{
    CGPoint toPoint = self.view.center;
    toPoint.x = self.view.frame.size.width * 1.4f - PTCNMenuOffset;
    return toPoint;
}

- (CGPoint)calculateClosedMenuPosition
{
    CGPoint startPoint = CGPointMake(self.parentViewController.view.frame.size.width / 2, self.parentViewController.view.frame.size.height / 2);
    return startPoint;
}

- (CGPoint)calculateCurrentCenterOfView:(CGPoint)fromCurrentLocation viewLocation:(CGPoint)viewLocation
{
    CGPoint fromPoint = self.view.center;
    fromPoint.x = fromCurrentLocation.x - viewLocation.x + self.parentViewController.view.frame.size.width / 2;
    return fromPoint;
}

#pragma mark - UIAlertViewDelegate


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.dimmedView.layer animationForKey:@"hideDimmedView"]) {
        [self.dimmedView.layer removeAllAnimations];
        self.dimmedView.hidden = YES;
    }
}

#pragma mark - Gesture

- (void)configurePanGesture
{
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panMenuButton:)];
    panGesture.delaysTouchesBegan = NO;
    [self.menuButton addGestureRecognizer:panGesture];
}

- (void)panMenuButton:(UIGestureRecognizer *)gesture
{
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:{
            self.startLocation = [gesture locationInView:self.view];
            break;
        }
        case UIGestureRecognizerStateChanged: {
            CGPoint currentLocation = [gesture locationInView:self.parentViewController.view];
            [self menuButtonDidMoveAtDistance:(currentLocation.x - self.startLocation.x)];
            self.startLocation = currentLocation;
            break;
        }
        case UIGestureRecognizerStateEnded: {
            [self endMoveWithGlobalLocation:[gesture locationInView:self.parentViewController.view] viewLocation:[gesture locationInView:self.view]];
            break;
        }
        default:
            break;
    }
}

- (void)menuButtonDidMoveAtDistance:(CGFloat)distance
{
    CGFloat maxValueX = self.view.frame.size.width - PTCNMenuOffset;
    if (self.view.frame.origin.x >= 0  && self.view.frame.origin.x <= maxValueX) {
        CGPoint center = self.view.center;
        center.x += distance;
        if (center.x >= self.view.frame.size.width / 2 && center.x <= maxValueX + self.view.frame.size.width / 2) {
            self.view.center = center;
        }
    }
}

- (void)endMoveWithGlobalLocation:(CGPoint)location viewLocation:(CGPoint)viewLocation
{
    [self setAnimationDirection:location viewLocation:viewLocation];
    if (location.x != [self calculateOpenMenuPosition].x || location.x != [self calculateClosedMenuPosition].x) {
        [self setPositionForContainerFromPoint:[self calculateCurrentCenterOfView:location viewLocation:viewLocation]];
    }
}

- (void)setAnimationDirection:(CGPoint)location viewLocation:(CGPoint)viewLocation
{
    if (self.isMenuOpened) {
        CGFloat pointX = self.parentViewController.view.frame.size.width - PTCNMinMenuActionOffset - PTCNMenuOffset;
        if (location.x - viewLocation.x > pointX){
            self.isMenuOpened = !self.isMenuOpened;
        }
    } else {
        if (location.x - viewLocation.x < PTCNMinMenuActionOffset){
            self.isMenuOpened = !self.isMenuOpened;
        }
    }
}

#pragma mark - Animation

- (CAKeyframeAnimation *)animationForOpenMenuFromPoint:(CGPoint)location
{
    CAKeyframeAnimation *moveAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    CGPoint finalPosition = [self calculateOpenMenuPosition];
    
    if (self.view.frame.origin.x < finalPosition.x - self.view.frame.size.width / 2 - SpringAnimationOffset) {
        moveAnimation.values = @[
                                 [NSValue valueWithCGPoint:location],
                                 [NSValue valueWithCGPoint:CGPointMake(finalPosition.x + 10, finalPosition.y)],
                                 [NSValue valueWithCGPoint:CGPointMake(finalPosition.x - 10, finalPosition.y)],
                                 [NSValue valueWithCGPoint:finalPosition]
                                 ];
        moveAnimation.duration = AnimationDuration;
    } else {
        moveAnimation.values = @[
                                 [NSValue valueWithCGPoint:location],
                                 [NSValue valueWithCGPoint:finalPosition]
                                 ];
        moveAnimation.duration = AnimationDuration / 4;
    }
    moveAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    return moveAnimation;
}

- (CAKeyframeAnimation *)animationForClosedMenuFromPoint:(CGPoint)location
{
    CAKeyframeAnimation *moveAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    CGPoint finalPosition = [self calculateClosedMenuPosition];
    
    if (self.view.frame.origin.x > SpringAnimationOffset) {
        moveAnimation.values = @[
                                 [NSValue valueWithCGPoint:location],
                                 [NSValue valueWithCGPoint:CGPointMake(finalPosition.x, finalPosition.y)],
                                 [NSValue valueWithCGPoint:CGPointMake(finalPosition.x + 10, finalPosition.y)],
                                 [NSValue valueWithCGPoint:finalPosition]
                                 ];
        moveAnimation.duration = AnimationDuration;
        
    } else {
        moveAnimation.values = @[
                                 [NSValue valueWithCGPoint:location],
                                 [NSValue valueWithCGPoint:finalPosition]
                                 ];
        moveAnimation.duration = AnimationDuration / 4;
    }
    moveAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    return moveAnimation;
}

@end
