//
//  ClientNotificationViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 15.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ClientNotificationViewController.h"
#import "Animation.h"

@interface ClientNotificationViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end

@implementation ClientNotificationViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self prepareButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
#ifdef HIDE_STATUS_BAR_ON_INFO
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
#endif
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
    
    [self prepareUI];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
#ifdef HIDE_STATUS_BAR_ON_INFO
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
#endif
}

#pragma mark - IBActions

- (IBAction)closeButtonPress:(id)sender
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"fadeAnimation"];
    self.view.layer.opacity = 0.f;
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"fadeAnimation"]) {
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - Public

+ (void)presentInformationViewControllerFromViewController:(UIViewController *)presenter withText:(NSString *)informationText backgraundImage:(UIImage *)backgroundImage
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ClientMode" bundle:nil];
    ClientNotificationViewController *clientNotificationViewController = [storyboard instantiateViewControllerWithIdentifier:@"notificationViewController"];
    
    clientNotificationViewController.image = backgroundImage;
    clientNotificationViewController.text = informationText;
    clientNotificationViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
    clientNotificationViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
    presenter.parentViewController.parentViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [presenter.navigationController presentViewController:clientNotificationViewController animated:NO completion:nil];
}

#pragma mark - Private

- (void)prepareButton
{
    self.closeButton.layer.cornerRadius = 5.f;
    self.closeButton.layer.borderWidth = 1.5f;
    self.closeButton.layer.borderColor = [UIColor grayColor].CGColor;
}

- (void)prepareUI
{
    self.backgroundImageView.image = self.image;
    self.informationLabel.text = self.text;
}

@end
