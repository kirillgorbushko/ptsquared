//
//  ClientNotificationViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 15.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface ClientNotificationViewController : UIViewController

@property (strong, nonatomic) UIImage *image;
@property (copy, nonatomic) NSString *text;

+ (void)presentInformationViewControllerFromViewController:(UIViewController *)presenter withText:(NSString *)informationText backgraundImage:(UIImage *)backgroundImage;

@end
