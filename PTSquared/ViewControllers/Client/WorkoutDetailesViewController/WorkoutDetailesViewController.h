//
//  WorkoutDetailesViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//
@class Workout;

@interface WorkoutDetailesViewController : BaseDynamicColorViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) __block Workout *selectedWorkout;

@end
