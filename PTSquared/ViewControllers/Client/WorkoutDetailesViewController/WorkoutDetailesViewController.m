//
//  WorkoutDetailesViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "WorkoutDetailesViewController.h"
#import "ClientNavigationController.h"
#import "UINavigationController+Addition.h"
#import "WorkoutTableViewCell.h"
#import "SupersetHeaderTableViewCell.h"
#import "NutritionWorkoutInfoViewController.h"
#import "Snapshot.h"
#import "Workout.h"
#import "Exercise.h"

static NSString *const TableViewCellIdentifier = @"workoutCell";
static NSString *const SupersetHeader = @"supersetHeader";
static NSString *const SupersetName = @"superset";
static CGFloat const SupersetHeaderDefaultHeight = 35.f;
static CGFloat const CellHeight = 51.f;

@interface WorkoutDetailesViewController () 

@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) __block NSArray *exercisesForWorkout;
@property (copy, nonatomic) __block NSString *planName;

@end

@implementation WorkoutDetailesViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareNavigationController];
    [self fetchWorkoutData];
}

#pragma mark - CustomAccessors

- (void)setExercisesForWorkout:(NSArray *)exercisesForWorkout
{
    _exercisesForWorkout = exercisesForWorkout;
    [self.tableView reloadData];
}

#pragma mark - IBActions

- (void)rightButtonPressed
{
    NutritionWorkoutInfoViewController *infoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"nutritionInfo"];
    infoViewController.infoType = ClientViewTypeWorkout;
    infoViewController.backgroundImage = [Snapshot screenshot];
    infoViewController.inputData = self.selectedWorkout;
    infoViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
    infoViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
    self.parentViewController.parentViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.navigationController presentViewController:infoViewController animated:NO completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.exercisesForWorkout.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    Exercise *currentExersise = self.exercisesForWorkout[section];
    return currentExersise.exerciseSets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WorkoutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[WorkoutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
    }
    
    Exercise *currentExersise = self.exercisesForWorkout[indexPath.section];
    if ([currentExersise.exerciseName isEqualToString:SupersetName]) {
        cell.type = WorkoutTableViewCellTypeSuperset;
    } else {
        cell.type = WorkoutTableViewCellTypeNormal;
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SupersetHeaderTableViewCell *header = [tableView dequeueReusableCellWithIdentifier:SupersetHeader];
    header.titleLabel.text = @"SUPERSET";
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    Exercise *currentExersise = self.exercisesForWorkout[section];
    if ([currentExersise.exerciseName isEqualToString:SupersetName]) {
        return SupersetHeaderDefaultHeight;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Exercise *currentExersise = self.exercisesForWorkout[indexPath.section];
    if ([currentExersise.exerciseName isEqualToString:SupersetName]) {
        return SupersetHeaderDefaultHeight;
    }
    return CellHeight;
}

- (void)configureCell:(WorkoutTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSInteger currentSection = indexPath.section;
    
    Exercise *activeSuperExercise = self.exercisesForWorkout[currentSection];
    Exercise *exerciseToDisplay = activeSuperExercise.exerciseSets[indexPath.row];
    NSString *exersiseName;
    NSString *valueType = exerciseToDisplay.exerciseValueType;
    NSString *intervalType = exerciseToDisplay.exerciseIntervalType;
    if ([activeSuperExercise.exerciseName isEqualToString:SupersetName]) {
        exersiseName = exerciseToDisplay.exerciseName;
    } else {
        exersiseName = activeSuperExercise.exerciseName;
    }
    
    NSInteger valueCount = exerciseToDisplay.exerciseSetValueCount;
    NSInteger intervals = exerciseToDisplay.exerciseSetIntervalCount;
    cell.repeatCountLabel.text = [NSString stringWithFormat:@"%i %@", (int)intervals, intervalType];
    cell.descriptionLabel.text = exersiseName;
    cell.notesLabel.text =  [NSString stringWithFormat:@"%i%@", (int)valueCount, valueType];
    
    Exercise *currentExersise = self.exercisesForWorkout[indexPath.section];
    NSInteger numberofItemsInSection = currentExersise.exerciseSets.count;
    if (indexPath.row == numberofItemsInSection - 1) {
        [cell showSeparatorView];
    }
    
}

#pragma mark - Private

- (void)fetchWorkoutData
{
    self.planName =  self.selectedWorkout.workoutName;
    [self setTitleNameForViewController];
    
    NSArray *sortedExersises = [self.selectedWorkout.workoutDetailedExerciseList.workoutExersises sortedArrayUsingComparator:^NSComparisonResult(Exercise  * __nonnull exercise1, Exercise   * __nonnull exercise2) {
        return exercise1.exerciseID > exercise2.exerciseID;
    }];
    
    self.exercisesForWorkout = sortedExersises;
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
    [self.tableView reloadData];
}

- (void)prepareNavigationController
{
    [self setTitleNameForViewController];
    [self.navigationController setBackButtonStyle:BackButtonStyleDark];
    [self.navigationController setRightButtonWithImageNamed:@"Workout_Info_Icon" andDelegate:self];
}

- (void)setTitleNameForViewController
{
    if (self.planName.length) {
        [((ClientNavigationController *)self.navigationController) setTitleLabelToNavigationBar:self.planName];
    }
}

@end
