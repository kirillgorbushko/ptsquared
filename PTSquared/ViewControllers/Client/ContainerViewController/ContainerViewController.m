//
//  PTContainerViewController.m
//  PTSquared
//
//  Created by Stas Volskyi on 10.11.14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

#import "ContainerViewController.h"
#import "TutorialViewController.h"
#import "HomeViewController.h"
#import "ClientCalendarViewController.h"
#import "Snapshot.h"
#import "UIImage+ImageEffects.h"
#import "NutritionAndWorkoutViewController.h"
#import "ProfileViewController.h"
#import "LogoutViewController.h"
#import <QuartzCore/QuartzCore.h>

static NSString *const CMClientStoryBoardName= @"ClientMode";
static NSString *const LoginStoryBoardName= @"Login";

@interface ContainerViewController () 

@property (strong, nonatomic) NSArray *menuControllers;

@end

@implementation ContainerViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureMenu];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showTutorial];
}

#pragma mark - Private

- (void)showTutorial
{
    if (self.isFirstLogin) {
        TutorialViewController *tutorialController = [[UIStoryboard storyboardWithName:@"Login" bundle:nil] instantiateViewControllerWithIdentifier:@"tutorial"];
        tutorialController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
        tutorialController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self.navigationController presentViewController:tutorialController animated:NO completion:nil];
    }
}

- (void)configureMenu
{
    if (!self.menuControllers) {
        [self initialisationMenuControllers];
    }
    MenuViewController *menuViewController = [[UIStoryboard storyboardWithName:CMClientStoryBoardName bundle:nil] instantiateViewControllerWithIdentifier:@"menu"];
    menuViewController.delegate = self;
    self.navigationController = [[ClientNavigationController alloc] initWithRootViewController:self.menuControllers[0]];
    
    [self addChildViewController:menuViewController];
    [self addChildViewController:self.navigationController];
    
    [self.view addSubview:menuViewController.view];
    [self.view addSubview:self.navigationController.view];
}

- (void)initialisationMenuControllers
{
    HomeViewController *homeViewController = [[UIStoryboard storyboardWithName:CMClientStoryBoardName bundle:nil] instantiateViewControllerWithIdentifier:@"homeViewController"];
    ClientCalendarViewController *calendarViewController = [[UIStoryboard storyboardWithName:CMClientStoryBoardName bundle:nil] instantiateViewControllerWithIdentifier:@"calendar"];
    
    NutritionAndWorkoutViewController *workoutViewController = [[UIStoryboard storyboardWithName:CMClientStoryBoardName bundle:nil] instantiateViewControllerWithIdentifier:@"nutritionAndWorkout"];
    workoutViewController.viewType = ClientViewTypeWorkout;
    
    NutritionAndWorkoutViewController *nutritionViewController = [[UIStoryboard storyboardWithName:CMClientStoryBoardName bundle:nil] instantiateViewControllerWithIdentifier:@"nutritionAndWorkout"];
    nutritionViewController.viewType = ClientViewTypeNutrition;

    ProfileViewController *profileViewController = [[UIStoryboard storyboardWithName:CMClientStoryBoardName bundle:nil] instantiateViewControllerWithIdentifier:@"profile"];
    LogoutViewController *logoutViewController = [[UIStoryboard storyboardWithName:LoginStoryBoardName bundle:nil] instantiateViewControllerWithIdentifier:@"logout"];
    
    self.menuControllers = @[homeViewController, calendarViewController, workoutViewController, nutritionViewController, profileViewController, logoutViewController];
}

- (void)cleanUpNavigationBarItems
{
    UIView *view = [self.navigationController.navigationBar viewWithTag:PTCNImageTag];
    [view removeFromSuperview];
    view = [self.navigationController.navigationBar viewWithTag:PTCNLabelTag];
    [view removeFromSuperview];
}

#pragma mark - PTMenuViewControllerDelegate

- (void)selectMenuItem:(NSInteger)controllerIndex
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (controllerIndex == (int)self.menuControllers.count - 1) {
            UIImage *snapShot = [[Snapshot screenshot] applyDarkEffect];
            LogoutViewController *logoutController = [self.menuControllers lastObject];
            logoutController.backgroundImage = snapShot;
#ifdef __IPHONE_8_0
            logoutController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
            self.modalPresentationStyle = UIModalPresentationCurrentContext;
            [self presentViewController:logoutController animated:NO completion:nil];
        } else {
            if (![self.navigationController.topViewController isEqual:self.menuControllers[controllerIndex]]) {
                [self.navigationController popToRootViewControllerAnimated:NO];
                [self.navigationController setViewControllers:[NSArray arrayWithObject:self.menuControllers[controllerIndex]] animated:NO];
                [self cleanUpNavigationBarItems];
                [self.navigationController configureNavigationBarItems:self.menuControllers[controllerIndex]];
            }
            [self.navigationController menuButtonPress];
        }
    });
}

#pragma mark - Public for Push

- (void)selecItem:(NSUInteger)menuItem
{
    if (menuItem < 2 && menuItem > 3) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![self.navigationController.topViewController isEqual:self.menuControllers[menuItem]]) {
            [self.navigationController popToRootViewControllerAnimated:NO];
            [self.navigationController setViewControllers:[NSArray arrayWithObject:self.menuControllers[menuItem]] animated:NO];
            [self cleanUpNavigationBarItems];
            [self.navigationController configureNavigationBarItems:self.menuControllers[menuItem]];
        }
    });
}

@end
