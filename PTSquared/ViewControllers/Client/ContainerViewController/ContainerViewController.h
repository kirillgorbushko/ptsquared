//
//  PTContainerViewController.h
//  PTSquared
//
//  Created by Stas Volskyi on 10.11.14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//
#import "MenuViewController.h"
#import "ClientNavigationController.h"

@interface ContainerViewController : UIViewController <MenuViewControllerDelegate>

@property (strong, nonatomic) ClientNavigationController *navigationController;
@property (assign, nonatomic) BOOL isFirstLogin;

- (void)selecItem:(NSUInteger)menuItem;

@end
