//
//  ProfileViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface ProfileViewController : BaseDynamicColorViewController <UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate>

@end
