//
//  ProfileViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ProfileViewController.h"
#import "ClientNavigationController.h"
#import "ProfileHeaderTableViewCell.h"
#import "ProfileTableViewCell.h"
#import "ProfileCollectionViewCell.h"
#import "ClientProfile.h"
#import "ProfileFooterTableViewCell.h"

static NSString *const CollectionViewCelldentifier = @"stateCell";
static NSString *const TableViewCellIdentifier = @"profileCell";
static NSString *const TableViewCellHeader = @"header";
static NSString *const TableViewCellFooter = @"footer";
static NSString *const ProfilePlistName = @"ProfileItems";
static CGFloat const TableCellHeight = 50;

static NSString *const KeyBeforePhoto = @"photoBefore";
static NSString *const KeyAfterPhoto = @"photoAfter";
static NSString *const KeyDataSourceItemName = @"itemName";
static NSString *const KeyDataSourceItemValue = @"itemValues";

@interface ProfileViewController()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *selectedDetailsLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;

@property (strong, nonatomic) NSDictionary *profileData;
@property (strong, nonatomic) NSArray *staticTableViewDataSource;

@property (strong, nonatomic) __block NSMutableDictionary *clientPhotos;

@end

@implementation ProfileViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setPlaceholdersData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self receiveClientProfile];
    [((ClientNavigationController *)self.navigationController) setTitleLabelToNavigationBar:@"Profile"];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCelldentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[ProfileCollectionViewCell alloc] init];
    }
    [self configureCollectionViewCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    for (NSIndexPath *indexPath in @[[self.collectionView.indexPathsForVisibleItems firstObject]]) {
        ProfileCollectionViewCell *cell = (ProfileCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        if (self.collectionView.contentOffset.x > cell.frame.size.width / 2) {
            self.selectedDetailsLabel.text = @"After";
        } else {
            self.selectedDetailsLabel.text = @"Before";
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSDictionary *tableViewHeaders = [self.staticTableViewDataSource valueForKey:KeyDataSourceItemName];
    return tableViewHeaders.count ? tableViewHeaders.count - 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *tableViewContentsLabel = [self.staticTableViewDataSource valueForKey:KeyDataSourceItemValue];
    NSArray *selectedSection = tableViewContentsLabel[section];
    
    NSInteger elementsCount;
    if (selectedSection.count) {
        elementsCount = selectedSection.count;
    } else {
        elementsCount = 1;
    }
    return elementsCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[ProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableCellHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ProfileHeaderTableViewCell *header = [tableView dequeueReusableCellWithIdentifier:TableViewCellHeader];
    NSArray *tableViewHeaders = [self.staticTableViewDataSource valueForKey:KeyDataSourceItemName];
    header.headerNameLabel.text = tableViewHeaders[section];
    return header;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    ProfileFooterTableViewCell *footer = [tableView dequeueReusableCellWithIdentifier:TableViewCellFooter];
    for (UIView *innerView in footer.subviews) {
        for (UIView *footerView in innerView.subviews) {
            if (footerView.tag == 120) {
                footerView.backgroundColor = [[ColorSchemeManager sharedManager] interfaceColor];
            }
        }
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    NSArray *tableViewHeaders = [self.staticTableViewDataSource valueForKey:KeyDataSourceItemName];
    
    if (section == (int)tableViewHeaders.count - 1) {
        return 0;
    } else {
        return 30;
    }
}

#pragma mark - Custom Accessors

- (void)setStaticTableViewDataSource:(NSArray *)staticTableViewDataSource
{
    _staticTableViewDataSource = staticTableViewDataSource;
    [self.tableView reloadData];
}

#pragma mark - Private

- (void)configureCell:(ProfileTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSArray *tableViewContentsLabel = [self.staticTableViewDataSource valueForKey:KeyDataSourceItemValue];
    NSArray *selectedSectionItems = tableViewContentsLabel[indexPath.section];
    NSDictionary *selectedRow = selectedSectionItems[indexPath.row];
    
    if (selectedSectionItems.count) {
        if ([selectedRow.allKeys[0] isEqualToString:@""]) {
            cell.parameterNameLabel.text = selectedRow.allValues[0];
            cell.parameterValueLabel.text = nil;
            return;
        }
        cell.parameterNameLabel.text = selectedRow.allKeys[0];
        cell.parameterValueLabel.text = selectedRow.allValues[0];
    }
}

- (void)configureCollectionViewCell:(ProfileCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    UIImage *imageToShow;
    if (indexPath.row) {
        imageToShow = self.clientPhotos[KeyAfterPhoto];
    } else {
        imageToShow = self.clientPhotos[KeyBeforePhoto];
    }
    [self showImage:imageToShow inCell:cell];
}

- (void)showImage:(UIImage *)imageToShow inCell:(ProfileCollectionViewCell *)cell
{
    if (imageToShow) {
        [cell stopAnimatingActivityIndicator];
        cell.clientPhotoImageView.image = imageToShow;
    } else {
        [cell startAnimatingActivityIndicator];
    }
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
    [self.tableView reloadData];
}

- (void)receiveClientProfile
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIClientProfileWithResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            weakSelf.staticTableViewDataSource = response;
            [weakSelf profilePhotosWithResponce:[response lastObject]];
        } else {
            [weakSelf setPlaceholdersData];
        }
    }];
}

- (void)profilePhotosWithResponce:(NSDictionary *)response
{
    NSDictionary *photosDataSource = response[KeyDataSourceItemValue];
    [self addPhotoWithPath:[photosDataSource valueForKey:KeyBeforePhoto] key:KeyBeforePhoto];
    [self addPhotoWithPath:[photosDataSource valueForKey:KeyAfterPhoto] key:KeyAfterPhoto];
}

- (void)addPhotoWithPath:(NSString *)path key:(NSString *)photoKey
{
    if (!self.clientPhotos) {
        self.clientPhotos = [[NSMutableDictionary alloc] init];
    }
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIGetImageWithPath:path withCompletition:^(BOOL success, UIImage *response) {
        if (success) {
            [weakSelf.clientPhotos setValue:response forKey:photoKey];
            if ([weakSelf.clientPhotos allKeys].count == 2) {
                [weakSelf.collectionView reloadData];
            }
        }
    }];
}

- (void)setPlaceholdersData
{
    self.staticTableViewDataSource =  [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProfileDataSource" ofType:@"plist"]];
}

@end