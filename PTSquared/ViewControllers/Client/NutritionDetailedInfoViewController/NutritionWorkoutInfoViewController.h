//
//  NutritionDetailedInfoViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NutritionAndWorkoutViewController.h"

@interface NutritionWorkoutInfoViewController : UIViewController

@property (strong, nonatomic) id inputData;
@property (assign, nonatomic) ClientViewType infoType;
@property (strong, nonatomic) UIImage *backgroundImage;

@end
