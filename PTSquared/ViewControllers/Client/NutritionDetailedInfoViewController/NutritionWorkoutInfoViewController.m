//
//  NutritionDetailedInfoViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NutritionWorkoutInfoViewController.h"
#import "Animation.h"
#import "UIImage+ImageEffects.h"
#import "Workout.h"
#import "CalendarDataSource.h"
#import "Nutrition.h"
#import "NutritionDay.h"

@interface NutritionWorkoutInfoViewController ()

@property (weak, nonatomic) IBOutlet UILabel *planTypeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nutritionQtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *nutritionDurationLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *nutritionTypeLabel;
@property (weak, nonatomic) IBOutlet UIView *nutritionView;
@property (weak, nonatomic) IBOutlet UIView *workOutView;
@property (weak, nonatomic) IBOutlet UILabel *workOutType;
@property (weak, nonatomic) IBOutlet UILabel *workoutDurationType;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation NutritionWorkoutInfoViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
#ifdef HIDE_STATUS_BAR_ON_INFO
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
#endif
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
    
    [self prepareInterface];
    [self fetchDataForSelectedDataType];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
#ifdef HIDE_STATUS_BAR_ON_INFO
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
#endif
}

#pragma mark - IBActions

- (IBAction)closeButtonPress:(id)sender
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"fadeAnimation"];
    self.view.layer.opacity = 0.f;
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"fadeAnimation"]) {
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - Private

- (void)prepareInterface
{
    if (self.infoType){
        self.nutritionView.hidden = NO;
    } else {
        self.workOutView.hidden = NO;
    }
    
    self.backgroundImageView.image = [self.backgroundImage applyDarkEffect];
}

- (void)fetchDataForSelectedDataType
{
    if (self.infoType) {
        [self getNutritionDetails];
    } else {
        [self getWorkoutDetails];
    }
}

- (void)getWorkoutDetails
{    
    Workout *currentWorkout = (Workout *)self.inputData;    
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIGetWorkoutDetailsForWorkoutID:currentWorkout.workoutID operationResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            Workout *receivedWorkout = response;
            weakSelf.workoutDurationType.text = [NSString stringWithFormat:@"%.0f min", receivedWorkout.workoutDuration];
            weakSelf.planTypeTitleLabel.text = receivedWorkout.workoutName;
            weakSelf.textView.text = receivedWorkout.workoutDetails;
            weakSelf.workOutType.text = receivedWorkout.workoutTypeName;
        }
    }];
}

- (void)getNutritionDetails
{
    Nutrition *currentNutrition = (Nutrition *)self.inputData;
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIClientGetNutritionDetailsForNutritionID:currentNutrition.nutritionObjectID operationResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            [weakSelf updateUIWithNutrition:response];
        }
    }];
}

- (void)updateUIWithNutrition:(Nutrition *)nutrition
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.nutritionDurationLabel.text = [NSString stringWithFormat:@"%i day(s)", (int)nutrition.nutritionDaysNumber];
        weakSelf.nutritionQtyLabel.text = [NSString stringWithFormat:@"%i meal(s)", (int)nutrition.nutritionMealsPerDay];
        weakSelf.nutritionTypeLabel.text = nutrition.nutritionTypeName;
        weakSelf.textView.text = nutrition.nutritionDetails;
        weakSelf.planTypeTitleLabel.text = nutrition.nutritionName;
    });
}

@end
