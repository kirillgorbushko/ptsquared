//
//  CalendarViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ClientCalendarViewController.h"
#import "ClientNavigationController.h"
#import "EventDescriptionTableViewCell.h"
#import "SessionDetailsViewController.h"
#import "CalendarCell.h"
#import "CalendarDataSource.h"
#import "CalendarDay.h"
#import "ClientNotificationViewController.h"
#import "Snapshot.h"
#import "UIImage+ImageEffects.h"
#import "UINavigationController+Addition.h"
#import "Session.h"
#import "Animation.h"
#import "ProfileManager.h"
#import "GoogleAnaliticsManager.h"
#import "MBProgressHUD.h"
#import "CalendarEventSynchronizer.h"

static NSString *const TableViewCellIdentifier = @"eventDescriptionCell";
static CGFloat const TableViewCellHeight = 51;

@interface ClientCalendarViewController()

@property (weak, nonatomic) IBOutlet UILabel *monthNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *requestButton;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;
@property (weak, nonatomic) IBOutlet UIView *dayNameContainer;
@property (weak, nonatomic) IBOutlet UIView *sessionView;
@property (weak, nonatomic) IBOutlet UIView *informationView;

@property (weak, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) CalendarDataSource *dataSourceProvider;
@property (strong, nonatomic) NSDate *actualDateForSessionToDisplay;
@property (strong, nonatomic) NSIndexPath *selectedDayIndexPath;
@property (strong, nonatomic) NSIndexPath *todayIndexPath;
@property (strong, nonatomic) __block NSMutableArray *tableViewDataSource;

@end

@implementation ClientCalendarViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self preparePageControl];
    [self prepareTableView];
    [self prepareDataSource];
    [self updateDataSourceForDate:[NSDate date]];
    [self updateMonthNameWithDate:[NSDate date]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setRightButtonWithText:@"TODAY" andDelegate:self];
    [((ClientNavigationController *)self.navigationController) setTitleLabelToNavigationBar:@"Calendar"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setBackButtonStyle:BackButtonStyleDark];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

#pragma mark - CustomAccessors

- (void)setTableViewDataSource:(NSMutableArray *)tableViewDataSource
{
    _tableViewDataSource = tableViewDataSource;
    [MBProgressHUD hideHUDForView:self.tableView animated:YES];
    [self updateBottomUIElements];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    return [self viewControllerAtIndex:((CalendarViewController *)pageViewController.viewControllers[0]).dataSourceOffset - 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    return [self viewControllerAtIndex:((CalendarViewController *)pageViewController.viewControllers[0]).dataSourceOffset + 1];
}

- (CalendarViewController *)viewControllerAtIndex:(NSInteger)index
{
    CalendarViewController *pageContent = [self.storyboard instantiateViewControllerWithIdentifier:@"calendarViewController"];
    pageContent.dataSourceOffset = index;
    pageContent.delegate = self;
    
    [self prepareDataSourceWithOffset:index fetchResult:^(NSArray *result) {
        pageContent.calendarData = result;
        if (!index) {
            pageContent.selectedDate = self.actualDateForSessionToDisplay;
        }
    }];
    return pageContent;
}

#pragma mark - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed && previousViewControllers.count) {
        CalendarCell *selectedCell = [((CalendarViewController *)previousViewControllers[0]) cellForRowAtIndexPath:self.selectedDayIndexPath];
        [selectedCell setCellSelected:NO];
        NSInteger selectedDay = [selectedCell.dayNumberLabel.text integerValue];
        CalendarCell *cell = [((CalendarViewController *)self.pageViewController.viewControllers[0]) cellForDayNumber:selectedDay];
        
        if (cell) {
            [cell setSelected:YES];
        }
        
        [self dispalySessionsForSelectedDay];
    }
}

#pragma mark - CalendarViewControllerDelegate

- (void)cleanUpPreviousItems
{
    [self.tableViewDataSource removeAllObjects];
//    [self updateBottomUIElements];
    [self.tableView reloadData];
    [AppHelper showLoaderOnView:self.tableView];
}

- (void)didSelectItemAtIndexPath:(NSIndexPath *)indexPath inCell:(CalendarCell *)cell
{
    [self cleanUpPreviousItems];
    
    CalendarViewController *calendarViewController = (CalendarViewController *)self.pageViewController.viewControllers[0];
    if (self.selectedDayIndexPath) {
        CalendarDay *selectedDay = (CalendarDay *)calendarViewController.calendarData[self.selectedDayIndexPath.row];
        NSDate *cellDate = selectedDay.calendarDayDate;

        CalendarCell *deselectedCell = [((CalendarViewController *)self.pageViewController.viewControllers[0]) cellForRowAtIndexPath:self.selectedDayIndexPath];
        [deselectedCell setCellSelected:NO];
        
        if ([CalendarDataSource isDay:cellDate isSameDayAs:[NSDate date]]) {
            deselectedCell.daySelectionView.backgroundColor = [UIColor blackColor];
            deselectedCell.daySelectionView.hidden = NO;
        }
    }
    self.selectedDayIndexPath = indexPath;
    
    CalendarCell *selectedCell = [((CalendarViewController *)self.pageViewController.viewControllers[0]) cellForRowAtIndexPath:self.selectedDayIndexPath];
    [selectedCell setCellSelected:YES];
    
    CalendarDay *day = (CalendarDay *)((CalendarViewController *)self.pageViewController.viewControllers[0]).calendarData[indexPath.row];
    [self updateDataSourceForDate:day.calendarDayDate];
    [self updateMonthNameWithDate:day.calendarDayDate];
}

- (void)didSelectItemFromNextMonthWithDate:(NSDate *)selectedDate
{
    [self showNextMonthWithSelectedDate:selectedDate];
}

- (void)didSelectItemFromPreviosMonthWithDate:(NSDate *)selectedDate
{
    [self showPreviousMonthWithSelectedDate:selectedDate];
}

- (void)shouldUpdateSelectedIndexPath:(NSIndexPath *)indexPath
{
    self.selectedDayIndexPath = indexPath;
}

- (void)setIndexPathForToday:(NSIndexPath *)indexPath
{
    self.todayIndexPath = indexPath;
    if (!self.selectedDayIndexPath) {
        self.selectedDayIndexPath = indexPath;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableViewDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[EventDescriptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableViewCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventDescriptionTableViewCell *cell = (EventDescriptionTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
}

- (void)configureCell:(EventDescriptionTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Session *sessionForSelectedDay = self.tableViewDataSource[indexPath.row];
    cell.session = sessionForSelectedDay;
}

#pragma mark - IBActions

- (IBAction)showPrevMonthButtonPress:(id)sender
{
    NSInteger currentMonth = ((CalendarViewController *)self.pageViewController.viewControllers[0]).dataSourceOffset;
    NSDate *dateToShow = [self.dataSourceProvider dateFromMonthWithMonthOffsetFromNow:currentMonth - 1];
    self.actualDateForSessionToDisplay = dateToShow;
    [self updateMonthNameWithDate:dateToShow];
    
    [self showPreviousMonthWithSelectedDate:nil];
}

- (IBAction)showNextMonthButtonPress:(id)sender
{
    NSInteger currentMonth = ((CalendarViewController *)self.pageViewController.viewControllers[0]).dataSourceOffset;
    NSDate *dateToShow = [self.dataSourceProvider dateFromMonthWithMonthOffsetFromNow:currentMonth + 1];
    self.actualDateForSessionToDisplay = dateToShow;
    [self updateMonthNameWithDate:dateToShow];
    
    [self showNextMonthWithSelectedDate:nil];
}

- (IBAction)requestButtonPress:(id)sender
{
    UIImage *bluredImage = [[Snapshot screenshot] applyDarkEffect];
    [AppHelper showLoader];
    
    NSInteger trainerID = [ProfileManager sharedManager].profileTrainerID;
    NSInteger clientID = [ProfileManager sharedManager].profileClientID;
    
    [[NetworkManager sharedManager] ptSquaredAPIClientRequestSessionWithTrainerID:trainerID dateTime:self.actualDateForSessionToDisplay operationResult:^(BOOL success, NSError *error, id response) {
        [AppHelper hideLoader];
        NSString *message;
        if (success) {
            message = response;
            [[GoogleAnaliticsManager sharedManager] trackClientSessionRequestWithClientID:clientID];
        } else {
            message = NotificationMessageRequestWasSentWithError;
        }
        [ClientNotificationViewController presentInformationViewControllerFromViewController:self withText:message backgraundImage:bluredImage];
    }];
}

- (void)rightButtonPressed
{
    NSDate *current = [self.actualDateForSessionToDisplay copy];
    
    [self updateDataSourceForDate:[NSDate date]];
    [self updateMonthNameWithDate:[NSDate date]];
    
    CalendarViewController *zeroViewConrolelr = [self viewControllerAtIndex:0];
    
    UIPageViewControllerNavigationDirection direction = UIPageViewControllerNavigationDirectionReverse;
    if ([CalendarDataSource isDate:[NSDate date] latestThanDate:current]) {
        direction = UIPageViewControllerNavigationDirectionForward;
    }
    
    NSInteger currentMonth = [CalendarDataSource monthNumberFromDate:[NSDate date]];
    NSInteger selectedMonth = [CalendarDataSource monthNumberFromDate:current];
    if (currentMonth == selectedMonth) {
        CalendarCell *selectedCell = [((CalendarViewController *)self.pageViewController.viewControllers[0]) cellForRowAtIndexPath:self.todayIndexPath];
        CalendarCell *deselectedCell = [((CalendarViewController *)self.pageViewController.viewControllers[0]) cellForRowAtIndexPath:self.selectedDayIndexPath];
        [deselectedCell setCellSelected:NO];
        [selectedCell setCellSelected:YES];
        self.selectedDayIndexPath = self.todayIndexPath;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [self.pageViewController setViewControllers:@[zeroViewConrolelr] direction:direction animated:YES completion:^(BOOL finished) {
        if (finished) {
            CalendarCell *selectedCell = [zeroViewConrolelr cellForRowAtIndexPath:weakSelf.todayIndexPath];
            [selectedCell setCellSelected:YES];
            weakSelf.selectedDayIndexPath = weakSelf.todayIndexPath;
        }
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"sessionDetails"]){
        SessionDetailsViewController *sessionDetailsViewController = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Session *session = self.tableViewDataSource[indexPath.row];
        sessionDetailsViewController.activeSession = session;
        sessionDetailsViewController.sessionType = session.sessionType;
    }
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.informationView.layer animationForKey:@"hideInformationView"]) {
        [self.informationView.layer removeAllAnimations];
        self.informationView.hidden = YES;
    } else if (anim == [self.sessionView.layer animationForKey:@"hideSessionView"]) {
        [self.sessionView.layer removeAllAnimations];
        self.sessionView.hidden = YES;
    }
}

#pragma mark - Private

- (void)dispalySessionsForSelectedDay
{
    CalendarViewController *calendarViewController = (CalendarViewController *)self.pageViewController.viewControllers[0];
    NSDate *dateToDisplayNameFrom = [self.dataSourceProvider dateFromMonthWithMonthOffsetFromNow:calendarViewController.dataSourceOffset];
    NSArray *currentDataSource = calendarViewController.calendarData;
    [self updateMonthNameWithDate:dateToDisplayNameFrom];
    
    NSDate *dateForRequest = ((CalendarDay *)currentDataSource[self.selectedDayIndexPath.row]).calendarDayDate;
    [self updateDataSourceForDate:dateForRequest];
}

- (void)updateMonthNameWithDate:(NSDate *)date
{
    NSString *monthName = [self.dataSourceProvider monthNameFromDate:date];
    self.monthNameLabel.text = [monthName capitalizedString];
}

- (void)preparePageControl
{
    self.pageViewController = self.childViewControllers[0];
    CalendarViewController *viewControllerStart = [self viewControllerAtIndex:0];
    [self.pageViewController setViewControllers:@[viewControllerStart] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
}

- (void)prepareTableView
{
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)prepareDataSource
{
    if (!self.tableViewDataSource) {
        self.tableViewDataSource = [[NSMutableArray alloc] init];
    }
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
    [self.tableView reloadData];
}

- (void)updateBottomUIElements
{
    if (self.tableViewDataSource.count && self.informationView.layer.opacity) {
        [self.sessionView.layer removeAllAnimations];
        [self.informationView.layer addAnimation:[Animation fadeAnimFromValue:1.0 to:0. delegate:self] forKey:@"hideInformationView"];
        self.informationView.layer.opacity = 0.f;
        self.sessionView.hidden = NO;
        [self.sessionView.layer addAnimation:[Animation fadeAnimFromValue:0.0 to:1. delegate:nil] forKey:nil];
        self.sessionView.layer.opacity = 1.f;
    } else if (!self.tableViewDataSource.count && self.sessionView.layer.opacity) {
        [self.informationView.layer removeAllAnimations];
        [self.sessionView.layer addAnimation:[Animation fadeAnimFromValue:1.0 to:0. delegate:self] forKey:@"hideSessionView"];
        self.sessionView.layer.opacity = 0.f;
        self.informationView.hidden = NO;
        [self.informationView.layer addAnimation:[Animation fadeAnimFromValue:0.0 to:1. delegate:nil] forKey:nil];
        self.informationView.layer.opacity = 1.f;
    }
}

- (void)showNextMonthWithSelectedDate:(NSDate *)selectedDate
{
    NSInteger index = ((CalendarViewController *)self.pageViewController.viewControllers[0]).dataSourceOffset + 1;
    CalendarViewController *nextViewController = [self viewControllerAtIndex:index];
    if (selectedDate) {
        nextViewController.selectedDate = selectedDate;
    }
    
    __weak typeof(self) weakSelf = self;
    [self.pageViewController setViewControllers:@[nextViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
        CalendarCell *selectedCell = [nextViewController cellForRowAtIndexPath:weakSelf.selectedDayIndexPath];
        [selectedCell setCellSelected:YES];
    }];
}

- (void)showPreviousMonthWithSelectedDate:(NSDate *)selectedDate
{
    NSInteger index = ((CalendarViewController *)self.pageViewController.viewControllers[0]).dataSourceOffset - 1;
    CalendarViewController *nextViewController = [self viewControllerAtIndex:index];
    if (selectedDate) {
        nextViewController.selectedDate = selectedDate;
    }
    
    __weak typeof(self) weakSelf = self;
    [self.pageViewController setViewControllers:@[nextViewController] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:^(BOOL finished) {
        CalendarCell *selectedCell = [nextViewController cellForRowAtIndexPath:weakSelf.selectedDayIndexPath];
        [selectedCell setCellSelected:YES];
    }];
}

- (NSDate *)date:(NSDate *)date withDay:(NSInteger)day
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:date];
    [dateComponents setDay:day];
    return [gregorian dateFromComponents:dateComponents];
}

#pragma mark - DataSource

- (void)prepareDataSourceWithOffset:(NSInteger)offset fetchResult:(void(^)(NSArray *result))fetchRequestResult
{
    if (!self.dataSourceProvider) {
        self.dataSourceProvider = [[CalendarDataSource alloc] init];
    }
    __weak typeof(self) weakSelf = self;
    void (^PerformDataSourcePreparation)(NSArray *daysForMonth) = ^(NSArray *daysForMonth) {
       NSArray *dataSource = [weakSelf.dataSourceProvider daysForMonthWithOffsetFromNow:offset sessionsDays:daysForMonth];
        fetchRequestResult(dataSource);
        
        if (self.pageViewController.viewControllers.count) {
            CalendarCell *selectedCell = [((CalendarViewController *)self.pageViewController.viewControllers[0]) cellForRowAtIndexPath:weakSelf.selectedDayIndexPath];
            [selectedCell setCellSelected:YES];
        }
    };
    
    PerformDataSourcePreparation(@[]); //offline generating calendar
    
    NSDate *newDateOfMonth = [self dateForRequestWithOffset:offset];
    [[NetworkManager sharedManager] ptSquaredAPIClientGetSessionInPeriodFrom:newDateOfMonth daysCount:43 operationResult:^(BOOL success, NSError *error, NSArray *response) {
        if (success) {
            [self performSyncOfEventsWithStartDate:newDateOfMonth andEvents:response];
            PerformDataSourcePreparation(response);
        }
    }];
}

- (NSDate *)dateForRequestWithOffset:(NSInteger)offset
{
    NSDate *dateForRequest = [self.dataSourceProvider dateFromMonthWithMonthOffsetFromNow:offset];
    NSDateComponents *comps = [[[NSLocale currentLocale] objectForKey:NSLocaleCalendar] components:(NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth)  fromDate:dateForRequest];
    NSInteger weekDay = [comps weekday];
    
    NSInteger offcet = -(weekDay - 2);
    NSDateComponents *dt = [[NSDateComponents alloc] init];
    [dt setDay:offcet];
    [dt setWeekday:1];
    [dt setHour:[self.dataSourceProvider getGMTIntervalFromDate:dateForRequest]];
    [dt setMonth:[comps month]];
    [dt setYear:[comps year]];
    
    NSDate *newDateOfMonth = [[[NSLocale currentLocale] objectForKey:NSLocaleCalendar]  dateFromComponents:dt];
    return newDateOfMonth;
}

- (void)updateDataSourceForDate:(NSDate *)date
{
    NSString *stringDate = [CalendarDataSource stringFromDateInRequestFormat:date];
    NSDate *updatedDate = [CalendarDataSource zeroHourDateFromString:stringDate];

    self.actualDateForSessionToDisplay = updatedDate;
    
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMTForDate:date];
    NSDate *dateInUTC = [date dateByAddingTimeInterval:timeZoneSeconds];

    __weak typeof(self) weakSelf = self;
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:-1];
    NSDate *prevDay = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:dateInUTC options:0];

    //taken 3 day due to server response - not return day if 00:00 set in session
    [[NetworkManager sharedManager] ptSquaredAPIClientGetSessionInPeriodFrom:prevDay daysCount:3 operationResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            if ([weakSelf.actualDateForSessionToDisplay isEqualToDate:updatedDate]) {
                weakSelf.tableViewDataSource = [[weakSelf verificateSessionWithArray:response withDate:dateInUTC] mutableCopy];
                [weakSelf.tableView reloadData];
            }
        }
    }];
}

- (NSArray *)verificateSessionWithArray:(NSArray *)input withDate:(NSDate *)requestedDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    
    NSMutableArray *sessions = [[NSMutableArray alloc] init];
    for (Session *session in input) {
        NSString *sessionDate = [dateFormatter stringFromDate:[session.sessionDate dateByAddingTimeInterval:timeZoneSeconds]];
        sessionDate = [[sessionDate componentsSeparatedByString:@" "] firstObject];
        
        NSString *requestDate = [dateFormatter stringFromDate:requestedDate];
        requestDate = [[requestDate componentsSeparatedByString:@" "] firstObject];
        
        if ([sessionDate isEqualToString:requestDate]) {
            [sessions addObject:session];
        }
    }
    return sessions;
}

#pragma mark - Sync Calendar

- (void)performSyncOfEventsWithStartDate:(NSDate *)startDate andEvents:(NSArray *)events
{
    NSDate *endDate = [startDate dateByAddingTimeInterval:(42 * 24 * 60 * 60)]; //days * hours * min * sec
    [[CalendarEventSynchronizer sharedInstance] synchronizeCalendarEventsWithSessions:events inRangeFromDate:startDate toDate:endDate calendarType:CalendarSynchronizerTypeClient];
}

@end
