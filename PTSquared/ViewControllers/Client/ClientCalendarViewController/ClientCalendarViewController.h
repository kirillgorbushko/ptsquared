//
//  CalendarViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 24.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "CalendarViewController.h"

@interface ClientCalendarViewController : BaseDynamicColorViewController  <UIPageViewControllerDataSource, UIPageViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, CalendarViewControllerDelegate>

@end
