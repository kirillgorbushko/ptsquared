//
//  PTMenuViewController.m
//  PTSquared
//
//  Created by Stas Volskyi on 10.11.14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuTableViewCell.h"
#import <sys/utsname.h>

static NSString *const PTMCellIdentifier = @"menuCell";
static NSString *const PTMMenuItemName = @"itemName";
static NSString *const PTMMenuImageName = @"imageName";

static CGFloat const DefaultRowHeight = 83;
static CGFloat const RowHeightIphone4 = 65;

@interface MenuViewController () 

@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;

@property (strong, nonatomic) NSArray *menuItems;

@end

@implementation MenuViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureDataSource];
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
    self.menuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PTMCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:PTMCellIdentifier];
    }
    cell.menuTitleUILabel.text = [(NSDictionary *)self.menuItems[indexPath.row] valueForKey:PTMMenuItemName];
    NSString *imageName = [(NSDictionary *)self.menuItems[indexPath.row] valueForKey:PTMMenuImageName];
    cell.menuItemLogoUIImageView.image = [UIImage imageNamed:imageName];

    if (indexPath.row == (int)(self.menuItems.count - 1)) {
        cell.separatorView.hidden = YES;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuTableViewCell *cell = (MenuTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    [self.delegate selectMenuItem:indexPath.row];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *mashineName = [self machineName];
    if ([mashineName isEqualToString:@"iPhone3,1"] || [mashineName isEqualToString:@"iPhone3,3"]  || [mashineName isEqualToString:@"iPhone3,2"] || [mashineName isEqualToString:@"iPhone4,1"]) {
        return RowHeightIphone4;
    } else {
        return DefaultRowHeight;
    }
}

#pragma mark - Private

- (void)configureDataSource
{
    if (!self.menuItems) {
        self.menuItems = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MenuItems" ofType:@"plist"]];
    }
}

- (NSString *)machineName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
    [self.menuTableView reloadData];
}

@end
