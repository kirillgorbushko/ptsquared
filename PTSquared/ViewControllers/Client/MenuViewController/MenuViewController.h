//
//  PTMenuViewController.h
//  PTSquared
//
//  Created by Stas Volskyi on 10.11.14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

@protocol MenuViewControllerDelegate

@required
- (void)selectMenuItem:(NSInteger)controllerIndex;

@end

@interface MenuViewController : BaseDynamicColorViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) id <MenuViewControllerDelegate>delegate;

@end
