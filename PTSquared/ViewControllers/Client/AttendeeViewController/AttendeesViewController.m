//
//  AttendeesViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AttendeesViewController.h"
#import "AttendeesTableViewCell.h"
#import "Animation.h"
#import "UIImage+ImageEffects.h"
#import "NetworkManager.h"
#import "ClientProfile.h"

static NSString *const AttendeesTableViewCellIdentifier = @"attendeeCell";
static CGFloat const AttendeesCellHeight = 52;

@interface AttendeesViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (strong, nonatomic) NSArray *dataSource;

@end

@implementation AttendeesViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
    self.backgroundImageView.image = [self.backgroundImage applyDarkEffect];
#ifdef HIDE_STATUS_BAR_ON_INFO
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
#endif
    
    [self receiveAttendeesList];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
#ifdef HIDE_STATUS_BAR_ON_INFO
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
#endif
}

#pragma mark - IBActions

- (IBAction)closeButtonPressed:(id)sender
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"fadeAnimation"];
    self.view.layer.opacity = 0.f;
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"fadeAnimation"]) {
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AttendeesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AttendeesTableViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[AttendeesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AttendeesTableViewCellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return AttendeesCellHeight;
}

- (void)configureCell:(AttendeesTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    ClientProfile *attendee = self.dataSource[indexPath.row];
    NSString *imagePath = attendee.clientAvatar.avatarImageUri;
    
    if (imagePath.length) {
        [[NetworkManager sharedManager] ptSquaredAPIGetImageWithPath:imagePath withCompletition:^(BOOL success, UIImage *response) {
            if (success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.photoImageView.image = response;
                });
            }
        }];
    }
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", attendee.clientFirstName, attendee.clientLastName];
}


#pragma mark - Private Methods

- (void)receiveAttendeesList
{
    __weak __typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIClientGetSessionAttendeeForSessionID:self.sessionID operationresult:^(BOOL success, NSError *error, id response) {
        if (success) {
            weakSelf.dataSource = response;
            [weakSelf.tableView reloadData];
        } else {
            //TODO
        }
    }];
}

@end
