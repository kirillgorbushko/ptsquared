//
//  AttendeesViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface AttendeesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (assign, nonatomic) NSInteger sessionID;
@property (strong, nonatomic) UIImage *backgroundImage;

@end
