//
//  SwitchViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 10.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "SwitchViewController.h"
#import "AppDelegate.h"

@interface SwitchViewController ()

@end

@implementation SwitchViewController

#pragma mark - LIfeCycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (![ProfileManager sharedManager].profileType) {
        [((AppDelegate *)[UIApplication sharedApplication].delegate) showLoginViewController];
    }
}

@end