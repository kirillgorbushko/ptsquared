//
//  TrainerAttendeesViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerScanQRCodeViewController.h"

@class Session;

@interface TrainerAttendeesViewController : BaseDynamicColorViewController <TrainerScanQRCodeViewControllerDelegate>

@property (strong, nonatomic) Session *session;

@end
