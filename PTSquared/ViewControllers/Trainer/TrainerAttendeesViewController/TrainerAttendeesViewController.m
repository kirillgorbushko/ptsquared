//
//  TrainerAttendeesViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerAttendeesViewController.h"
#import "TrainerWorkoutDetailsViewController.h"
#import "TrainerNavigationController.h"
#import "UINavigationController+Addition.h"
#import "TrainerAttendeeTableViewCell.h"
#import "TrainerAddAttendeeViewController.h"
#import "TrainerScanQRCodeViewController.h"
#import "TrainerMembershipDetailsViewController.h"
#import "Snapshot.h"
#import "ProfileManager.h"
#import "Session.h"
#import "ClientProfile.h"
#import "CalendarDataSource.h"

static NSString *const TrainerAttendeeCellIdentifier = @"trainerAttendee";
static CGFloat const TrainerAttendeeCellHeight = 52;

static NSString *const SessionTitleIndividual = @"Individual";
static NSString *const SessionTitlePublic = @"Public";

@interface TrainerAttendeesViewController () <TrainerMembershipDetailsDelegate, AddAttendeeDelegate>

@property (weak, nonatomic) IBOutlet UILabel *sesssionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionBeginTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *dataSource;
@property (copy, nonatomic) NSString *planName;

@end

@implementation TrainerAttendeesViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareNavigationController];
    [self receiveAttendees];
}

#pragma mark - IBActions

- (void)rightButtonPressed
{
    [((TrainerNavigationController *)self.navigationController) logout];
}

- (IBAction)scanMembershipCodeButtonPressed:(id)sender
{
    TrainerScanQRCodeViewController *scanMembershipCodeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"scanQRCodeVC"];
    scanMembershipCodeViewController.activeSession = self.session;
    scanMembershipCodeViewController.delegate = self;
    scanMembershipCodeViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
    scanMembershipCodeViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.navigationController presentViewController:scanMembershipCodeViewController animated:NO completion:nil];
}

- (IBAction)addAttendeeButtonPressed:(id)sender
{
    TrainerAddAttendeeViewController *addAttendeeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"addAttendee"];
    UIImage *screenShot = [Snapshot screenshot];
    addAttendeeViewController.delegate = self;
    addAttendeeViewController.sessionID = self.session.sessionId;
    addAttendeeViewController.activeSession = self.session;
    addAttendeeViewController.screenShotForBlur = screenShot;
    addAttendeeViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
    addAttendeeViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.navigationController presentViewController:addAttendeeViewController animated:NO completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TrainerAttendeeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TrainerAttendeeCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[TrainerAttendeeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TrainerAttendeeCellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TrainerAttendeeCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TrainerMembershipDetailsViewController *membershipDatailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"membershipDetails"];
    UIImage *screenShot = [Snapshot screenshot];
    membershipDatailsViewController.delegate = self;
    membershipDatailsViewController.screenShotForBlur = screenShot;
    membershipDatailsViewController.attendee = self.dataSource[indexPath.row];
    membershipDatailsViewController.activeSession = self.session;
    
    if ([self.session.sessionType isEqualToString:@"group"]) {
        membershipDatailsViewController.sessionType = SessionTypeGroup;
    } else {
        membershipDatailsViewController.sessionType = SessionTypeIndividual;
    }
    
    membershipDatailsViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
    membershipDatailsViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.navigationController presentViewController:membershipDatailsViewController animated:NO completion:nil];
}

- (void)configureCell:(TrainerAttendeeTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.attendee = self.dataSource[indexPath.row];
}

#pragma mark - TrainerMembershipDetailsDelegate

- (void)attendeeAddedToSessionSuccessfully
{
    [self receiveAttendees];
}

#pragma mark - AddAttendeeDelegate

- (void)addedAttendeeSuccesful
{
    [self receiveAttendees];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"details"]) {
        TrainerWorkoutDetailsViewController *viewController = segue.destinationViewController;
        viewController.sessionID = self.session.sessionId;
    }
}

#pragma mark - TrainerScanQRCodeViewControllerDelegate

- (void)willDismissQRCodeViewController
{
    [self receiveAttendees];
}

#pragma mark - Custom Accessors

- (void)setDataSource:(NSArray *)dataSource
{
    _dataSource = dataSource;
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Private

- (void)prepareNavigationController
{
    NSString *type;
    
    if ([[self.session.sessionType capitalizedString] isEqualToString:SessionTitleIndividual]) {
        type = SessionTitleIndividual;
    }  else if (self.session.sessionIsPublic) {
        type = SessionTitlePublic;
    } else {
        type = [self.session.sessionType capitalizedString];
    }
    
    NSString *title = [NSString stringWithFormat:@"%@ Session", type];
    [((TrainerNavigationController *)self.navigationController) setTitleText:title];
    [self.navigationController setRightButtonWithImageNamed:@"Logout_Icon" andDelegate:self];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
}

- (void)receiveAttendees
{
    [AppHelper showLoader];
    [[NetworkManager sharedManager] ptSquaredAPITrainerGetSessionAttendeeForSessionID:self.session.sessionId operationresult:^(BOOL success, NSError *error, id response) {
        [AppHelper hideLoader];
        if (success) {
            self.dataSource = response;
        }
    }];
}

- (void)prepareUI
{
    
    self.sesssionNameLabel.text = self.session.sessionName;
    self.sessionBeginTimeLabel.text = [CalendarDataSource timeFromDate:self.session.sessionDate];;
}

@end
