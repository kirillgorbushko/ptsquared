//
//  TrainerCompleteSessionViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 04.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class Session;

@protocol TrainerCompleteSessionDelegate <NSObject>

- (void)sessionCompletedSuccessfully;

@end

@interface TrainerCompleteSessionViewController : UIViewController <UIAlertViewDelegate>

@property (strong, nonatomic) UIImage *screenShotForBlur;
@property (assign, nonatomic) NSInteger sessionID;

@property (strong, nonatomic) Session *activeSession;

@property (weak, nonatomic) id<TrainerCompleteSessionDelegate>delegate;

@end
