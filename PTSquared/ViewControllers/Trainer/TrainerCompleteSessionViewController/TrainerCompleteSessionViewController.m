//
//  TrainerCompleteSessionViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 04.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerCompleteSessionViewController.h"
#import "Animation.h"
#import "UIImage+ImageEffects.h"
#import "GoogleAnaliticsManager.h"
#import "CalendarDataSource.h"
#import "Session.h"

@interface TrainerCompleteSessionViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end

@implementation TrainerCompleteSessionViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [AppHelper prepareInformationButtons:@[self.completeButton, self.cancelButton]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.backgroundImageView.image = [self.screenShotForBlur applyDarkEffect];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

#pragma mark - IBActions

- (IBAction)closeButtonPress:(id)sender
{
    [self addAnimationWithKey:@"fadeAnimation"];
}

- (IBAction)completeSessionButtonPressed:(id)sender
{
    [AppHelper showLoader];
    __weak typeof(self) weakSelf = self;
    NSString *stringDate = [CalendarDataSource stringZeroInRequestFormat:self.activeSession.sessionDate];
    [[NetworkManager sharedManager] ptSquaredAPITrainerSetSessionCompletedForSessionID:self.activeSession.sessionId parentID:self.activeSession.sessionParentID sessionTime:stringDate reoccurence:self.activeSession.sessionReoccurence operationResult:^(BOOL success, NSError *error, id response) {
        [AppHelper hideLoader];
        if (success) {
            [AppHelper alertViewWithMessage:response delegate:weakSelf];
            [weakSelf trackScannedQRCodes];
        } else {
            [AppHelper alertViewWithMessage:NotificationMessageCompleteSessionRequestError];
        }
    }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self addAnimationWithKey:@"fadeAnimationCompleteSession"];
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"fadeAnimation"]) {
        [self dismissViewController];
    } else if (anim == [self.view.layer animationForKey:@"fadeAnimationCompleteSession"]) {
        [self dismissViewController];
        if ([self.delegate respondsToSelector:@selector(sessionCompletedSuccessfully)]) {
            [self.delegate performSelector:@selector(sessionCompletedSuccessfully)];
        }
    }
}

#pragma mark - Private

- (void)dismissViewController
{
    [self.view.layer removeAllAnimations];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)addAnimationWithKey:(NSString *)key
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:key];
    self.view.layer.opacity = 0.f;
}

- (void)trackScannedQRCodes
{
    NSString *key = [NSString stringWithFormat:@"%@ _ %i _ session", KeyCalcSessionScan, (int)self.activeSession.sessionId];
    NSInteger scannedQRQty = [[[NSUserDefaults standardUserDefaults] valueForKey:key] integerValue];
    if (scannedQRQty) {
        [[GoogleAnaliticsManager sharedManager] trackTrainerQRCodeScanForSessionID:self.activeSession.sessionId scannedQRCodeTimes:scannedQRQty];
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
