//
//  TrainerScanQRCodeViewController
//  PTSquared
//
//  Created by Kirill Gorbushko on 04.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "QRCodeReader.h"

@class Session;

@protocol TrainerScanQRCodeViewControllerDelegate <NSObject>

@required
- (void)willDismissQRCodeViewController;

@end

@interface TrainerScanQRCodeViewController : BaseDynamicColorViewController <QRCodeReaderDelagte, UIAlertViewDelegate>

@property (weak, nonatomic) id <TrainerScanQRCodeViewControllerDelegate> delegate;
@property (strong, nonatomic) Session *activeSession;

@end
