//
//  TrainerScanQRCodeViewController
//  PTSquared
//
//  Created by Kirill Gorbushko on 04.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerScanQRCodeViewController.h"
#import "Animation.h"
#import "Session.h"
#import "ProfileManager.h"
#import "CalendarDataSource.h"
#import "GoogleAnaliticsManager.h"

typedef NS_ENUM(NSUInteger, ScannedStatus) {
    ScannedStatusAlert,
    ScannedStatusOK,
    ScannedStatusUndefined
};

typedef NS_ENUM(NSUInteger, ScanButtonState) {
    ScanButtonStateStart,
    ScanButtonStateFinish
};

static NSString *const TickIconName = @"Tick_Small_Icon";
static NSString *const AlertIconName = @"Alert_Small_Icon";

@interface TrainerScanQRCodeViewController ()

@property (weak, nonatomic) IBOutlet UIView *previewBarcodeView;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UILabel *participantName;
@property (weak, nonatomic) IBOutlet UIButton *startStopScanningButton;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;

@property (assign, nonatomic) ScanButtonState state;
@property (strong, nonatomic) QRCodeReader *barcodeReader;
@property (strong, nonatomic) NSString *previousAttendee;

@end

@implementation TrainerScanQRCodeViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([QRCodeReader isDeviceHasBackCamera]) {
        __weak typeof(self) weakSelf = self;
        [QRCodeReader checkPermissionForCamera:^(BOOL status) {
            if (status) {
                [weakSelf prepareBarcodeRader];
                [weakSelf.barcodeReader startReading];
            } else {
                [AppHelper alertViewWithMessage:AlertMEssageNoCameraAccessGranted delegate:weakSelf otherButtonTitles:@"Go to settings"];
            }
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
}

#pragma mark - IBActions

- (IBAction)closeButtonPress:(id)sender
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"fadeAnimation"];
    self.view.layer.opacity = 0.f;
}

- (IBAction)scanningButtonPress:(id)sender
{
    [self.barcodeReader stopReading];
    [self closeButtonPress:nil];
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"fadeAnimation"]) {
        [self.view.layer removeAllAnimations];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(willDismissQRCodeViewController)]) {
            [self.delegate willDismissQRCodeViewController];
        }
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex) {
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    } else {
        [self closeButtonPress:self];
    }
}

#pragma mark - BarcodeReaderDelegate

- (void)readerDidFinishCapturingWithResult:(NSString *)result
{
    if ([self.previousAttendee isEqualToString:result]) {
        return;
    } else {
        self.previousAttendee = result;
    }
    
    __weak typeof(self) weakSelf = self;
    [AppHelper showLoader];
    NSString *stingDate = [CalendarDataSource stringZeroInRequestFormat:self.activeSession.sessionDate];
    [[NetworkManager sharedManager] ptSquaredAPITrainerGetPaidStatusFromQRCodeFromSessionID:self.activeSession.sessionId parentID:self.activeSession.sessionParentID sessionTime:stingDate reoccurence:self.activeSession.sessionReoccurence clientQrCode:result operationResult:^(BOOL success, NSError *error, id response) {

        [AppHelper hideLoader];
        if (success) {
            NSString *statusResult = [response valueForKey:@"status"];
            if ([statusResult isEqualToString:@"paid"]) {
                [weakSelf setStatusForScanning:ScannedStatusOK];
            } else if ([statusResult isEqualToString:@"unpaid"]) {
                [weakSelf setStatusForScanning:ScannedStatusAlert];
            }
            NSString *clientName = [NSString stringWithFormat:@"%@ %@", [response valueForKey:@"client_first_name"], [response valueForKey:@"client_last_name"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.participantName.text = clientName;
            });
            [self addScannCountToSession];
        } else {
            [weakSelf setStatusForScanning:ScannedStatusAlert];
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.participantName.text = response;
            });
        }
    }];
}

#pragma mark - Private

- (void)prepareBarcodeRader
{
    self.barcodeReader = [[QRCodeReader alloc] initWithView:self.previewBarcodeView];
    self.barcodeReader.delegate = self;
}

- (void)setStatusForScanning:(ScannedStatus)scannedStatus
{
    UIImage *fileName = [[UIImage alloc] init];
    switch (scannedStatus) {
        case ScannedStatusAlert: {
            fileName = [UIImage imageNamed:AlertIconName];
            break;
        }
        case ScannedStatusOK:{
            fileName = [UIImage imageNamed:TickIconName];
            break;
        }
        case ScannedStatusUndefined: {
            break;
        }
    }
    self.statusImageView.image = fileName;
}

- (void)addScannCountToSession
{
    NSString *key = [NSString stringWithFormat:@"%@ _ %i _ session", KeyCalcSessionScan, (int)self.activeSession.sessionId];
    NSInteger count = 1;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:key]) {
        count = [[[NSUserDefaults standardUserDefaults] valueForKey:key] integerValue];
        count++;
    }
    [[NSUserDefaults standardUserDefaults] setValue:@(count) forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
}

@end
