//
//  TrainerSessionViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 30.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerSessionViewController.h"
#import "TrainerNavigationController.h"
#import "UINavigationController+Addition.h"
#import "GoogleMapsProvider.h"
#import "TrainerWorkoutDetailsViewController.h"
#import "TrainerAttendeesViewController.h"
#import "TrainerCompleteSessionViewController.h"
#import "Snapshot.h"
#import "Session.h"
#import "CalendarDataSource.h"

static NSString *const SessionTitleGroup = @"Group Session";
static NSString *const SessionTitlePublic = @"Public Session";
static NSString *const SessionTitleIndividual = @"Individual Session";

static NSString *const SessionTypeIndividual = @"individual";

@interface TrainerSessionViewController () <TrainerCompleteSessionDelegate>

@property (weak, nonatomic) IBOutlet UILabel *sessionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionStartTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionPriceLabel;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;
@property (weak, nonatomic) IBOutlet UIButton *sessionCompleteSessionButton;

@property (strong, nonatomic) __block Session *selectedSession;

@end

@implementation TrainerSessionViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self fetchSessionData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self prepareNavigationController];
}

#pragma mark - IBAction

- (void)rightButtonPressed
{
    [((TrainerNavigationController *)self.navigationController) logout];
}

- (IBAction)completeSessionButtonPressed:(id)sender
{
    TrainerCompleteSessionViewController *completeSessionViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"completeSession"];
    completeSessionViewController.delegate = self;
    completeSessionViewController.activeSession = self.selectedSession;
    completeSessionViewController.screenShotForBlur = [Snapshot screenshot];
    completeSessionViewController.sessionID = self.selectedSession.sessionId;
    completeSessionViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
    completeSessionViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.navigationController presentViewController:completeSessionViewController animated:NO completion:nil];
}

#pragma mark - GoogleMap

- (void)prepareGoogleMapView
{
    [GoogleMapsProvider setupMapOnView:self.mapView setMarkerOnLatitude:self.selectedSession.sessionLatitude andLongtitude:self.selectedSession.sessionLongtitude];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"workourDetails"]) {
        TrainerWorkoutDetailsViewController *viewController = segue.destinationViewController;
        viewController.sessionID = self.selectedSession.sessionId;
    } else if ([segue.identifier isEqualToString:@"trainerAttendees"]){
        TrainerAttendeesViewController *viewController = segue.destinationViewController;
        viewController.session = self.selectedSession;
    }
}

#pragma mark - TrainerCompleteSessionDelegate

- (void)sessionCompletedSuccessfully
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private

- (void)fetchSessionData
{
    __weak typeof(self) weakSelf = self;
    [AppHelper showLoaderOnView:[AppHelper topView]];
    [[NetworkManager sharedManager] ptSquaredAPITrainerGetSessionDetailsForSessionID:self.session.sessionId  sessionDate:self.session.sessionDate  operationresult:^(BOOL success, NSError *error, id response) {
        if (success) {
            weakSelf.selectedSession = response;
            [weakSelf prepareGoogleMapView];
            [weakSelf updateUI];
        } else {
            [AppHelper alertViewWithMessage:NotificationMessageRequestWasSentWithError delegate:weakSelf];
        }
        [AppHelper hideLoader];
    }];
}

- (void)prepareNavigationController
{
    NSString *title;
    
    if ([self.sessionType isEqualToString:SessionTypeIndividual]) {
        title = SessionTitleIndividual;
    } else if (self.isPublic) {
        title = SessionTitlePublic;
    } else {
        title = SessionTitleGroup;
    }
    
    [((TrainerNavigationController *)self.navigationController) setTitleText:title];
    [self.navigationController setRightButtonWithImageNamed:@"Logout_Icon" andDelegate:self];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
}

- (void)updateUI
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.sessionNameLabel.text = weakSelf.selectedSession.sessionName;
        weakSelf.sessionStartTimeLabel.text = [CalendarDataSource timeFromDate:weakSelf.selectedSession.sessionDate];
        weakSelf.sessionAddressLabel.text = weakSelf.selectedSession.sessionAddress;
        weakSelf.sessionPriceLabel.text = [NSString stringWithFormat:@"$%li", (long)weakSelf.selectedSession.sessionPrice];
        if (weakSelf.selectedSession.sessionIsComplete) {
            [weakSelf.sessionCompleteSessionButton setTitle:@"Completed" forState:UIControlStateNormal];
            weakSelf.sessionCompleteSessionButton.enabled = NO;
            weakSelf.sessionCompleteSessionButton.alpha = 0.7f;
        }
    });
}

@end
