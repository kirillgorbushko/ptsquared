//
//  TrainerSessionViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 30.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Session.h"

@interface TrainerSessionViewController : BaseDynamicColorViewController <UIAlertViewDelegate>

@property (strong, nonatomic) NSString *sessionType;
@property (assign, nonatomic) BOOL isPublic;
@property (strong, nonatomic) Session *session;

@end
