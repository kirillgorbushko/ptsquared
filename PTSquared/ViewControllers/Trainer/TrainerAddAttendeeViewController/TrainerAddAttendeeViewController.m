//
//  TrainerAddAttendeeViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerAddAttendeeViewController.h"
#import "Animation.h"
#import "UIImage+ImageEffects.h"
#import "LeftImageViewTextField.h"
#import "TextFieldNavigator.h"
#import "UITextField+Validation.h"
#import "UserTableViewCell.h"
#import "ClientProfile.h"
#import "CalendarDataSource.h"
#import "Session.h"

static CGFloat const DefaultCellHeight = 53.f;
static CGFloat const DefaultHeaderHeight = 24.f;

@interface TrainerAddAttendeeViewController () <UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate, LeftImageViewTextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundBlureImageView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *emailTextField;
@property (weak, nonatomic) IBOutlet LeftImageViewTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTextFieldTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightOfContentConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *cancellButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *searchResults;
@property (strong, nonatomic) ClientProfile *selectedUser;

@property (assign, nonatomic) BOOL isKeyboardVisible;
@property (strong, nonatomic) UITextField *focusedTextField;

@end

@implementation TrainerAddAttendeeViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareTextFields];
    [self prepareUI];
    [AppHelper prepareInformationButtons:@[self.addButton, self.cancellButton]];
    [self addSwipeGestureRecognizerForScrollView];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.phoneNumberTextField.subDelegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateUI];
    [self setupNotification];
    
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self removeNotification];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:UserTableViewCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    tableView.separatorStyle = self.searchResults.count ? UITableViewCellSeparatorStyleSingleLine : UITableViewCellSeparatorStyleNone;
    return self.searchResults.count;
}

- (void)configureCell:(UserTableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    cell.user = self.searchResults[indexPath.row];
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [UIView new];
    header.backgroundColor = [UIColor clearColor];
    return header;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedUser = self.searchResults[indexPath.row];
//    self.searchTextField.text = @"";
//    [self fillAttendee:self.selectedUser];
//    [self.view endEditing:YES];
//    [self hideSearchResults];
    
    [self addSelectedAttendee];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return DefaultCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return DefaultHeaderHeight;
}

#pragma mark - IBActions

- (IBAction)closeButtonPress:(id)sender
{ 
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"fadeAnimation"];
    self.view.layer.opacity = 0.f;
}

- (IBAction)addButtonPressed:(id)sender
{
    if (self.selectedUser && [self.emailTextField.text isEqualToString:self.selectedUser.clientEmail]) {
        [self addSelectedAttendee];
    } else {
        [self addNewUser];
    }
}

- (IBAction)phoneNumberTextFieldTextDidChanged:(UITextField *)sender
{
    if (sender.text.length == 1) {
        if ([sender.text rangeOfString:@"+"].location == NSNotFound) {
            NSString *textToShow = [@"+" stringByAppendingString:sender.text];
            sender.text = textToShow;
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.returnKeyType == UIReturnKeySearch) {
        [self searchClientWithName:textField.text];
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.scrollView.contentOffset = CGPointZero;
        }];
        self.isKeyboardVisible = NO;
    }
    
    if (!textField.tag) {
        [textField endEditing:YES];
        self.isKeyboardVisible = NO;
        return YES;
    }
    [TextFieldNavigator findNextTextFieldFromCurrent:textField];
    if (textField.returnKeyType == UIReturnKeyDone) {
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.scrollView.contentOffset = CGPointZero;
        }];
        self.isKeyboardVisible = NO;
        return YES;
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (!textField.tag && range.location) {
        NSMutableString *clientName = [NSMutableString stringWithString:textField.text];
        [clientName appendString:string];
        [self searchClientWithName:clientName];
    } else if (!textField.tag && !range.location) {
        [self hideSearchResults];
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.focusedTextField = textField;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.focusedTextField = nil;
    return YES;
}

#pragma mark - LeftImageViewTextFieldDelegate

- (void)textFieldDidDelete:(UITextField *)textField
{
    if (textField.text.length == 1) {
        textField.text = @"";
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self closeButtonPress:self];
}

#pragma mark - TextFieldValidation

- (IBAction)textFieldDidChangeTextEvent:(UITextField *)sender
{
    [sender textFieldDidChangedText];
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"fadeAnimation"]) {
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
        if ([self.delegate respondsToSelector:@selector(addedAttendeeSuccesful)]) {
            [self clearAllFields];
            [self.delegate performSelector:@selector(addedAttendeeSuccesful)];
        }
    }
}

#pragma mark - Keyboard

- (void)setupNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)removeNotification
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    } @catch (NSException *ex){
        NSLog(@"Cant remove observer for keyboard - %@",ex.description);
    }
}

- (void)keyboardOnScreen:(NSNotification *)notification
{
    self.isKeyboardVisible = YES;
    if (self.focusedTextField.frame.origin.y >= 100) {
        self.scrollView.contentOffset = CGPointMake(0, 100);
    }
}

#pragma mark - Private

- (void)prepareUI
{
    self.tableView.hidden = YES;
    self.containerView.hidden = NO;
}

- (void)updateUI
{
    CGFloat screenHeight =  [UIScreen mainScreen].bounds.size.height;
    self.heightOfContentConstraint.constant = screenHeight;
    if (IS_IPHONE_5) {
        self.searchTextFieldTopConstraint.constant /= 2;
    } else if (IS_IPHONE_4_OR_LESS) {
        self.heightOfContentConstraint.constant = 568;
    }
    self.backgroundBlureImageView.image = [self.screenShotForBlur applyDarkEffect];
}

- (void)prepareTextFields
{
    for (UIView *view in self.containerView.subviews) {
        if ([view isKindOfClass:[LeftImageViewTextField class]]) {
            [LeftImageViewTextField setupTextField:((LeftImageViewTextField *)view)];
            [((LeftImageViewTextField *)view) setTintColor:[UIColor whiteColor]];
        }
    }
    [self.searchTextField setTintColor:[UIColor whiteColor]];
}

- (void)addSwipeGestureRecognizerForScrollView
{
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDown)];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.scrollView addGestureRecognizer:swipeRecognizer];
}

- (void)swipeDown
{
    if (self.isKeyboardVisible) {
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            [weakSelf.view endEditing:YES];
            weakSelf.scrollView.contentOffset = CGPointZero;
        }];
        self.isKeyboardVisible = NO;
    }
}

- (void)searchClientWithName:(NSString *)name
{
    __weak typeof(self) weakSelf = self;
    void (^showSearchResults)() = ^() {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf showSearchResults];
        });
    };
    
    void (^hideSearchResults)() = ^() {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideSearchResults];
        });
    };
    
    [[NetworkManager sharedManager] ptSquaredAPITrainerSearchClientWithName:name count:50 offset:0 searchResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            weakSelf.searchResults = response;
            if (!weakSelf.searchResults.count) {
                hideSearchResults();
            } else {
                showSearchResults();
            }
        } else {
            [AppHelper alertViewWithMessage:NotificationMessageRequestWasSentWithError];
        }
    }];
}

- (void)showSearchResults
{
    [UIView animateWithDuration:0.25f delay:0.f options:0 animations:^{
        self.tableView.alpha = 1.f;
        self.containerView.alpha = 0.f;
    } completion:^(BOOL finished) {
        self.tableView.hidden = NO;
        self.containerView.hidden = YES;
        NSIndexSet* section = [NSIndexSet indexSetWithIndex:0];
        [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)hideSearchResults
{
    [UIView animateWithDuration:0.25f delay:0.f options:0 animations:^{
        self.tableView.alpha = 0.f;
        self.containerView.alpha = 1.f;
    } completion:^(BOOL finished) {
        self.tableView.hidden = YES;
        self.containerView.hidden = NO;
        NSIndexSet* section = [NSIndexSet indexSetWithIndex:0];
        [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)fillAttendee:(ClientProfile*)attendee
{
    self.firstNameTextField.text = self.selectedUser.clientFirstName;
    self.lastNameTextField.text = self.selectedUser.clientLastName;
    self.emailTextField.text = self.selectedUser.clientEmail;
    self.phoneNumberTextField.text = self.selectedUser.clientPhone;
}

- (void)clearAllFields
{
    self.selectedUser = nil;
    self.firstNameTextField.text = @"";
    self.lastNameTextField.text = @"";
    self.emailTextField.text = @"";
    self.phoneNumberTextField.text = @"";
}

- (void)addSelectedAttendee
{
    NSNumber *attendeeID = self.selectedUser.clientID;
    __weak typeof(self) weakSelf = self;
    NSString *stringDate = [CalendarDataSource stringZeroInRequestFormat:self.activeSession.sessionDate];

    [[NetworkManager sharedManager] ptSquaredAPITrainerAddAttendeesFromIDsArray:@[attendeeID] forSessionID:self.sessionID parentID:self.activeSession.sessionParentID sessionTime:stringDate reoccurence:self.activeSession.sessionReoccurence result:^(BOOL success, NSError *error, id response) {
        [weakSelf clearAllFields];
        if (success) {
            weakSelf.selectedUser = nil;
            [weakSelf closeButtonPress:nil];
        } else {
            [AppHelper alertViewWithMessage:response];
        }
    }];
}

- (void)addNewUser
{
    NSString *firstName = self.firstNameTextField.text;
    NSString *lastName = self.lastNameTextField.text;
    NSString *email = self.emailTextField.text;
    NSString *phone = self.phoneNumberTextField.text;
    
    if ([self checkInputDataForNewUser:firstName lastName:lastName email:email phone:phone]) {
        __weak typeof(self) weakSelf = self;
        
        if ([phone rangeOfString:@"+"].location == NSNotFound) {
            phone = [NSString stringWithFormat:@"+%@", phone];
        }
        
        NSString *stringDate = [CalendarDataSource stringZeroInRequestFormat:self.activeSession.sessionDate];
        
        [[NetworkManager sharedManager] ptSquaredApiTrainerAddClientWithFirstName:firstName lastName:lastName email:email phone:phone sessionID:self.sessionID parentID:self.activeSession.sessionParentID sessionTime:stringDate reoccurence:self.activeSession.sessionReoccurence addClientResult:^(BOOL success, NSError *error, id response) {
            if (success) {
                [AppHelper alertViewWithMessage:response delegate:weakSelf];
            } else {
                [AppHelper alertViewWithMessage:response];
            }
        }];
    }
}

- (BOOL)checkInputDataForNewUser:(NSString *)firstName lastName:(NSString *)lastName email:(NSString *)userEmail phone:(NSString *)userPhone
{
    NSString *template = @"Please enter a valid ";
    NSString *message = nil;
    
    if (!firstName.length) {
        message = [NSString stringWithFormat:@"%@ %@", template, @"first name"];
    } else if (!lastName.length) {
        message = [NSString stringWithFormat:@"%@ %@", template,@"first name"];
    } else if (!userEmail.length || ![AppHelper isNSStringIsValidEmail:userEmail applyFilter:NO]) {
        message = [NSString stringWithFormat:@"%@ %@", template,@"email"];
    } else if (![AppHelper isNSStringIsValidPhoneNumber:userPhone]) {
        message = [NSString stringWithFormat:@"%@ %@", template, @"phone number.\n\nFor example: +61491570110"];
    }
    
    if (message.length) {
        [AppHelper alertViewWithMessage:message];
        return NO;
    } else {
        return YES;
    }
}

@end