//
//  TrainerAddAttendeeViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class Session;

@protocol AddAttendeeDelegate <NSObject>

- (void)addedAttendeeSuccesful;

@end

@interface TrainerAddAttendeeViewController : UIViewController

@property (weak, nonatomic) id<AddAttendeeDelegate>delegate;
@property (assign, nonatomic) NSInteger sessionID;
@property (strong, nonatomic) UIImage *screenShotForBlur;

@property (strong, nonatomic) Session *activeSession;

@end
