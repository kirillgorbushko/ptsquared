//
//  TrainerCalendarViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 30.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerCalendarViewController.h"
#import "CalendarCell.h"
#import "CalendarDataSource.h"
#import "CalendarDay.h"

static NSString *const CalendarCellIdentifier = @"calendarCell";
static CGFloat const HeightOfCollectionView = 57;

@interface TrainerCalendarViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) CalendarDataSource *calendarDataSourceProvider;
@property (strong, nonatomic) NSIndexPath *prevSelectedCellIndexPath;

@end

@implementation TrainerCalendarViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(willAppearNewTrainerCalendarViewController:)]) {
        [self.delegate willAppearNewTrainerCalendarViewController:self];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showSelectedCell];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    CalendarCell *cell = (CalendarCell *)[self.collectionView cellForItemAtIndexPath:self.prevSelectedCellIndexPath];
    cell.daySelectionView.hidden = YES;
}

#pragma mark - Custom Accessors

- (void)setCalendarDataSource:(NSArray *)calendarDataSource
{
    _calendarDataSource = calendarDataSource;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showSelectedCell];
    });
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self setCellSelectedAtIndexPath:indexPath];
    self.indexPathForSelection = indexPath;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelecetCell:atIndexPath:selectedDate:)]) {
        [self.delegate didSelecetCell:(CalendarCell *)[collectionView cellForItemAtIndexPath:indexPath] atIndexPath:indexPath selectedDate:((CalendarDay *)self.calendarDataSource[indexPath.row]).calendarDayDate];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.calendarDataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CalendarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CalendarCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[CalendarCell alloc] init];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellWidth = ([UIScreen mainScreen].bounds.size.width - 4) / 7; //4 - offset for cells (0.5 * 8)
    CGSize cellsize = CGSizeMake(cellWidth, HeightOfCollectionView);
    
    return cellsize;
}

#pragma mark - Public

- (void)setCellSelectedAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.prevSelectedCellIndexPath && self.prevSelectedCellIndexPath != indexPath) {
        [self deselectCellAtIndexPath:self.prevSelectedCellIndexPath];
    }
    self.prevSelectedCellIndexPath = indexPath;
    
    [self.collectionView layoutIfNeeded];
    CalendarCell *cell = (CalendarCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [cell setCellSelected:YES];
}

#pragma mark - Private

- (void)deselectCellAtIndexPath:(NSIndexPath *)indexPath
{
    CalendarCell *prevSelectedCell = (CalendarCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [self configureCell:prevSelectedCell atIndexPath:indexPath];
}

- (void)configureCell:(CalendarCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CalendarDay *currentDay = (CalendarDay *)self.calendarDataSource[indexPath.row];
    NSDate *shownDate = currentDay.calendarDayDate;
    NSInteger day = [CalendarDataSource numberofDay:currentDay.calendarDayDate];
    cell.dayNumberLabel.text = [NSString stringWithFormat:@"%i", (int)day];
    
    [cell setCellSelected:NO];
    
    if ([CalendarDataSource isDay:[NSDate date] isSameDayAs:shownDate]){
        cell.daySelectionView.hidden = NO;
        cell.daySelectionView.backgroundColor = [UIColor blackColor];
    }

    if (currentDay.calendarSessions.count) {
        [cell setCellSelectedWithDay:currentDay];
    }
}

- (void)showSelectedCell
{
    if (self.indexPathForSelection) {
        [self setCellSelectedAtIndexPath:self.indexPathForSelection];
    }
}

@end
