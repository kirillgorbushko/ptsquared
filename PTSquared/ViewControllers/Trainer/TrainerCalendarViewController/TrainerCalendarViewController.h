//
//  TrainerCalendarViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 30.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class CalendarCell;
@class TrainerCalendarViewController;

@protocol TrainerCalendarViewControllerDeleagte <NSObject>

@optional
- (void)didSelecetCell:(CalendarCell *)cell atIndexPath:(NSIndexPath *)indexPath selectedDate:(NSDate *)date;
- (void)willAppearNewTrainerCalendarViewController:(TrainerCalendarViewController *)calendar;

@end

@interface TrainerCalendarViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) id <TrainerCalendarViewControllerDeleagte> delegate;

@property (assign, nonatomic) NSInteger dataSourceOffset;
@property (assign, nonatomic) NSIndexPath *indexPathForSelection;
@property (strong, nonatomic) __block NSArray *calendarDataSource;

- (void)setCellSelectedAtIndexPath:(NSIndexPath *)indexPath;

@end
