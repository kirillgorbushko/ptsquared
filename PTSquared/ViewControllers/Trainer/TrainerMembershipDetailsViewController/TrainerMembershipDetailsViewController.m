//
//  TrainerMembershipDetailsViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 04.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerMembershipDetailsViewController.h"
#import "Animation.h"
#import "UIImage+ImageEffects.h"
#import "ProfileManager.h"
#import "ClientProfile.h"
#import "Session.h"
#import "CalendarDataSource.h"

static NSString *const AnimationKey = @"fadeAnimation";

static NSString *const ButtonTitleDeductSesssion = @"Deduct Session";
static NSString *const ButtonTitleReceivePayment = @"Receive Payment";

static NSString *const SessionTitleGroup = @"Group session";
static NSString *const SessionTitleIndividual = @"Private session";

@interface TrainerMembershipDetailsViewController()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *sessionControllButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *participantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionleftQtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *sessionTypeLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLabelNameConstraint;

@end

@implementation TrainerMembershipDetailsViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [AppHelper prepareInformationButtons:@[self.sessionControllButton, self.cancelButton]];
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.backgroundImageView.image = [self.screenShotForBlur applyDarkEffect];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

#pragma mark - IBActions

- (IBAction)closeButtonPress:(id)sender
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:AnimationKey];
    self.view.layer.opacity = 0.f;
}

- (IBAction)sessionControllButtonPress:(id)sender
{
    NSInteger clientID = [self.attendee.clientID integerValue];
    [AppHelper showLoader];
    
    [self deductSessionId:self.activeSession.sessionId forClient:clientID];
}

#pragma mark - APIUsage

- (void)deductSessionId:(NSInteger)sessionID forClient:(NSInteger)clientID
{
    __weak typeof(self) weakSelf = self;

    NSString *stingDate = [CalendarDataSource stringZeroInRequestFormat:self.activeSession.sessionDate];

    [[NetworkManager sharedManager] ptSquaredAPITrainerDeductSessionWithID:self.activeSession.sessionId forClient:clientID parentID:self.activeSession.sessionParentID sessionTime:stingDate reoccurence:self.activeSession.sessionReoccurence operationResult:^(BOOL success, NSError *error, id response) {
        [AppHelper hideLoader];
        if (success) {
            [AppHelper alertViewWithMessage:NotificationMessageRequestWasSentSuccessfully delegate:weakSelf];
        } else {
            [AppHelper alertViewWithMessage:NotificationMessageRequestWasSentWithError];
        }
    }];
}

- (void)receivePaymentForSessionId:(NSInteger)sessionID forClient:(NSInteger)clientID
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPITrainerReceivePaymentForSessionWithID:sessionID forClient:clientID operationResult:^(BOOL success, NSError *error, id response) {
        [AppHelper hideLoader];
        if (success) {
            [AppHelper alertViewWithMessage:response delegate:weakSelf];
        } else {
            [AppHelper alertViewWithMessage:NotificationMessageRequestWasSentWithError];
        }
    }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self closeButtonPress:self];
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:AnimationKey]) {
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
        [self attendeeAddedToSession];
    }
}

#pragma mark - Private

- (void)prepareUI
{
    NSString *firstName = self.attendee.clientFirstName;
    NSString *lastName = self.attendee.clientLastName;
    self.participantNameLabel.text = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    
    NSString *attendeeStatus = self.attendee.clientCurrentStatusForSessionID;
    
    if (![attendeeStatus isEqualToString:@"invited"]) {
        self.sessionControllButton.enabled = NO;
        self.sessionControllButton.alpha = 0.5f;
    }
    
    switch (self.sessionType) {
        case SessionTypeGroup: {
            self.sessionTypeLabel.text = SessionTitleGroup;
            [self setInerfaceForAvailableSessions:self.attendee.clientAvailableGroupSessions.integerValue];
            break;
        }
        case SessionTypeIndividual: {
            self.sessionTypeLabel.text = SessionTitleIndividual;
            [self setInerfaceForAvailableSessions:self.attendee.clientAvailableIndividualSessions.integerValue];
            break;
        }
    }
    
    if (IS_IPHONE_4_OR_LESS) {
        self.topLabelNameConstraint.constant /= 3;
    }
}

- (void)setInerfaceForAvailableSessions:(NSInteger)availableSessions
{
    if (availableSessions) {
        [self setInterfaceForDeductSession];
        self.sessionleftQtyLabel.text = [NSString stringWithFormat:@"%i", (int)availableSessions];
    } else {
        [self setInterfaceForPaymentSession];
    }
}

- (void)setInterfaceForDeductSession
{
    [self.sessionControllButton setTitle:ButtonTitleDeductSesssion forState:UIControlStateNormal];
}

- (void)setInterfaceForPaymentSession
{
    [self.sessionControllButton setTitle:ButtonTitleReceivePayment forState:UIControlStateNormal];
    self.containerView.hidden = YES;
    self.infoLabel.hidden = NO;
}

- (void)attendeeAddedToSession
{
    if ([self.delegate respondsToSelector:@selector(attendeeAddedToSessionSuccessfully)]) {
        [self.delegate performSelector:@selector(attendeeAddedToSessionSuccessfully)];
    }
}

@end