//
//  TrainerMembershipDetailsViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 04.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class ClientProfile;
@class Session;

typedef NS_ENUM(NSUInteger, SessionType) {
    SessionTypeGroup,
    SessionTypeIndividual
};

@protocol TrainerMembershipDetailsDelegate <NSObject>

- (void)attendeeAddedToSessionSuccessfully;

@end

@interface TrainerMembershipDetailsViewController : UIViewController <UIAlertViewDelegate>

@property (assign, nonatomic) SessionType sessionType;
@property (strong, nonatomic) Session *activeSession;
@property (strong, nonatomic) UIImage *screenShotForBlur;
@property (strong, nonatomic) ClientProfile *attendee;

@property (weak, nonatomic) id<TrainerMembershipDetailsDelegate> delegate;

@end