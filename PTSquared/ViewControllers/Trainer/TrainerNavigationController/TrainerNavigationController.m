//
//  TrainerNavigationController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 30.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerNavigationController.h"
#import "UINavigationController+Addition.h"
#import "ModeSelectionViewController.h"
#import "ProfileManager.h"
#import "CredentialFetchService.h"
#import "Animation.h"
#import "LogoutViewController.h"
#import "Snapshot.h"
#import "UIImage+ImageEffects.h"

static CGFloat const OffsetForImage = 10;

@interface TrainerNavigationController ()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIButton *titleBackButton;

@end

@implementation TrainerNavigationController

#pragma mark - LifeCycle

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        [self prepareNavigationBar];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareNavigationBar];
}

#pragma mark - IBActions

- (void)backAction
{
    [self popViewControllerAnimated:YES];
}

#pragma mark - Private

- (void)prepareBackButtonItemWithNavBarSize:(CGSize)navBarSize
{
    self.titleBackButton = [[UIButton alloc] init];
    CGRect buttonFrame = CGRectMake(0, 0, 54, navBarSize.height);
    self.titleBackButton.frame = buttonFrame;
    self.titleBackButton.imageEdgeInsets = UIEdgeInsetsMake(OffsetForImage, OffsetForImage, OffsetForImage, OffsetForImage * 2);
    [self.titleBackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationBar addSubview:self.titleBackButton];
}

- (void)prepareTitleLabelWithNavBarSize:(CGSize)navBarSize
{
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, navBarSize.width - navBarSize.height *2,  navBarSize.height)];
    self.titleLabel.minimumScaleFactor = 0.5;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.center = CGPointMake(navBarSize.width / 2, navBarSize.height / 2);
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.numberOfLines = 0;
    [self.navigationBar addSubview:self.titleLabel];
}

#pragma mark - Public

- (void)prepareNavigationBar
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

    self.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationBar.translucent = NO;
    
    [self prepareTitleLabelWithNavBarSize:self.navigationBar.frame.size];
    [self prepareBackButtonItemWithNavBarSize:self.navigationBar.frame.size];
}

- (void)setBackButtonWithImageNamed:(NSString *)imageName
{
    if (!self.titleBackButton) {
        [self prepareBackButtonItemWithNavBarSize:self.navigationBar.frame.size];
    }
    [self.titleBackButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [self.titleBackButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
    [self.titleBackButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateSelected];
}

- (void)setTitleText:(NSString *)text
{
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:text attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:17.0], NSForegroundColorAttributeName : [UIColor blackColor] }];
    self.titleLabel.attributedText = title;
}

- (void)cleanUpBackButton
{
    [self.titleBackButton removeFromSuperview];
    self.titleBackButton = nil;
}

- (void)logout
{
    LogoutViewController *logoutViewController = [[UIStoryboard storyboardWithName:@"Login" bundle:nil] instantiateViewControllerWithIdentifier:@"logout"];
    UIImage *snapShot = [[Snapshot screenshot] applyDarkEffect];
    logoutViewController.backgroundImage = snapShot;
    [self presentViewController:logoutViewController animated:NO completion:nil];
}

@end
