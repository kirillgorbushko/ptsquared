//
//  TrainerNavigationController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 30.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface TrainerNavigationController : UINavigationController

- (void)setTitleText:(NSString *)text;
- (void)setBackButtonWithImageNamed:(NSString *)imageName;
- (void)cleanUpBackButton;

- (void)logout;

@end
