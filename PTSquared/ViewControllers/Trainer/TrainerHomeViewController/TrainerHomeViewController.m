//
//  HomeViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerHomeViewController.h"
#import "TrainerNavigationController.h"
#import "UINavigationController+Addition.h"
#import "CalendarDataSource.h"
#import "TutorialViewController.h"
#import "FacebookManager.h"
#import "CalendarDay.h"
#import "Animation.h"
#import "CalendarEventSynchronizer.h"

static NSString *const TodayDayTitle = @"Today";
static NSInteger const CashCountForTwoMonth = 84;

@interface TrainerHomeViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;
@property (weak, nonatomic) IBOutlet UIView *dayNameContainer;
@property (weak, nonatomic) IBOutlet UILabel *dateDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@property (weak, nonatomic) UIPageViewController *calendarPageViewController;
@property (weak, nonatomic) UIPageViewController *calendarEventPageViewController;
@property (strong, nonatomic) NSDate *selectedDate;
@property (strong, nonatomic) NSIndexPath *currentlySelectedIndexPath;
@property (strong, nonatomic) TrainerCalendarViewController *previousCalendarViewController;
@property (strong, nonatomic) TrainerCalendarViewController *nextCalendarViewController;
@property (strong, nonatomic) CalendarDataSource *calendarDataSourceCreator;
@property (strong, nonatomic) NSMutableArray *dataSource;

@end

@implementation TrainerHomeViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupDefaultValues];
    [self prepareCalendarPageController];
    [self prepareCalendarEventPageController];
    [self setNewDate:[NSDate date]];
    [self updateWeekFromDay:nil];
    
    [[CalendarEventSynchronizer sharedInstance] requestAccess:^(BOOL granted, NSError *error) {
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareNavigationBar];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [((TrainerNavigationController *)self.navigationController) cleanUpBackButton];
    [self.navigationController setBackButtonStyle:BackButtonStyleDark];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updateCalendarIfNeeded];
    [self updatedEventsIfNeeded];
}

#pragma mark - IBActions

- (void)rightButtonPressed
{
    [((TrainerNavigationController *)self.navigationController) logout];
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    if ([pageViewController.viewControllers[0] isKindOfClass:[TrainerCalendarViewController class]]) {
        return [self viewControllerAtIndex:((TrainerCalendarViewController *)pageViewController.viewControllers[0]).dataSourceOffset - 1];
    } else {
        NSDate *prevDate = [CalendarDataSource dayBefore:self.selectedDate];
        return [self viewControllerWithDate:prevDate];
    }
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    if ([pageViewController.viewControllers[0] isKindOfClass:[TrainerCalendarViewController class]]) {
        return [self viewControllerAtIndex:((TrainerCalendarViewController *)pageViewController.viewControllers[0]).dataSourceOffset + 1];
    } else {
        NSDate *nextDate = [CalendarDataSource dayAfter:self.selectedDate];
        return [self viewControllerWithDate:nextDate];
    }
}

#pragma mark - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{    
    if ([pageViewController.viewControllers[0] isKindOfClass:[TrainerCalendarViewController class]]) {
        if (self.currentlySelectedIndexPath) {
            ((TrainerCalendarViewController *)pendingViewControllers[0]).indexPathForSelection = self.currentlySelectedIndexPath;
        }
    }
    self.infoLabel.hidden = YES;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed) {
        if ([pageViewController.viewControllers[0] isKindOfClass:[TrainerCalendarViewController class]]) {
            if (self.currentlySelectedIndexPath) {
                ((TrainerCalendarViewController *)previousViewControllers[0]).indexPathForSelection = self.currentlySelectedIndexPath;
            }
            if (((TrainerCalendarViewController *)self.calendarPageViewController.viewControllers[0]).dataSourceOffset > ((TrainerCalendarViewController *)previousViewControllers[0]).dataSourceOffset) {
                NSDate *newDate = [CalendarDataSource dateWithOffset:7 fromDate:self.selectedDate];
                
                [self updateWeekFromDay:newDate];
                
                self.selectedDate = newDate;
                __weak typeof(self) weakSelf = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.calendarEventPageViewController setViewControllers:@[[weakSelf viewControllerWithDate:newDate]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
                });
            } else {
                NSDate *newDate = [CalendarDataSource dateWithOffset:-7 fromDate:self.selectedDate];
                
                [self updateWeekFromDay:newDate];
                
                self.selectedDate = newDate;
                __weak typeof(self) weakSelf = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [weakSelf.calendarEventPageViewController setViewControllers:@[[weakSelf viewControllerWithDate:newDate]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
                });
            }
            
        } else {
            NSDate *newRequestedDate = ((TrainerCalendarEventViewController *)self.calendarEventPageViewController.viewControllers[0]).requestedDate;
            if ([CalendarDataSource isDate:self.selectedDate latestThanDate:newRequestedDate]) {
                [self setNextTrainerCalendar];
            } else {
                [self setPreviousTrainerCalendar];
            }
            self.selectedDate = newRequestedDate;
        }
    }
    [self setNewDate:self.selectedDate];
}

#pragma mark - TrainerCalendarViewControllerDeleagte

- (void)didSelecetCell:(CalendarCell *)cell atIndexPath:(NSIndexPath *)indexPath selectedDate:(NSDate *)date
{
    self.currentlySelectedIndexPath = indexPath;
    
    if ([CalendarDataSource isDay:self.selectedDate isSameDayAs:date]) {
        self.selectedDate = date;
        return;
    }
    UIPageViewControllerNavigationDirection direction = UIPageViewControllerNavigationDirectionReverse;
    if ([CalendarDataSource isDate:date latestThanDate:self.selectedDate]) {
        direction = UIPageViewControllerNavigationDirectionForward;
    }
    self.selectedDate = date;
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.calendarEventPageViewController setViewControllers:@[[weakSelf viewControllerWithDate:weakSelf.selectedDate]] direction:direction animated:YES completion:nil];
    });
    [self setNewDate:self.selectedDate];
    
    [self shouldHideInformationlabel];
}

- (void)willAppearNewTrainerCalendarViewController:(TrainerCalendarViewController *)calendar
{
    NSArray *dataSource = calendar.calendarDataSource;
    if (dataSource.count && [[dataSource firstObject] isKindOfClass:[CalendarDay class]]) {
        CalendarDay *firstWeekDay = (CalendarDay *)[dataSource firstObject];
        [self updateWeekFromDay:firstWeekDay.calendarDayDate];
    }
}

#pragma mark - TrainerCalendarUpdateLogic

- (void)setPreviousTrainerCalendar
{
    if (self.currentlySelectedIndexPath.row + 1 < 7) {
        TrainerCalendarViewController *viewController = self.calendarPageViewController.viewControllers[0];
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:self.currentlySelectedIndexPath.row + 1 inSection:0];
        [viewController setCellSelectedAtIndexPath:newIndexPath];
        self.currentlySelectedIndexPath = newIndexPath;
        viewController.indexPathForSelection = newIndexPath;
    } else {
        TrainerCalendarViewController *newVC = [self viewControllerAtIndex:((TrainerCalendarViewController *)self.calendarPageViewController.viewControllers[0]).dataSourceOffset + 1];
        newVC.indexPathForSelection = [NSIndexPath indexPathForItem:0 inSection:0];
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.calendarPageViewController setViewControllers: @[newVC] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
                if (finished) {
                    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
                    weakSelf.currentlySelectedIndexPath = newIndexPath;
                    [newVC setCellSelectedAtIndexPath:newIndexPath];
                    newVC.indexPathForSelection = newIndexPath;
                }
            }];
        });
    }
}

- (void)setNextTrainerCalendar
{
    if (self.currentlySelectedIndexPath.row > 0) {
        TrainerCalendarViewController *viewController = self.calendarPageViewController.viewControllers[0];
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:self.currentlySelectedIndexPath.row - 1 inSection:0];
        [viewController setCellSelectedAtIndexPath:newIndexPath];
        self.currentlySelectedIndexPath = newIndexPath;
        viewController.indexPathForSelection = newIndexPath;
    } else {
        TrainerCalendarViewController *newVC = [self viewControllerAtIndex:((TrainerCalendarViewController *)self.calendarPageViewController.viewControllers[0]).dataSourceOffset - 1];
        newVC.indexPathForSelection = [NSIndexPath indexPathForItem:6 inSection:0];
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.calendarPageViewController setViewControllers: @[newVC] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:^(BOOL finished) {
                if (finished) {
                    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:6 inSection:0];
                    weakSelf.currentlySelectedIndexPath = newIndexPath;
                    [newVC setCellSelectedAtIndexPath:newIndexPath];
                    newVC.indexPathForSelection = newIndexPath;
                }
            }];
        });
    }
}

#pragma mark - TrainerCalendarEventViewControllerDelegate

- (void)shouldShowInformationLabel
{
    if (self.infoLabel.hidden) {
        self.infoLabel.hidden = NO;
        [self.infoLabel.layer addAnimation:[Animation fadeAnimFromValue:0. to:1. delegate:nil] forKey:nil];
        self.infoLabel.layer.opacity = 1.;
    }
}

- (void)shouldHideInformationlabel
{
    self.infoLabel.hidden = YES;
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.infoLabel.layer animationForKey:@"hideLabel"]) {
        [self.infoLabel.layer removeAllAnimations];
        self.infoLabel.hidden = YES;
    }
}

#pragma mark - Private

- (TrainerCalendarViewController *)viewControllerAtIndex:(NSInteger)index
{
    TrainerCalendarViewController *pageContent = [self.storyboard instantiateViewControllerWithIdentifier:@"trainerCalendarVC"];
    pageContent.dataSourceOffset = index;
    
    __weak typeof(self) weakSelf = self;
    [self dataWithOffset:index result:^(NSArray *arr) {
        pageContent.calendarDataSource = arr;
        if (!index) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.calendarEventPageViewController setViewControllers:@[[weakSelf viewControllerWithDate:weakSelf.selectedDate]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            });
        }
    }];
    
    if (self.currentlySelectedIndexPath) {
        pageContent.indexPathForSelection = self.currentlySelectedIndexPath;
    }
    pageContent.delegate = self;
    return pageContent;
}

- (TrainerCalendarEventViewController *)viewControllerWithDate:(NSDate *)date
{
    TrainerCalendarEventViewController *pageContent = [self.storyboard instantiateViewControllerWithIdentifier:@"trainerEventVC"];
    pageContent.requestedDate = date;
    pageContent.delegate = self;
    return pageContent;
}

- (void)prepareNavigationBar
{
    [((TrainerNavigationController *)self.navigationController) setBackButtonWithImageNamed:@"PT_logo_Coloured"];
    [((TrainerNavigationController *)self.navigationController) setTitleText:@"Calendar"];
    [self.navigationController setRightButtonWithImageNamed:@"Logout_Icon" andDelegate:self];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}

- (void)prepareCalendarPageController
{
    self.calendarPageViewController = self.childViewControllers[0];
    TrainerCalendarViewController *viewControllerStart = [self viewControllerAtIndex:0];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.calendarPageViewController setViewControllers:@[viewControllerStart] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    });
    self.calendarPageViewController.dataSource = self;
    self.calendarPageViewController.delegate = self;
}

- (void)prepareCalendarEventPageController
{
    self.calendarEventPageViewController = self.childViewControllers[1];
    
    TrainerCalendarEventViewController *viewControllerStart = [self viewControllerWithDate:self.selectedDate];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.calendarEventPageViewController setViewControllers:@[viewControllerStart] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    });
    self.calendarEventPageViewController.dataSource = self;
    self.calendarEventPageViewController.delegate = self;
}

- (void)updateWeekFromDay:(NSDate *)day
{
    NSDictionary *formats = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"WeekDayOrder" ofType:@"plist"]];
    NSMutableArray *days;
    if ([CalendarDataSource firstUserWeekDayInCalendar] == 1) {
        days = [NSMutableArray arrayWithArray:[formats objectForKey:@"AmericanFormat"]];
    } else if ([CalendarDataSource firstUserWeekDayInCalendar] == 2) {
        days = [NSMutableArray arrayWithArray:[formats objectForKey:@"EuropianFormat"]];
    }

    if (day) {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekOfYear|NSCalendarUnitWeekday fromDate:day];
        NSInteger week = components.weekOfYear;
        
        NSDate *currentDate = [NSDate date];
        components = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekOfYear|NSCalendarUnitWeekday fromDate:currentDate];
        NSInteger currentWeek = components.weekOfYear;
        
        if (week == currentWeek) {
            NSInteger todayWeekday = components.weekday - 1;
        
            if ([CalendarDataSource firstUserWeekDayInCalendar] == 2) {
                if (todayWeekday) {
                    todayWeekday--;
                } else {
                    todayWeekday = 6;
                }
            }
            days[todayWeekday] = TodayDayTitle;
        }
    } else {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekOfYear|NSCalendarUnitWeekday fromDate:[NSDate date]];
        NSInteger todayWeekday = components.weekday - 1;
        if ([CalendarDataSource firstUserWeekDayInCalendar] == 2) {
            if (todayWeekday) {
                todayWeekday--;
            } else {
                todayWeekday = 6;
            }
        }
        days[todayWeekday] = TodayDayTitle;
    }
    
    NSArray *subviews = [self.dayNameContainer subviews];
    for (id subview in subviews) {
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel*)subview;
            label.text = days[label.tag];
        }
    }
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
}

- (void)setupDefaultValues
{
    NSString *stringDate = [CalendarDataSource stringFromDateInRequestFormat:[NSDate date]];
    NSDate *startDate = [CalendarDataSource zeroHourDateFromString:stringDate];

    self.selectedDate = startDate;
    self.currentlySelectedIndexPath = [NSIndexPath indexPathForItem:[CalendarDataSource weekDayNumberWithDate:self.selectedDate] - 1 inSection:0];
}

- (void)setNewDate:(NSDate *)date
{
    if (!self.calendarDataSourceCreator) {
        self.calendarDataSourceCreator = [[CalendarDataSource alloc] init];
    }
    self.dateDescriptionLabel.text = [CalendarDataSource stringFromDate:date];
}

#pragma mark - DataSourceProviders

- (void)dataWithOffset:(NSInteger)dataSourceOffset result:(void(^)(NSArray *arr))result
{
    NSDate *startOfTheWeek = [self weekStartDateWithOffset:dataSourceOffset];
    __weak typeof(self) weakSelf = self;
    void (^UpdateDataSource)(NSArray *response) = ^(NSArray *response) {
        weakSelf.calendarDataSourceCreator = [[CalendarDataSource alloc] init];
        NSArray *data = [weakSelf.calendarDataSourceCreator daysForWeekWithOffsetFromNow:dataSourceOffset sessionsDays:response];
        if (response.count) {
            [weakSelf updateDataSourceWithNewData:data];
        }
        result (data);
    };
    
    UpdateDataSource(@[]); //offlineGeneration dataSource
    [[NetworkManager sharedManager] ptSquaredAPITrainerGetSessionInPeriodFrom:startOfTheWeek daysCount:8 operationResult:^(BOOL success, NSError *error, NSArray *response) {
        if (success) {
            [self performSyncOfEventsWithStartDate:startOfTheWeek andEvents:response];
            UpdateDataSource(response);
        }
    }];
}

- (NSDate *)weekStartDateWithOffset:(NSInteger)offset
{
    NSString *stringDate = [CalendarDataSource stringFromDateInRequestFormat:[NSDate date]];
        
    NSDate *startDate = [CalendarDataSource zeroHourDateFromString:stringDate];

    NSDate *dateForRequestedWeek = [CalendarDataSource dateWithOffset:offset * 7 fromDate:startDate];
    NSDate *startOfTheWeek;
    NSTimeInterval interval;
    [[NSCalendar currentCalendar] rangeOfUnit:NSWeekCalendarUnit startDate:&startOfTheWeek interval:&interval forDate:dateForRequestedWeek];
    return startOfTheWeek;
}

- (NSDate *)midnightUTC:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setTimeZone:[NSTimeZone defaultTimeZone]];
    
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:date];
    [dateComponents setHour:3];
    [dateComponents setMinute:0];
    [dateComponents setSecond:0];
    NSDate *midnightUTC = [calendar dateFromComponents:dateComponents];
    return midnightUTC;
}

#pragma mark - Update

- (void)updateCalendarIfNeeded
{
    if (!self.calendarPageViewController.viewControllers.count) {
        return;
    }
    
    TrainerCalendarViewController *viewControllerToReload = [self viewControllerAtIndex:((TrainerCalendarViewController *)self.calendarPageViewController.viewControllers[0]).dataSourceOffset];
    NSInteger currentOffset = viewControllerToReload.dataSourceOffset;
    if (currentOffset) {
        TrainerCalendarViewController *viewControllerToReload = [self viewControllerAtIndex:((TrainerCalendarViewController *)self.calendarPageViewController.viewControllers[0]).dataSourceOffset];
        viewControllerToReload.indexPathForSelection = self.currentlySelectedIndexPath;
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.calendarPageViewController setViewControllers:@[viewControllerToReload] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
                if (finished) {
                    [((TrainerCalendarViewController *)weakSelf.calendarPageViewController.viewControllers[0]) setCellSelectedAtIndexPath:weakSelf.currentlySelectedIndexPath];
                }
            }];
        });
    }
}

- (void)updatedEventsIfNeeded
{
    BOOL needToUpdateEvents = ![CalendarDataSource isDay:self.selectedDate isSameDayAs:[NSDate date]];
    if (needToUpdateEvents) {
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf.calendarEventPageViewController setViewControllers:@[[weakSelf viewControllerWithDate:weakSelf.selectedDate]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        });
    }
}

- (void)updateDataSourceWithNewData:(NSArray *)newData
{
//    if (!self.dataSource) {
        self.dataSource = [[NSMutableArray alloc] init];
//    }
    for (CalendarDay *day in newData) {
        if (![self.dataSource containsObject:day]) {
            [self.dataSource addObject:day];
        }
    }
    if (self.dataSource.count == CashCountForTwoMonth) {
        for (int i = 0; i < 7; i++) {
            [self.dataSource removeObjectAtIndex:i];
        }
    }
}

#pragma mark - Sync Calendar

- (void)performSyncOfEventsWithStartDate:(NSDate *)startDate andEvents:(NSArray *)events
{
    NSDate *endDate = [startDate dateByAddingTimeInterval:(7 * 24 * 60 * 60)]; //days * hours * min * sec
    [[CalendarEventSynchronizer sharedInstance] synchronizeCalendarEventsWithSessions:events inRangeFromDate:startDate toDate:endDate calendarType:CalendarSynchronizerTypePersonalTrainer];
}

@end