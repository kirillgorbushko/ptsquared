//
//  HomeViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 29.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerCalendarViewController.h"
#import "TrainerCalendarEventViewController.h"

@interface TrainerHomeViewController : BaseDynamicColorViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate, TrainerCalendarViewControllerDeleagte,TrainerCalendarEventViewControllerDelegate>

@end
