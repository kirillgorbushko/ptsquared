//
//  TrainerCalendarEventViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 30.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@protocol TrainerCalendarEventViewControllerDelegate <NSObject>

@required
- (void)shouldShowInformationLabel;
- (void)shouldHideInformationlabel;

@end

@interface TrainerCalendarEventViewController : BaseDynamicColorViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id <TrainerCalendarEventViewControllerDelegate> delegate;

@property (assign, nonatomic) NSInteger pageOffset;
@property (strong, nonatomic) NSDate *requestedDate;

@end
