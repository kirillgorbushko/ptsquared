//
//  TrainerCalendarEventViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 30.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerCalendarEventViewController.h"
#import "EventDescriptionTableViewCell.h"
#import "TrainerSessionViewController.h"
#import "CalendarDataSource.h"
#import "Session.h"
#import "Animation.h"
#import "CalendarDay.h"

static NSString *const TableViewEventCellIdentifier = @"eventDescriptionCell";
static CGFloat const TableViewCellHeight = 52;
static NSString *const TrainerSessionDetailsSegueIdentifier = @"trainerSession";

@interface TrainerCalendarEventViewController () 

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) __block NSArray *sessions;

@end

@implementation TrainerCalendarEventViewController

#pragma mark - LIfeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self fetchDataForEvents];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updatedContainerUI];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.sessions = nil;
}

#pragma mark - Custom Accessors

- (void)setSessions:(NSArray *)sessisons
{
    _sessions = sessisons;
    [self animatedReloadDataIfNeeded];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sessions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewEventCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[EventDescriptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewEventCellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableViewCellHeight;
}

- (void)configureCell:(EventDescriptionTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Session *currentSession = self.sessions[indexPath.row];
    cell.session = currentSession;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventDescriptionTableViewCell *cell = (EventDescriptionTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:TrainerSessionDetailsSegueIdentifier]){
        TrainerSessionViewController *viewController = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Session *session = self.sessions[indexPath.row];
        viewController.isPublic = session.sessionIsPublic;
        viewController.sessionType = session.sessionType;
        viewController.session = session;
    }
}

#pragma mark - Private

- (void)updatedContainerUI
{
    if (!self.sessions.count) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(shouldShowInformationLabel)]) {
            [self.delegate shouldShowInformationLabel];
        }
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(shouldHideInformationlabel)]) {
            [self.delegate shouldHideInformationlabel];
        }
    }
}

- (void)fetchDataForEvents
{
    if (!self.sessions) {
        self.sessions = [[NSArray alloc] init];
    }
    
    __weak typeof(self) weakSelf = self;
    void (^UpdateDataSource)(NSArray *) = ^(NSArray *response) {
        if (response.count) {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(shouldHideInformationlabel)]) {
                [weakSelf.delegate shouldHideInformationlabel];
            }
        }
        weakSelf.sessions = response;
    };
    
    CalendarDataSource *dataSource = [[CalendarDataSource alloc] init];
    NSDate *dateWithOffset = [dataSource UTCDateFromLocalDate:self.requestedDate];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:-1];
    NSDate *prevDay = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:dateWithOffset options:0];
    
    //taken 3 day due to server response - not return day if 00:00 set in session
    [[NetworkManager sharedManager] ptSquaredAPITrainerGetSessionInPeriodFrom:prevDay daysCount:3 operationResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            UpdateDataSource([weakSelf verificateSessionWithArray:response withDate:dateWithOffset]);
        }
    }];
}

- (NSArray *)verificateSessionWithArray:(NSArray *)input withDate:(NSDate *)requestedDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *dateInLocalTimezone = [requestedDate dateByAddingTimeInterval:timeZoneSeconds];
    
    NSMutableArray *sessions = [[NSMutableArray alloc] init];
    for (Session *session in input) {
        NSString *sessionDate = [dateFormatter stringFromDate:[session.sessionDate dateByAddingTimeInterval:timeZoneSeconds]];
        sessionDate = [[sessionDate componentsSeparatedByString:@" "] firstObject];
        
        NSString *requestDate = [dateFormatter stringFromDate:dateInLocalTimezone];
        requestDate = [[requestDate componentsSeparatedByString:@" "] firstObject];

        if ([sessionDate isEqualToString:requestDate]) {
            [sessions addObject:session];
        }
    }
    return sessions;
}

- (void)animatedReloadDataIfNeeded
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.sessions.count) {
            [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }
    });
}

- (void)prepareInterfaceColor
{
   // [self.tableView reloadData];
}

@end
