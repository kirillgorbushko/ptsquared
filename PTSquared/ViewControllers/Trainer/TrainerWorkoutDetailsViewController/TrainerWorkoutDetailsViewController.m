//
//  TrainerWorkoutDetailsViewController.m
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TrainerWorkoutDetailsViewController.h"
#import "TrainerNavigationController.h"
#import "UINavigationController+Addition.h"
#import "SupersetHeaderTableViewCell.h"
#import "WorkoutTableViewCell.h"
#import "Workout.h"
#import "Exercise.h"

static NSString *const TableViewCellIdentifier = @"workoutCell";
static NSString *const SupersetHeader = @"supersetHeader";
static NSString *const SupersetName = @"superset";
static CGFloat const SupersetHeaderDefaultHeight = 35.f;
static CGFloat const CellHeight = 51.f;

@interface TrainerWorkoutDetailsViewController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *colorSchemeImageView;

@property (strong, nonatomic) __block NSArray *dataSource;
@property (strong, nonatomic) __block Workout *requestedWorkout;

@end

@implementation TrainerWorkoutDetailsViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareNavigationController];
    [self receiveExersises];
}

#pragma mark - CustomAccessors

- (void)setDataSource:(NSArray *)dataSource
{
    _dataSource = dataSource;
    [self.tableView reloadData];
}

#pragma mark - IBActions

- (void)rightButtonPressed
{
    [((TrainerNavigationController *)self.navigationController) logout];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    Exercise *currentExersise = self.dataSource[section];
    return currentExersise.exerciseSets.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WorkoutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[WorkoutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
    }
    
    Exercise *currentExersise = self.dataSource[indexPath.section];
    if ([currentExersise.exerciseName isEqualToString:SupersetName]) {
        cell.type = WorkoutTableViewCellTypeSuperset;
    } else {
        cell.type = WorkoutTableViewCellTypeNormal;
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SupersetHeaderTableViewCell *header = [tableView dequeueReusableCellWithIdentifier:SupersetHeader];
    header.titleLabel.text = @"SUPERSET";
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    Exercise *currentExersise = self.dataSource[section];
    if ([currentExersise.exerciseName isEqualToString:SupersetName]) {
        return SupersetHeaderDefaultHeight;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Exercise *currentExersise = self.dataSource[indexPath.section];
    if ([currentExersise.exerciseName isEqualToString:SupersetName]) {
        return SupersetHeaderDefaultHeight;
    }
    return CellHeight;
}

- (void)configureCell:(WorkoutTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Exercise *currentExersise = self.dataSource[indexPath.section];

    NSString *valueType = currentExersise.exerciseValueType;
    NSString *intervalType = [currentExersise.exerciseIntervalType capitalizedString];
    NSString *exersiseName = currentExersise.exerciseName;
    NSInteger valueCount;
    NSInteger intervals;
    
    if (!valueType.length || !valueType) {
        valueType = @"";
    }
    if (!intervalType.length || !intervalType) {
        intervalType = @"";
    }
    
    if (indexPath.row) {
        Exercise *exersiseToDisplay = (Exercise *)currentExersise.exerciseSets[indexPath.row];
        valueCount = exersiseToDisplay.exerciseSetValueCount;
        intervals = exersiseToDisplay.exerciseSetIntervalCount;
        exersiseName = exersiseToDisplay.exerciseName;
    } else {
        valueCount = currentExersise.exerciseSetValueCount;
        intervals = currentExersise.exerciseSetIntervalCount;
    }
    
    cell.repeatCountLabel.text = [NSString stringWithFormat:@"%i %@", (int)intervals, intervalType];
    cell.descriptionLabel.text = [exersiseName isEqualToString:@"superset"] ? @"" : exersiseName;
    cell.notesLabel.text =  [NSString stringWithFormat:@"%i%@", (int)valueCount, valueType];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private

- (void)prepareNavigationController
{
    [self updateTitleLabelWithTitle:nil];
    [self.navigationController setRightButtonWithImageNamed:@"Logout_Icon" andDelegate:self];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)updateTitleLabelWithTitle:(NSString *)title
{
    if (title.length) {
        [((TrainerNavigationController *)self.navigationController) setTitleText:title];
    } else {
        [((TrainerNavigationController *)self.navigationController) setTitleText:@"Workout details"];
    }
}

- (void)prepareInterfaceColor
{
    self.colorSchemeImageView.image = [[ColorSchemeManager sharedManager] backgroundImage];
    [self.tableView reloadData];
}

- (void)receiveExersises
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] ptSquaredAPIGetSessionFullWorkoutWithID:self.sessionID operationResult:^(BOOL success, NSError *error, id response) {
        if (success) {
            if (![[(Workout *)response workoutExersises] count]) {
                [AppHelper alertViewWithMessage:AlertMessageWorkoutHasBeenRemoved delegate:self];
            } else {
                weakSelf.requestedWorkout = response;
                
                weakSelf.dataSource = weakSelf.requestedWorkout.workoutExersises;
                //TODO
//                weakSelf.dataSource = [weakSelf.requestedWorkout.workoutExersises sortedArrayUsingComparator:^NSComparisonResult(Exercise *obj1, Exercise *obj2) {
//                    return obj1.exerciseID > obj2.exerciseID;
//                }];
                
                [weakSelf updateTitleLabelWithTitle:weakSelf.requestedWorkout.workoutName];
            }
        }
    }];
}

@end
