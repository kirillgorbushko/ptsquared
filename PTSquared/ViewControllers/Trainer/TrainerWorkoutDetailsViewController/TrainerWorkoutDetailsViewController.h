//
//  TrainerWorkoutDetailsViewController.h
//  PTSquared
//
//  Created by Kirill Gorbushko on 02.05.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface TrainerWorkoutDetailsViewController : BaseDynamicColorViewController

@property (assign, nonatomic) NSInteger sessionID;

@end
